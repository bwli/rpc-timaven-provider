package com.timaven.provider.audit;

import com.timaven.provider.model.dto.Principal;
import org.hibernate.envers.RevisionListener;
import org.slf4j.MDC;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;

public class CustomRevisionListener implements RevisionListener {
    @Override
    public void newRevision(Object o) {
        CustomRevisionEntity entity = (CustomRevisionEntity) o;
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = "System";
        if (authentication != null && authentication.isAuthenticated()) {
            Principal user = (Principal) authentication.getPrincipal();
            username = user.getUsername();
        } else if (!StringUtils.isEmpty(MDC.get("username"))) {
            username = MDC.get("username");
        }
        entity.setUsername(username);
    }
}
