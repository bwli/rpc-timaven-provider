package com.timaven.provider.dao.repository;

import com.timaven.provider.model.ReportSubmission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;

public interface ReportSubmissionRepository extends JpaRepository<ReportSubmission, Long> {

    @Query("select r from ReportSubmission r " +
            "where r.projectId = :projectId " +
            "and r.userId = :userId " +
            "and r.teamName = :teamName " +
            "and r.reportedAt >= :startDate " +
            "and (cast(:endDate as timestamp) is null or r.reportedAt <= :endDate) " +
            "order by r.reportedAt desc")
    List<ReportSubmission> findReportSubmissionsByIdsAndTeamAndDates(Long projectId, Long userId, String teamName, LocalDateTime startDate, LocalDateTime endDate);

    @Query("select distinct r from ReportSubmission r " +
            "left join fetch r.reportPercs " +
            "where r.projectId = :projectId " +
            "and r.userId = :userId " +
            "and r.teamName = :teamName " +
            "and r.reportedAt >= :startDate " +
            "and (cast(:endDate as timestamp) is null or r.reportedAt <= :endDate) " +
            "order by r.reportedAt desc")
    List<ReportSubmission> findReportSubmissionsByIdsAndTeamAndDatesFetching(Long projectId, Long userId, String teamName, LocalDateTime startDate, LocalDateTime endDate);
}
