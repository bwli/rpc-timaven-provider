package com.timaven.provider.dao.repository;

import com.timaven.provider.model.Equipment;
import com.timaven.provider.model.enums.EquipmentOwnershipType;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;


public interface EquipmentRepository extends JpaRepository<Equipment, Long> {
    List<Equipment> findByOwnershipType(EquipmentOwnershipType ownershipType, Sort sort);

    @Query(value = "select e from Equipment e " +
            "left join e.equipmentUsages eu on :projectId is null and eu.projectId is null or eu.projectId = :projectId " +
            "where e.ownershipType = :ownershipType " +
            "or eu.id is not null ")
    Set<Equipment> getRentalAndBeingUsedEquipments(Long projectId, EquipmentOwnershipType ownershipType);

    Equipment findByEquipmentId(String equipmentId);

    List<Equipment> findByActive(boolean active, Sort equipmentId);

    Set<Equipment> findAllByEquipmentIdIn(Set<String> equipmentIds);

    List<Equipment> findByActiveTrue();
}
