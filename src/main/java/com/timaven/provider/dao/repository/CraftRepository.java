package com.timaven.provider.dao.repository;

import com.timaven.provider.model.Craft;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface CraftRepository extends JpaRepository<Craft, Long>, JpaSpecificationExecutor<Craft> {

    @Query(value = "select c from Craft c " +
            "where (:projectId is null and c.projectId is null or c.projectId = :projectId) " +
            "and (cast(:startDate as date) is null " +
            "or ((cast(:endDate as date) is null " +
            "and c.startDate <= :startDate or cast(:endDate as date) is not null " +
            "and c.startDate <= :endDate) and (c.endDate is null or c.startDate <= c.endDate)))")
    Set<Craft> findByProjectIdAndDateRange(Long projectId, LocalDate startDate, LocalDate endDate);

    @Query(value = "with t0 as (select distinct on (c.code) c.* from project.craft c " +
            "where c.code = :code " +
            "and (:projectId = 0 and c.project_id is null or c.project_id = :projectId) " +
            "and c.start_date <= :startDate " +
            "order by c.code, c.created_at desc) " +
            "select * from t0 where (t0.end_date is null or t0.end_date >= :startDate)", nativeQuery = true)
    Craft findByProjectIdAndCodeAndDate(Long projectId, String code, LocalDate startDate);

    @Query(value = "select c from Craft c " +
            "where (:projectId is null and c.projectId is null or c.projectId = :projectId) " +
            "and c.code in :codes")
    List<Craft> findByProjectIdAndCodes(Long projectId, Collection<String> codes);

    List<Craft> findByPerDiemId(Long perDiemId);
}
