package com.timaven.provider.dao.repository;

import com.timaven.provider.model.BillingCodeOverride;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface BillingCodeOverrideRepository extends JpaRepository<BillingCodeOverride, Long> {
    BillingCodeOverride findByBillingCodeIdAndProjectId(Long billingCodeId, Long projectId);

    void deleteByBillingCodeIdAndProjectId(Long id, Long projectId);

    List<BillingCodeOverride> findByProjectId(Long projectId);

    @Query("select bco from BillingCodeOverride bco " +
            "inner join bco.billingCode bc " +
            "where bco.projectId = :projectId " +
            "and bco.codeName in :existingCodeNames " +
            "and bc.typeId = :typeId")
    Set<BillingCodeOverride> findByCodeNameInAndProjectId(Long typeId, Set<String> existingCodeNames, Long projectId);

    List<BillingCodeOverride> findByProjectIdAndBillingCodeIdIn(Long projectId, Collection<Long> billingCodeIds);
}
