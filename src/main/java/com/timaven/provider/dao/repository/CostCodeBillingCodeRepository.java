package com.timaven.provider.dao.repository;

import com.timaven.provider.model.CostCodeBillingCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;

public interface CostCodeBillingCodeRepository extends JpaRepository<CostCodeBillingCode, Long> {
    @Query("select ccbc from CostCodeBillingCode ccbc " +
            "left join fetch ccbc.costCodeLog " +
            "left join fetch ccbc.billingCode bc " +
            "left join fetch bc.billingCodeType " +
            "where ccbc.costCodeId in :costCodeIds")
    Set<CostCodeBillingCode> findAllByCostCodeIdsFetchingCostCodeAndBillingCodeType(Set<Long> costCodeIds);

    @Query("select ccbc from CostCodeBillingCode ccbc " +
            "where ccbc.costCodeId in :costCodeIds")
    Set<CostCodeBillingCode> findAllByCostCodeIds(Set<Long> costCodeIds);

    @Modifying
    @Query(value = "insert into project.cost_code_billing_code (cost_code_id, billing_code_id) " +
            "values (:costCodeId, :billingCodeId) on conflict do nothing", nativeQuery = true)
    void saveIfNoConflict(Long costCodeId, Long billingCodeId);
}
