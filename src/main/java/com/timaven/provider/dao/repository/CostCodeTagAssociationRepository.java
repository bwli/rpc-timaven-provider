package com.timaven.provider.dao.repository;

import com.timaven.provider.model.CostCodeTagAssociation;
import com.timaven.provider.model.enums.CostCodeType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface CostCodeTagAssociationRepository extends JpaRepository<CostCodeTagAssociation, Long> {
    Set<CostCodeTagAssociation> findByKeyInAndCostCodeType(Set<Long> keys, CostCodeType type);

    void deleteByKeyInAndCostCodeType(Set<Long> keys, CostCodeType type);
}
