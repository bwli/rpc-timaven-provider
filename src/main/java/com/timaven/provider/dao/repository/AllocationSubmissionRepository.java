package com.timaven.provider.dao.repository;

import com.timaven.provider.model.AllocationSubmission;
import com.timaven.provider.model.enums.AllocationSubmissionStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;

import javax.persistence.LockModeType;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface AllocationSubmissionRepository extends JpaRepository<AllocationSubmission, Long>, CustomRepository<AllocationSubmission> {
    @Query("select s from AllocationSubmission s " +
            "left join fetch s.reportSubmission " +
            "left join fetch s.allocationTimes t " +
            "left join fetch t.costCodePercs p " +
            "left join fetch t.allocationRecords " +
            "left join fetch t.punchExceptions " +
            "left join fetch t.allocationExceptions " +
            "where s.id = :id")
    AllocationSubmission getAllocationSubmissionFetchAll(Long id);

    @Query("select s from AllocationSubmission s " +
            "left join fetch s.allocationTimes t " +
            "left join fetch t.costCodePercs p " +
            "where s.id = :id")
    AllocationSubmission getAllocationSubmissionFetchingCostCodePercs(Long id);

    @Query(value = "select distinct s from AllocationSubmission s " +
            "left join fetch s.allocationTimes t " +
            "left join fetch t.costCodePercs " +
            "where (cast(:endDate as date) is null or s.dateOfService <= :endDate) " +
            "and (cast(:startDate as date) is null or s.dateOfService >= :startDate) " +
            "and (:projectId is null and s.projectId is null or s.projectId = :projectId) " +
            "and (:teamName is null or s.teamName = :teamName) " +
            "and (:status is null or s.status = :status) " +
            "and (:notStatus is null or s.status <> :notStatus) " +
            "order by s.createdAt desc")
    Set<AllocationSubmission> getAllocationSubmissionsFetchingAllocationTimes(LocalDate startDate, LocalDate endDate, Long projectId, String teamName, AllocationSubmissionStatus status, AllocationSubmissionStatus notStatus);

    @Query(value = "select s from AllocationSubmission s " +
            "where (cast(:startDate as date) is null or s.dateOfService >= :startDate) " +
            "and (cast(:endDate as date) is null or s.dateOfService <= :endDate) " +
            "and (:projectId is null and s.projectId is null or s.projectId = :projectId) " +
            "and (:teamName is null or s.teamName = :teamName) " +
            "and (:status is null or s.status = :status) " +
            "and (:notStatus is null or s.status <> :notStatus) " +
            "order by s.createdAt desc")
    Set<AllocationSubmission> getAllocationSubmissions(LocalDate startDate, LocalDate endDate, Long projectId, String teamName, AllocationSubmissionStatus status, AllocationSubmissionStatus notStatus);

    AllocationSubmission findTopByProjectIdAndTeamNameAndDateOfServiceBeforeAndIsBySystemFalseOrderByDateOfServiceDesc(Long projectId, String teamName, LocalDate dateOfService);

    List<AllocationSubmission> findByProjectIdAndTeamNameAndDateOfServiceAndPayrollDateAndStatusNot(Long projectId, String teamName, LocalDate dateOfService, LocalDate payrollDate, AllocationSubmissionStatus denied);

    @Query(value = "select distinct s from AllocationSubmission s " +
            "inner join s.allocationTimes t " +
            "where t.weeklyProcessId in :ids")
    List<AllocationSubmission> findAllByWeeklyProcessId(Collection<Long> ids);

    List<AllocationSubmission> findByProjectIdAndTeamNameAndDateOfServiceAndStatusNot(Long projectId, String teamName, LocalDate dateOfService, AllocationSubmissionStatus denied);
}
