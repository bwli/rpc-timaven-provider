package com.timaven.provider.dao.repository;

import com.timaven.provider.model.EmployeeView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface EmployeeViewRepository extends JpaRepository<EmployeeView, Long>,
        JpaSpecificationExecutor<EmployeeView> {
}
