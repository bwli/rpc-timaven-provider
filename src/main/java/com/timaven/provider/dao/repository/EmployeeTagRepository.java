package com.timaven.provider.dao.repository;

import com.timaven.provider.model.EmployeeTag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface EmployeeTagRepository extends JpaRepository<EmployeeTag, Long> {
    @Query("select et from EmployeeTag et " +
            "where (:projectId is null and et.projectId is null or et.projectId = :projectId) " +
            "and et.empId = :empId " +
            "and et.tag = :tag")
    EmployeeTag findByProjectIdAndEmpIdAndTag(Long projectId, String empId, String tag);

    @Modifying
    @Query("delete from EmployeeTag et " +
            "where (:projectId is null and et.projectId is null or et.projectId = :projectId) " +
            "and et.empId = :empId " +
            "and et.tag = :tag")
    void deleteByProjectIdAndEmpIdAndTag(Long projectId, String empId, String tag);

    @Query("select et from EmployeeTag et " +
            "where :projectId is null and et.projectId is null or et.projectId = :projectId")
    List<EmployeeTag> findByProjectId(Long projectId);
}
