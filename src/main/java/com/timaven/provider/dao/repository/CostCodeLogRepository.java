package com.timaven.provider.dao.repository;

import com.timaven.provider.model.CostCodeLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface CostCodeLogRepository extends JpaRepository<CostCodeLog, Long>, JpaSpecificationExecutor<CostCodeLog> {
    @Query(value = "select cclg from CostCodeLog cclg " +
            "left join fetch cclg.costCodeBillingCodes " +
            "where cclg.id = :id")
    Optional<CostCodeLog> findByIdFetchingBillingCode(Long id);

    @Modifying
    @Query(value = "update project.cclg set is_locked = true " +
            "where cost_code_full in :costCodes " +
            "and (:projectId = 0 and project_id is null or project_id = :projectId) " +
            "and is_locked = false", nativeQuery = true)
    void lockCostCodes(Set<String> costCodes, Long projectId);

    @Query(value = "select distinct cclg from CostCodeLog cclg " +
            "where ((cclg.projectId is null and (:includeGlobal is true or :projectId is null)) or cclg.projectId = :projectId) " +
            "and (cast(:endDate as date) is null and cclg.startDate <= :startDate " +
            "or (cast(:endDate as date) is not null and cclg.startDate <= cast(:endDate as date))) " +
            "and (cclg.endDate is null or cclg.endDate >= cclg.startDate) " +
            "order by cclg.costCodeFull")
    List<CostCodeLog> findAllByProjectIdAndDateRange(Long projectId, @NotNull LocalDate startDate, LocalDate endDate, boolean includeGlobal);

    @Query(value = " with t0 as (" +
            "select distinct on (cclg.cost_code_full) cclg.* from project.cclg " +
            "where ((project_id is null and (:includeGlobal or :projectId = 0)) or project_id = :projectId) " +
            "and cclg.start_date <= :dateOfService " +
            "order by cost_code_full, project_id nulls last, created_at desc) " +
            "select * from t0 where (t0.end_date is null or t0.end_date >= :dateOfService) ", nativeQuery = true)
    List<CostCodeLog> findAllByProjectIdAndDateOfService(Long projectId, @NotNull LocalDate dateOfService, boolean includeGlobal);
}
