package com.timaven.provider.dao.repository;

import com.timaven.provider.model.CostCodeLogTag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CostCodeLogTagRepository extends JpaRepository<CostCodeLogTag, Long> {
    @Query("select cclt from CostCodeLogTag cclt " +
            "where (:projectId is null and cclt.projectId is null or cclt.projectId = :projectId) " +
            "and cclt.costCodeFull = :costCode " +
            "and cclt.tag = :tag")
    CostCodeLogTag findByProjectIdAndCostCodeFullAndTag(Long projectId, String costCode, String tag);

    @Modifying
    @Query("delete from CostCodeLogTag cclt " +
            "where (:projectId is null and cclt.projectId is null or cclt.projectId = :projectId) " +
            "and cclt.costCodeFull = :costCode " +
            "and cclt.tag = :tag")
    void deleteByProjectIdAndCostCodeFullAndTag(Long projectId, String costCode, String tag);

    @Query("select cclt from CostCodeLogTag cclt " +
            "where :projectId is null and cclt.projectId is null or cclt.projectId = :projectId")
    List<CostCodeLogTag> findByProjectId(Long projectId);
}
