package com.timaven.provider.dao.repository;

import com.timaven.provider.model.UserTeam;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface UserTeamRepository extends JpaRepository<UserTeam, Long> {
    Set<UserTeam> findByUserProjectIdIs(Long id);
}
