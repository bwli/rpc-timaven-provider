package com.timaven.provider.dao.repository;

import com.timaven.provider.model.EquipmentAllocationSubmission;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EquipmentAllocationSubmissionRepository extends JpaRepository<EquipmentAllocationSubmission, Long> {
}
