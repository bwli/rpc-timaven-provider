package com.timaven.provider.dao.repository;

import com.timaven.provider.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Set;

public interface ProjectRepository extends JpaRepository<Project, Long> , JpaSpecificationExecutor<Project> {
    @Query(value = "select * from pm.project p " +
            "left join pm.user_project up on p.id = up.project_id where not p.is_deleted", nativeQuery = true)
    Set<Project> getAllProjectsFetchUserProjects();

    @Query("select p from Project p " +
            "where (:jobNumber is null and p.jobNumber is null or p.jobNumber = :jobNumber) " +
            "and (:subJob is null and p.subJob is null or p.subJob = :subJob) ")
    Project findByJobNumberAndSubJob(String jobNumber, String subJob);

    Set<Project> findByIdIn(Set<Long> ids);
}
