package com.timaven.provider.dao.repository;

import com.timaven.provider.model.StageRoster;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface StageRosterRepository extends JpaRepository<StageRoster, Long> {
    List<StageRoster> findBySubmissionIdIs(Long id);
}
