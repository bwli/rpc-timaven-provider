package com.timaven.provider.dao.repository;

import com.timaven.provider.model.CostCodeExportView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CostCodeExportViewRepository extends JpaRepository<CostCodeExportView, Long> ,
        JpaSpecificationExecutor<CostCodeExportView> {
}
