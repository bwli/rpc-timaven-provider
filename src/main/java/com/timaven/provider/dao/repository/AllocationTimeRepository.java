package com.timaven.provider.dao.repository;

import com.timaven.provider.model.AllocationTime;
import com.timaven.provider.model.enums.AllocationSubmissionStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

public interface AllocationTimeRepository extends JpaRepository<AllocationTime, Long> {

    @Query("delete from AllocationTime a where a.allocationSubmission.id = :allocationSubmissionId")
    @Modifying
    void deleteAllocationTimesByAllocationSubmissionId(@Param("allocationSubmissionId") Long allocationSubmissionId);

    @Query(value = "select t from AllocationTime t " +
            "left join fetch t.costCodePercs " +
            "inner join fetch t.allocationSubmission s " +
            "where s.projectId = :projectId " +
            "and s.status = :status " +
            "and s.dateOfService >= :weekStartDate " +
            "and s.dateOfService <= :weekEndDate " +
            "and s.payrollDate = :weekEndDate")
    Set<AllocationTime> findWeeklyAllocationTimes(Long projectId, LocalDate weekStartDate, LocalDate weekEndDate,
                                                  AllocationSubmissionStatus status);

    @Query(value = "with t0 as (" +
            "select t.emp_id " +
            "from time.allocation_time t " +
            "inner join time.allocation_submission s on s.id = t.submission_id " +
            "inner join time.cost_code_perc ccp on t.id = ccp.allocate_time_id " +
            "where t.payroll_date >= :weekStartDate and t.payroll_date <= :weekEndDate and s.status = 2 " +
            "group by t.emp_id having count(distinct s.project_id) > 1 " +
            "and sum(coalesce(ccp.st_hour, 0) + coalesce(ccp.ot_hour, 0) + coalesce(ccp.dt_hour, 0)) > 40" +
            ") " +
            "select t.* from time.allocation_time t " +
            "inner join t0 on t0.emp_id = t.emp_id " +
            "left join time.allocation_submission s on s.id = t.submission_id " +
            "left join time.cost_code_perc ccp on t.id = ccp.allocate_time_id " +
            "where t.payroll_date >= :weekStartDate " +
            "and t.payroll_date <= :weekEndDate " +
            "and s.status = 2 " +
            "and t.allocated_hour > 0 ", nativeQuery = true)
    Set<AllocationTime> getAllocationTimesForReconcile(LocalDate weekStartDate, LocalDate weekEndDate);

    @Query(value = "select t from AllocationTime  t " +
            "left join fetch t.costCodePercs " +
            "left join fetch t.allocationSubmission " +
            "where t.id in :allocationTimeIds")
    Set<AllocationTime> getAllocationTimesFetchingCostCodePercAndSubmission(Set<Long> allocationTimeIds);

    @Query(value = "select t from AllocationTime t " +
            "left join fetch t.costCodePercs " +
            "inner join fetch t.allocationSubmission s " +
            "where s.projectId = :projectId " +
            "and s.status = :status " +
            "and (s.dateOfService < :weekStartDate) " +
            "and s.payrollDate = :weekEndDate")
    Set<AllocationTime> findTimeLeftOffAllocationTimes(Long projectId, LocalDate weekStartDate, LocalDate weekEndDate,
                                                       AllocationSubmissionStatus status);

    @Query(value = "select t from AllocationTime t " +
            "inner join t.allocationSubmission s " +
            "left join fetch t.costCodePercs " +
            "where (:ignoreDates is true or s.dateOfService in :dates) " +
            "and (cast(:endDate as date) is null or s.dateOfService <= :endDate) " +
            "and (cast(:startDate as date) is null or s.dateOfService >= :startDate) " +
            "and (cast(:payrollStartDate as date) is null or s.payrollDate >= :payrollStartDate) " +
            "and (cast(:payrollEndDate as date) is null or s.payrollDate <= :payrollEndDate) " +
            "and (:isAllProjects is true or (:projectId is null and s.projectId is null or s.projectId = :projectId)) " +
            "and (:teamName is null or :teamName = s.teamName) " +
            "and (:status is null or s.status = :status) " +
            "and (:notStatus is null or s.status <> :notStatus) " +
            "and (:ignoreEmpId is true or t.empId in (:empIds)) " +
            "order by s.createdAt desc")
    Set<AllocationTime> findByDateRangeAndProjectIdAndStatus(LocalDate startDate, LocalDate endDate, boolean ignoreDates, Set<LocalDate> dates, LocalDate payrollStartDate, LocalDate payrollEndDate, boolean isAllProjects, Long projectId, String teamName, AllocationSubmissionStatus status, AllocationSubmissionStatus notStatus, boolean ignoreEmpId, Set<String> empIds);

    @Query(value = "select t from AllocationTime t " +
            "inner join t.allocationSubmission s " +
            "left join fetch t.costCodePercs " +
            "left join fetch t.allocationRecords " +
            "left join fetch t.punchExceptions " +
            "left join fetch t.allocationExceptions " +
            "where (:ignoreDates is true or s.dateOfService in :dates) " +
            "and (cast(:endDate as date) is null or s.dateOfService <= :endDate) " +
            "and (cast(:startDate as date) is null or s.dateOfService >= :startDate) " +
            "and (cast(:payrollStartDate as date) is null or s.payrollDate >= :payrollStartDate) " +
            "and (cast(:payrollEndDate as date) is null or s.payrollDate <= :payrollEndDate) " +
            "and (:isAllProjects is true or (:projectId is null and s.projectId is null or s.projectId = :projectId)) " +
            "and (:teamName is null or :teamName = s.teamName) " +
            "and (:status is null or s.status = :status) " +
            "and (:notStatus is null or s.status <> :notStatus) " +
            "and (:ignoreEmpId is true or t.empId in (:empIds)) " +
            "order by s.createdAt desc")
    Set<AllocationTime> findByDateRangeAndProjectIdAndStatusFetchingAll(LocalDate startDate, LocalDate endDate, boolean ignoreDates, Set<LocalDate> dates, LocalDate payrollStartDate, LocalDate payrollEndDate, Boolean isAllProjects, Long projectId, String teamName, AllocationSubmissionStatus status, AllocationSubmissionStatus notStatus, boolean ignoreEmpId, Set<String> empIds);

    @Query("select t from AllocationTime t " +
            "left join fetch t.allocationSubmission " +
            "left join fetch t.costCodePercs " +
            "where t.id in :allocationTimeIds ")
    Set<AllocationTime> findAllByIdFetchingCostCodePerc(Set<Long> allocationTimeIds);

    @Query("select t from AllocationTime t " +
            "left join fetch t.allocationSubmission " +
            "where t.id in :allocationTimeIds ")
    Set<AllocationTime> findAllByIdFetchingAllocationSubmission(Set<Long> allocationTimeIds);

    List<AllocationTime> findByAllocationSubmissionId(Long id);

    @Query(value = "select t from AllocationTime t " +
            "inner join t.allocationSubmission s " +
            "where t.empId in :empIds " +
            "and s.dateOfService >= :hiredAt " +
            "and s.status in :statuses")
    List<AllocationTime> findByEmpIdAndDateOfServiceAndStatuses(Set<String> empIds, LocalDate hiredAt, List<AllocationSubmissionStatus> statuses);


}
