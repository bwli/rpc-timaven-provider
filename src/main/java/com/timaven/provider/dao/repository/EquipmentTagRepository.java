package com.timaven.provider.dao.repository;

import com.timaven.provider.model.EquipmentTag;
import org.springframework.data.jpa.repository.JpaRepository;


public interface EquipmentTagRepository extends JpaRepository<EquipmentTag, Long> {
    EquipmentTag findByEquipmentIdAndTag(String equipmentId, String tag);

    void deleteByEquipmentIdAndTag(String equipmentId, String tag);
}
