package com.timaven.provider.dao.repository;

import com.timaven.provider.model.StageSubmission;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;


public interface StageSubmissionRepository extends JpaRepository<StageSubmission, Long> {
    List<StageSubmission> findByEffectiveOnLessThanEqualAndStatusIsAndTypeIsOrderByCreatedAtDesc(LocalDate date, StageSubmission.Status status, StageSubmission.Type type);
}
