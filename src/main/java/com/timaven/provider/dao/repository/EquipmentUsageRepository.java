package com.timaven.provider.dao.repository;

import com.timaven.provider.model.EquipmentUsage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;


public interface EquipmentUsageRepository extends JpaRepository<EquipmentUsage, Long> {
    List<EquipmentUsage> findByActiveTrueAndEndDateNullOrActiveTrueAndEndDateAfter(LocalDate now);

    @Query("select eu from EquipmentUsage eu left join fetch eu.project " +
            "left join fetch eu.equipment " +
            "where eu.equipmentId = :equipmentId " +
            "and eu.active = true " +
            "and (eu.endDate is null or eu.endDate >= current_date) ")
    List<EquipmentUsage> findByEquipmentIdAndDate(Long equipmentId);

    @Query("select eu from EquipmentUsage eu " +
            "left join fetch eu.equipment " +
            "where eu.active = true " +
            "and (:projectId is null or eu.projectId = :projectId) " +
            "and (:equipmentId is null or eu.equipmentId = :equipmentId) " +
            "and (eu.endDate is null or eu.endDate > :startDate) " +
            "and (cast(:endDate as date) is null or eu.startDate <= :endDate) " +
            "order by eu.equipmentId, eu.projectId")
    List<EquipmentUsage> findByIdsAndDates(Long equipmentId, Long projectId, LocalDate startDate, LocalDate endDate);
}
