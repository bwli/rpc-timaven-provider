package com.timaven.provider.dao.repository;

import com.timaven.provider.model.PurchaseOrderGroup;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface PurchaseOrderGroupRepository extends JpaRepository<PurchaseOrderGroup, Long> {
    PurchaseOrderGroup findByPurchaseOrderNumber(String purchaseOrderNumber);

    void deleteByPurchaseOrderNumberNotIn(Collection<String> purchaseOrderNumbers);

    List<PurchaseOrderGroup> findByPurchaseOrderNumberIn(Collection<String> purchaseOrderNumbers);
}
