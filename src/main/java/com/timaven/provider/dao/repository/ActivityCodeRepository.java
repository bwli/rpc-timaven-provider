package com.timaven.provider.dao.repository;

import com.timaven.provider.model.ActivityCode;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ActivityCodeRepository extends JpaRepository<ActivityCode, Long> {
    Optional<ActivityCode> findByActivityCode(String activityCode);
}
