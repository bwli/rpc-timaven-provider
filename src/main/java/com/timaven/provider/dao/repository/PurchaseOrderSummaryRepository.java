package com.timaven.provider.dao.repository;

import com.timaven.provider.model.PurchaseOrderSummary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PurchaseOrderSummaryRepository extends JpaRepository<PurchaseOrderSummary, Long>,
        JpaSpecificationExecutor<PurchaseOrderSummary> {
}
