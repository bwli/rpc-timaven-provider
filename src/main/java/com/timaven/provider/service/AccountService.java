package com.timaven.provider.service;

import com.timaven.provider.model.Role;
import com.timaven.provider.model.User;
import com.timaven.provider.model.UserRole;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface AccountService {

    void saveUser(User user);

    User getUserById(Long userId);

    void saveUserRole(UserRole userRole);

    boolean changeUserValidity(Long id, boolean active);

    User loadUserByUsername(String username, Collection<String> includes);

    void addUser(User user, Long projectId);

    Set<User> getAdminManagerUsersByProjectId(Long projectId);

    User[] getUsersByProjectIdAndRoles(Long projectId, List<String> roles, Boolean includeSelf, Boolean includeNoRole);

    Role getRoleByName(String roleName);

    Role[] getRolesByNames(List<String> roleNames);

    User[] getUsers(Set<String> roles, Set<String> notIncludeRoles, Set<String> includes);

    List<User> getUsersById(Collection<Long> userIds);
}
