package com.timaven.provider.service.impl;

import com.timaven.provider.dao.repository.RPCUserPoolRepository;
import com.timaven.provider.model.RPCUserPool;
import com.timaven.provider.service.RPCUserPoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RPCUserPoolServiceImpl implements RPCUserPoolService {
    private final RPCUserPoolRepository rpcUserPoolRepository;

    @Autowired
    public RPCUserPoolServiceImpl(RPCUserPoolRepository rpcUserPoolRepository) {
        this.rpcUserPoolRepository = rpcUserPoolRepository;
    }

    @Override
    public List<RPCUserPool> getUserPools() {
        return rpcUserPoolRepository.getActiveUserPools();
    }
}
