package com.timaven.provider.service.impl;

import com.timaven.provider.dao.repository.CraftRepository;
import com.timaven.provider.dao.repository.EmployeeRepository;
import com.timaven.provider.dao.repository.PerDiemRepository;
import com.timaven.provider.dao.repository.RuleRepository;
import com.timaven.provider.model.Craft;
import com.timaven.provider.model.Employee;
import com.timaven.provider.model.PerDiem;
import com.timaven.provider.model.Rule;
import com.timaven.provider.model.paging.Page;
import com.timaven.provider.model.paging.PagingRequest;
import com.timaven.provider.model.utils.PagingRequestUtil;
import com.timaven.provider.service.PerDiemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.persistence.criteria.Predicate;
import java.util.*;


@Service
@Transactional
public class PerDiemServiceImpl implements PerDiemService {

    private final PerDiemRepository perDiemRepository;
    private final RuleRepository ruleRepository;
    private final CraftRepository craftRepository;
    private final EmployeeRepository employeeRepository;

    @Autowired
    public PerDiemServiceImpl(PerDiemRepository perDiemRepository, RuleRepository ruleRepository, CraftRepository craftRepository, EmployeeRepository employeeRepository) {
        this.perDiemRepository = perDiemRepository;
        this.ruleRepository = ruleRepository;
        this.craftRepository = craftRepository;
        this.employeeRepository = employeeRepository;
    }

    @Override
    public Page<PerDiem> getPerDiemPage(Long projectId, PagingRequest pagingRequest) {
        Page<PerDiem> page = new Page<>();
        page.setDraw(pagingRequest.getDraw());
        Specification<PerDiem> specificationAll = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (null == projectId) {
                predicates.add(criteriaBuilder.isNull(root.get("projectId")));
            } else {
                predicates.add(criteriaBuilder.equal(root.get("projectId"), projectId));
            }
            predicates.add(criteriaBuilder.isTrue(root.get("active")));
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
        List<PerDiem> allPerDiems = perDiemRepository.findAll(specificationAll);
        page.setRecordsTotal(allPerDiems.size());

        Map<String, String> columnMap = new HashMap<>();
        Specification<PerDiem> specification = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (null == projectId) {
                predicates.add(criteriaBuilder.isNull(root.get("projectId")));
            } else {
                predicates.add(criteriaBuilder.equal(root.get("projectId"), projectId));
            }
            predicates.add(criteriaBuilder.isTrue(root.get("active")));
            return PagingRequestUtil.getPredicate(root, criteriaQuery, criteriaBuilder, pagingRequest, columnMap,
                    predicates);
        };
        List<PerDiem> perDiemList = perDiemRepository.findAll(specification);
        page.setRecordsFiltered(perDiemList.size());
        Pageable pageable = PagingRequestUtil.Paging2Pageable(pagingRequest, columnMap);
        if (pageable != null) {
            org.springframework.data.domain.Page<PerDiem> rowPage = perDiemRepository.findAll(specification,
                    pageable);
            perDiemList = rowPage.getContent();
        }
        page.setData(perDiemList);
        return page;

    }

    @Override
    public PerDiem findById(Long id) {
        return perDiemRepository.findById(id).orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public PerDiem savePerDiem(PerDiem perDiem) {
        return perDiemRepository.save(perDiem);
    }

    @Override
    public void deleteById(Long id) {
        PerDiem perDiem = perDiemRepository.findById(id).orElseThrow(IllegalArgumentException::new);
        perDiem.setActive(false);
        List<Rule> rules = ruleRepository.findByPerDiemId(id);
        if (!CollectionUtils.isEmpty(rules)) {
            rules.forEach(r -> r.setPerDiemId(null));
        }
        List<Employee> employees = employeeRepository.findByPerDiemId(id);
        if (!CollectionUtils.isEmpty(employees)) {
            employees.forEach(e -> e.setPerDiemId(null));
        }
        List<Craft> crafts = craftRepository.findByPerDiemId(id);
        if (!CollectionUtils.isEmpty(crafts)) {
            crafts.forEach(c -> c.setPerDiemId(null));
        }
    }

    @Override
    public PerDiem[] getPerDiemsByProjectId(Long projectId) {
        List<PerDiem> perDiemList = perDiemRepository.findByProjectIdOrderByNameAsc(projectId);
        return perDiemList.toArray(PerDiem[]::new);
    }
}
