package com.timaven.provider.service.impl;

import com.timaven.provider.dao.repository.*;
import com.timaven.provider.model.*;
import com.timaven.provider.model.dto.CostCodeAssociationDto;
import com.timaven.provider.model.enums.CostCodeType;
import com.timaven.provider.model.paging.Page;
import com.timaven.provider.model.paging.PagingRequest;
import com.timaven.provider.model.utils.PagingRequestUtil;
import com.timaven.provider.service.CostCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.persistence.criteria.Predicate;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Service
@Transactional
public class CostCodeServiceImpl implements CostCodeService {

    private final CostCodeLogRepository costCodeLogRepository;
    private final CostCodeBillingCodeRepository costCodeBillingCodeRepository;
    private final CostCodeAssociationRepository costCodeAssociationRepository;
    private final TeamCostCodeRepository teamCostCodeRepository;
    private final CostCodeTagTypeRepository costCodeTagTypeRepository;
    private final CostCodeTagRepository costCodeTagRepository;
    private final CostCodeTagAssociationRepository costCodeTagAssociationRepository;
    private final CostCodeLogTagRepository costCodeLogTagRepository;
    private final CostCodeExportViewRepository costCodeExportViewRepository;

    @Autowired
    public CostCodeServiceImpl(CostCodeLogRepository costCodeLogRepository,
                               CostCodeBillingCodeRepository costCodeBillingCodeRepository,
                               CostCodeAssociationRepository costCodeAssociationRepository,
                               TeamCostCodeRepository teamCostCodeRepository,
                               CostCodeTagTypeRepository costCodeTagTypeRepository,
                               CostCodeTagRepository costCodeTagRepository,
                               CostCodeTagAssociationRepository costCodeTagAssociationRepository,
                               CostCodeLogTagRepository costCodeLogTagRepository,
                               CostCodeExportViewRepository costCodeExportViewRepository) {
        this.costCodeLogRepository = costCodeLogRepository;
        this.costCodeBillingCodeRepository = costCodeBillingCodeRepository;
        this.costCodeAssociationRepository = costCodeAssociationRepository;
        this.teamCostCodeRepository = teamCostCodeRepository;
        this.costCodeTagTypeRepository = costCodeTagTypeRepository;
        this.costCodeTagRepository = costCodeTagRepository;
        this.costCodeTagAssociationRepository = costCodeTagAssociationRepository;
        this.costCodeLogTagRepository = costCodeLogTagRepository;
        this.costCodeExportViewRepository = costCodeExportViewRepository;
    }

    @Override
    public void deleteById(Long id) {
        costCodeLogRepository.findById(id).ifPresent(cc -> cc.setEndDate(LocalDate.now()));
    }

    @Override
    public Page<CostCodeAssociationDto> findCostCodeAssociations(PagingRequest pagingRequest, Long projectId) {
        Page<CostCodeAssociationDto> page = new Page<>();
        page.setDraw(pagingRequest.getDraw());

        final Set<Long> costCodeIds;
        if (projectId != null) {
            costCodeIds = costCodeAssociationRepository.findDistinctIds(projectId);
        } else {
            costCodeIds = new HashSet<>();
        }
        Specification<CostCodeAssociation> specificationAll = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (null != projectId) {
                predicates.add(root.get("id").in(costCodeIds));
            } else {
                predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("startDate"), LocalDate.now()));
                predicates.add(criteriaBuilder.or(criteriaBuilder.isNull(root.get("endDate")),
                        criteriaBuilder.greaterThan(root.get("endDate"), LocalDate.now())));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };

        page.setRecordsTotal(costCodeAssociationRepository.count(specificationAll));
        Map<String, String> columnMap = new HashMap<>();

        Specification<CostCodeAssociation> specification = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (null != projectId) {
                predicates.add(root.get("id").in(costCodeIds));
            } else {
                predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("startDate"), LocalDate.now()));
                predicates.add(criteriaBuilder.or(criteriaBuilder.isNull(root.get("endDate")),
                        criteriaBuilder.greaterThan(root.get("endDate"), LocalDate.now())));
            }
            return PagingRequestUtil.getPredicate(root, criteriaQuery, criteriaBuilder, pagingRequest, columnMap,
                    predicates);
        };
        long recordsFiltered;
        List<CostCodeAssociation> costCodeAssociations;
        Pageable pageable = PagingRequestUtil.Paging2Pageable(pagingRequest, columnMap);
        if (pageable != null) {
            org.springframework.data.domain.Page<CostCodeAssociation> rowPage = costCodeAssociationRepository.findAll(specification,
                    pageable);
            costCodeAssociations = rowPage.getContent();
            recordsFiltered = costCodeAssociationRepository.count(specification);
        } else {
            costCodeAssociations = costCodeAssociationRepository.findAll(specification);
            recordsFiltered = costCodeAssociations.size();
        }
        page.setRecordsFiltered(recordsFiltered);
        List<CostCodeAssociationDto> rows = costCodeAssociations.stream()
                .map(CostCodeAssociationDto::new).collect(toList());
        page.setData(rows);
        return page;
    }

    @Override
    public CostCodeLog getCostCodeById(Long id) {
        return costCodeLogRepository.findById(id).orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public CostCodeBillingCode[] getCostCodeBillingCodes(Set<Long> costCodeIds, Set<String> includes) {
        Set<CostCodeBillingCode> costCodeBillingCodes;
        if (includes == null) includes = new HashSet<>();
        if (includes.contains("costCodeLog") || includes.contains("billingCodeType")) {
            costCodeBillingCodes = costCodeBillingCodeRepository.findAllByCostCodeIdsFetchingCostCodeAndBillingCodeType(costCodeIds);
        } else {
            costCodeBillingCodes = costCodeBillingCodeRepository.findAllByCostCodeIds(costCodeIds);
        }
        return costCodeBillingCodes.toArray(CostCodeBillingCode[]::new);
    }

    @Override
    public void lockCostCodes(Set<String> costCodes, Long projectId) {
        costCodeLogRepository.lockCostCodes(costCodes, projectId == null ? 0L : projectId);
    }

    @Override
    public CostCodeLog[] findCostCodesByProjectAndDate(Long projectId, LocalDate startDate, LocalDate endDate, Collection<String> costCodes, boolean includeGlobal) {
        List<CostCodeLog> costCodeLogs;
        if (endDate == null || startDate.isEqual(endDate)) {
            costCodeLogs = costCodeLogRepository.findAllByProjectIdAndDateOfService(projectId == null ? 0L : projectId, startDate, includeGlobal);
        } else {
            costCodeLogs = costCodeLogRepository.findAllByProjectIdAndDateRange(projectId == null ? 0L : projectId, startDate, endDate, includeGlobal);
        }

        if (costCodes != null) {
            costCodeLogs = costCodeLogs.stream()
                    .filter(c -> costCodes.contains(c.getCostCodeFull()))
                    .collect(Collectors.toList());
        }
        return costCodeLogs.toArray(CostCodeLog[]::new);
    }

    @Override
    public CostCodeLog[] getCostCodeLogsByProjectIdAndTeamNameAndDateOfService(Long projectId, String teamName, LocalDate dateOfService) {
        List<TeamCostCode> teamCostCodes = teamCostCodeRepository
                .findByProjectIdIsAndTeamNameAndDateOfService(projectId, teamName, dateOfService);
        Set<String> codes = teamCostCodes.stream()
                .map(TeamCostCode::getCostCode).collect(Collectors.toSet());
        return findCostCodesByProjectAndDate(projectId, dateOfService, null, codes, true);
    }

    @Override
    public Page<CostCodeTagType> getCostCodeTagTypesByProjectId(Long projectId, PagingRequest pagingRequest) {
        Page<CostCodeTagType> page = new Page<>();
        page.setDraw(pagingRequest.getDraw());

        Specification<CostCodeTagType> specificationAll = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(criteriaBuilder.equal(root.get("projectId"), projectId));
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };

        page.setRecordsTotal(costCodeTagTypeRepository.count(specificationAll));
        Map<String, String> columnMap = new HashMap<>();
        columnMap.put("idType", "tagType");

        Specification<CostCodeTagType> specification = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(criteriaBuilder.equal(root.get("projectId"), projectId));
            return PagingRequestUtil.getPredicate(root, criteriaQuery, criteriaBuilder, pagingRequest, columnMap,
                    predicates);
        };
        long recordsFiltered;
        List<CostCodeTagType> costCodeTagTypes;
        Pageable pageable = PagingRequestUtil.Paging2Pageable(pagingRequest, columnMap);
        if (pageable != null) {
            org.springframework.data.domain.Page<CostCodeTagType> rowPage = costCodeTagTypeRepository.findAll(specification,
                    pageable);
            costCodeTagTypes = rowPage.getContent();
            recordsFiltered = costCodeTagTypeRepository.count(specification);
        } else {
            costCodeTagTypes = costCodeTagTypeRepository.findAll(specification);
            recordsFiltered = costCodeTagTypes.size();
        }
        page.setRecordsFiltered(recordsFiltered);
        costCodeTagTypes.forEach(t -> {
            t.getIdType().put("id", t.getId());
            t.getIdType().put("type", t.getTagType());
        });
        page.setData(costCodeTagTypes);
        return page;
    }

    @Override
    public CostCodeTagType saveCostCodeTagType(CostCodeTagType costCodeTagType) {
        CostCodeTagType existingType = costCodeTagTypeRepository.findByProjectIdAndTagType(costCodeTagType.getProjectId(), costCodeTagType.getTagType());
        if (existingType != null) {
            costCodeTagType.setId(existingType.getId());
        }
        return costCodeTagTypeRepository.save(costCodeTagType);
    }

    @Override
    public void deleteCostCodeTagTypeById(Long id) {
        costCodeTagTypeRepository.deleteById(id);
    }

    @Override
    public Page<CostCodeTag> getCostCodeTagsByProjectId(Long typeId, PagingRequest pagingRequest) {
        Page<CostCodeTag> page = new Page<>();
        page.setDraw(pagingRequest.getDraw());

        Specification<CostCodeTag> specificationAll = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(criteriaBuilder.equal(root.get("typeId"), typeId));
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };

        page.setRecordsTotal(costCodeTagRepository.count(specificationAll));
        Map<String, String> columnMap = new HashMap<>();
        columnMap.put("idCode", "codeName");

        Specification<CostCodeTag> specification = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(criteriaBuilder.equal(root.get("typeId"), typeId));
            return PagingRequestUtil.getPredicate(root, criteriaQuery, criteriaBuilder, pagingRequest, columnMap,
                    predicates);
        };
        long recordsFiltered;
        List<CostCodeTag> costCodeTagTypes;
        Pageable pageable = PagingRequestUtil.Paging2Pageable(pagingRequest, columnMap);
        if (pageable != null) {
            org.springframework.data.domain.Page<CostCodeTag> rowPage = costCodeTagRepository.findAll(specification,
                    pageable);
            costCodeTagTypes = rowPage.getContent();
            recordsFiltered = costCodeTagRepository.count(specification);
        } else {
            costCodeTagTypes = costCodeTagRepository.findAll(specification);
            recordsFiltered = costCodeTagTypes.size();
        }
        page.setRecordsFiltered(recordsFiltered);
        costCodeTagTypes.forEach(t -> {
            t.getIdCode().put("id", t.getId());
            t.getIdCode().put("code", t.getCodeName());
        });
        page.setData(costCodeTagTypes);
        return page;
    }

    @Override
    public CostCodeTag saveCostCodeTag(CostCodeTag costCodeTag) {
        CostCodeTag existingTag = costCodeTagRepository.findByTypeIdAndCodeName(costCodeTag.getTypeId(), costCodeTag.getCodeName());
        if (existingTag != null) {
            costCodeTag.setId(existingTag.getId());
        }
        return costCodeTagRepository.save(costCodeTag);
    }

    @Override
    public void deleteCostCodeTagById(Long id) {
        costCodeTagRepository.deleteById(id);
    }

    @Override
    public CostCodeTagType getCostCodeTagTypeById(Long id) {
        return costCodeTagTypeRepository.findById(id).orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public CostCodeTag getCostCodeTagById(Long id) {
        return costCodeTagRepository.findById(id).orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public CostCodeTagType[] getAllCostCodeTagTypeByProjectId(Long projectId) {
        return costCodeTagTypeRepository.findByProjectId(projectId).toArray(CostCodeTagType[]::new);
    }

    @Override
    public CostCodeTag[] getCostCodeTagsByTypeId(Long typeId) {
        return costCodeTagRepository.findByTypeId(typeId).toArray(CostCodeTag[]::new);
    }

    @Override
    public void deleteCostCodeTagAssociationsByKeyInAndCostCodeType(Set<Long> keys, CostCodeType type) {
        costCodeTagAssociationRepository.deleteByKeyInAndCostCodeType(keys, type);
    }

    @Override
    public void saveCostCodeTagAssociations(Set<CostCodeTagAssociation> costCodeTagAssociations) {
        costCodeTagAssociationRepository.saveAll(costCodeTagAssociations);
    }

    @Override
    public CostCodeTagAssociation[] getCostCodeTagAssociationsByKeysAndType(Set<Long> keys, CostCodeType costCodeType) {
        return costCodeTagAssociationRepository.findByKeyInAndCostCodeType(keys, costCodeType).toArray(CostCodeTagAssociation[]::new);
    }

    @Override
    public void saveCostCodeLogTag(Long projectId, String costCode, String tag) {
        CostCodeLogTag costCodeLogTag = costCodeLogTagRepository.findByProjectIdAndCostCodeFullAndTag(projectId, costCode, tag);
        if (null == costCodeLogTag) {
            costCodeLogTag = new CostCodeLogTag(null, costCode, tag, projectId);
            costCodeLogTagRepository.save(costCodeLogTag);
        }
    }

    @Override
    public void deleteCostCodeLogTag(Long projectId, String costCode, String tag) {
        costCodeLogTagRepository.deleteByProjectIdAndCostCodeFullAndTag(projectId, costCode, tag);
    }

    @Override
    public String[] findCostCodeLogTagStringsByProjectId(Long projectId) {
        List<CostCodeLogTag> costCodeLogTags = costCodeLogTagRepository.findByProjectId(projectId);
        return costCodeLogTags.stream().map(CostCodeLogTag::getTag).toArray(String[]::new);
    }

    @Override
    public CostCodeLogTag[] findCostCodeLogTagsByProjectId(Long projectId) {
        List<CostCodeLogTag> costCodeLogTags = costCodeLogTagRepository.findByProjectId(projectId);
        return costCodeLogTags.toArray(CostCodeLogTag[]::new);
    }

    @Override
    public TeamCostCode[] getTeamCostCodesByProjectIdAndTeamNames(Long projectId, Collection<String> teamNames, LocalDate startDate, LocalDate endDate) {
        List<TeamCostCode> teamCostCodes = teamCostCodeRepository.findByProjectIdAndDateRange(projectId, startDate, endDate);
        if (!CollectionUtils.isEmpty(teamNames)) {
            teamCostCodes = teamCostCodes.stream().filter(tcc -> teamNames.contains(tcc.getTeamName())).collect(toList());
        }
        return teamCostCodes.toArray(TeamCostCode[]::new);
    }

    @Override
    public void saveTeamCostCodes(Set<TeamCostCode> teamCostCodes) {
        teamCostCodeRepository.saveAll(teamCostCodes);
    }

    @Override
    public CostCodeLog[] saveCostCodeLogs(Collection<CostCodeLog> costCodeLogs) {
        return costCodeLogRepository.saveAll(costCodeLogs).toArray(CostCodeLog[]::new);
    }

    @Override
    public void saveCostCodeBillingCodeById(Long costCodeId, Long billingCodeId) {
        costCodeBillingCodeRepository.saveIfNoConflict(costCodeId, billingCodeId);
    }

    @Override
    public CostCodeTagType getCostCodeTagByProjectIdAndTagType(Long projectId, String tagType) {
        return costCodeTagTypeRepository.findByProjectIdAndTagType(projectId, tagType);
    }

    @Override
    public CostCodeTag getCostCodeTagByTypeIdAndCodeName(Long typeId, String codeName) {
        return costCodeTagRepository.findByTypeIdAndCodeName(typeId, codeName);
    }

    @Override
    public CostCodeTagType[] getAllCostCodeTagTypes() {
        return costCodeTagTypeRepository.findAll().toArray(CostCodeTagType[]::new);
    }

    @Override
    public List<CostCodeAssociation> findCostCodeAssociations(Long projectId, LocalDate dateOfService, Collection<String> costCodes) {
        final Set<Long> costCodeIds;
        if (projectId != null) {
            costCodeIds = costCodeAssociationRepository.findDistinctIds(projectId, dateOfService);
        } else {
            costCodeIds = new HashSet<>();
        }
        Specification<CostCodeAssociation> specificationAll = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (null != projectId) {
                predicates.add(root.get("id").in(costCodeIds));
            } else {
                predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("startDate"), dateOfService));
                predicates.add(criteriaBuilder.or(criteriaBuilder.isNull(root.get("endDate")),
                        criteriaBuilder.greaterThan(root.get("endDate"), dateOfService)));
            }
            if (!CollectionUtils.isEmpty(costCodes)) {
                predicates.add(root.get("costCode").in(new HashSet<>(costCodes)));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
        return costCodeAssociationRepository.findAll(specificationAll);
    }

    @Override
    public List<CostCodeAssociation> findCostCodeAssociations(Long projectId, LocalDate startDate, LocalDate endDate, Collection<String> costCodes) {
        boolean ignoreCostCodes = CollectionUtils.isEmpty(costCodes);
        return costCodeAssociationRepository.findByProjectIdAndCodesAndDates(projectId, ignoreCostCodes, new HashSet<>(costCodes), startDate, endDate);
    }

    @Override
    public Set<CostCodeAssociation> findCostCodeAssociationsByProjectIdAndCodes(Long projectId, Set<String> codes, LocalDate dateOfService) {
        return costCodeAssociationRepository.findByProjectIdAndCodes(projectId, codes, dateOfService);
    }

    @Override
    public List<CostCodeExportView> findCostCodeExportViewByProjectId(Long projectId) {
        Specification<CostCodeExportView> specificationAll = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (null != projectId) {
                predicates.add(criteriaBuilder.equal(root.get("projectId"), projectId));
            } else {
                predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("startDate"), LocalDate.now()));
                predicates.add(criteriaBuilder.or(criteriaBuilder.isNull(root.get("endDate")),
                        criteriaBuilder.greaterThan(root.get("endDate"), LocalDate.now())));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
        return  costCodeExportViewRepository.findAll(specificationAll);
    }
}
