package com.timaven.provider.service.impl;

import com.timaven.provider.dao.repository.*;
import com.timaven.provider.model.*;
import com.timaven.provider.model.dto.EquipmentAllocationTimeFilter;
import com.timaven.provider.model.dto.EquipmentPriceDto;
import com.timaven.provider.model.enums.EquipmentAllocationSubmissionStatus;
import com.timaven.provider.model.enums.EquipmentOwnershipType;
import com.timaven.provider.model.enums.EquipmentWeeklyProcessType;
import com.timaven.provider.model.paging.Page;
import com.timaven.provider.model.paging.PagingRequest;
import com.timaven.provider.model.utils.PagingRequestUtil;
import com.timaven.provider.service.AccountService;
import com.timaven.provider.service.EquipmentService;
import com.timaven.provider.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.persistence.criteria.Predicate;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Transactional
public class EquipmentServiceImpl implements EquipmentService {

    private final EquipmentRepository equipmentRepository;
    private final EquipmentUsageRepository equipmentUsageRepository;
    private final EquipmentPriceRepository equipmentPriceRepository;
    private final AccountService accountService;
    private final NotificationService notificationService;
    private final ProjectRepository projectRepository;
    private final EquipmentAllocationTimeRepository equipmentAllocationTimeRepository;
    private final EquipmentAllocationSubmissionRepository equipmentAllocationSubmissionRepository;
    private final EquipmentWeeklyProcessRepository equipmentWeeklyProcessRepository;
    private final EquipmentTagRepository equipmentTagRepository;
    private final EquipmentCostCodePercRepository equipmentCostCodePercRepository; // TM-275

    @Autowired
    public EquipmentServiceImpl(EquipmentRepository equipmentRepository,
                                EquipmentUsageRepository equipmentUsageRepository,
                                EquipmentPriceRepository equipmentPriceRepository, AccountService accountService,
                                NotificationService notificationService, ProjectRepository projectRepository,
                                EquipmentAllocationTimeRepository equipmentAllocationTimeRepository,
                                EquipmentAllocationSubmissionRepository equipmentAllocationSubmissionRepository,
                                EquipmentWeeklyProcessRepository equipmentWeeklyProcessRepository,
                                EquipmentTagRepository equipmentTagRepository,
                                EquipmentCostCodePercRepository equipmentCostCodePercRepository) {
        this.equipmentRepository = equipmentRepository;
        this.equipmentUsageRepository = equipmentUsageRepository;
        this.equipmentPriceRepository = equipmentPriceRepository;
        this.accountService = accountService;
        this.notificationService = notificationService;
        this.projectRepository = projectRepository;
        this.equipmentAllocationTimeRepository = equipmentAllocationTimeRepository;
        this.equipmentAllocationSubmissionRepository = equipmentAllocationSubmissionRepository;
        this.equipmentWeeklyProcessRepository = equipmentWeeklyProcessRepository;
        this.equipmentTagRepository = equipmentTagRepository;
        this.equipmentCostCodePercRepository = equipmentCostCodePercRepository;
    }

    @Override
    public List<EquipmentUsage> getCurrentEquipmentUsages() {
        return equipmentUsageRepository.findByActiveTrueAndEndDateNullOrActiveTrueAndEndDateAfter(LocalDate.now());
    }

    @Override
    public Equipment getEquipmentById(Long id) {
        return equipmentRepository.findById(id).orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public Equipment getEquipmentByEquipmentId(String equipmentId) {
        return equipmentRepository.findByEquipmentId(equipmentId);
    }

    @Override
    public void saveEquipment(Equipment equipment) {
        equipmentRepository.save(equipment);
    }

    @Override
    public boolean changeEquipmentValidity(Long id, boolean active) {
        Optional<Equipment> equipmentOptional = equipmentRepository.findById(id);
        equipmentOptional.ifPresent(e -> e.setActive(active));
        return equipmentOptional.isPresent();
    }

    @Override
    public EquipmentUsage saveEquipmentUsage(Long equipmentId, Long userId, Long projectId, Long id, LocalDate date,
                                             String type) {
        Equipment equipment = equipmentRepository.findById(equipmentId).orElseThrow(IllegalArgumentException::new);
        Optional<EquipmentPrice> equipmentPriceOptional =
                equipmentPriceRepository.findByProjectIdAndEquipmentClass(projectId, equipment.getEquipmentClass());
        EquipmentUsage usage;
        if (id == null) {
            usage = new EquipmentUsage();
            usage.setProjectId(projectId);
            usage.setEquipmentId(equipmentId);
            usage.setStartedBy(userId);
            usage.setStartDate(date);

            if (date.isBefore(LocalDate.now()) || date.isEqual(LocalDate.now())) {
                List<EquipmentUsage> equipmentUsages = equipmentUsageRepository.findByEquipmentIdAndDate(equipmentId);
                Project project = projectRepository.findById(projectId).orElseThrow(IllegalArgumentException::new);
                usage.setProject(project);
                usage.setEquipment(equipmentRepository.findById(equipmentId).orElseThrow(IllegalArgumentException::new));
                equipmentUsages.add(usage);
                checkEquipmentScheduleConflict(equipmentUsages);
            }

            equipmentPriceOptional.ifPresent(ep -> {
                usage.setDailyRate(ep.getDailyRate());
                usage.setWeeklyRate(ep.getWeeklyRate());
                usage.setMonthlyRate(ep.getMonthlyRate());
            });
            equipmentUsageRepository.save(usage);
        } else {
            usage = equipmentUsageRepository.findById(id).orElseThrow(IllegalArgumentException::new);
            // Don't allow update usage from different project id
            if (!usage.getProjectId().equals(projectId)) return usage;
            if ("startDate".equalsIgnoreCase(type)) {
                usage.setStartDate(date);
                usage.setStartedBy(userId);
                if (date.isBefore(LocalDate.now()) || date.isEqual(LocalDate.now())) {
                    List<EquipmentUsage> equipmentUsages = equipmentUsageRepository.findByEquipmentIdAndDate(equipmentId);
                    Project project = projectRepository.findById(projectId).orElseThrow(IllegalArgumentException::new);
                    usage.setProject(project);
                    usage.setEquipment(equipmentRepository.findById(equipmentId).orElseThrow(IllegalArgumentException::new));
                    equipmentUsages.add(usage);
                    checkEquipmentScheduleConflict(equipmentUsages);
                }
            } else if ("endDate".equalsIgnoreCase(type)) {
                usage.setEndDate(date);
                usage.setEndedBy(userId);
            }
        }
        return usage;
    }

    @Override
    public List<EquipmentUsage> saveEquipmentUsageAll(Collection<EquipmentUsage> equipmentUsagesList, Long userId) {
        // TM-300 add import equipment log
        Set<String> collect = equipmentUsagesList.stream().map(EquipmentUsage::getEquipmentId).
                map(String::valueOf).collect(Collectors.toSet());
        Map<String, Equipment> equipmentMap = equipmentRepository.findAllByEquipmentIdIn(collect).stream().
                collect(Collectors.toMap(equipment -> equipment.getEquipmentId(), equipment -> equipment));
        equipmentUsagesList.stream().forEach(equipmentUsage -> {
            Optional<EquipmentPrice> equipmentPriceOptional =
                    equipmentPriceRepository.findByProjectIdAndEquipmentClass(equipmentUsage.getProjectId(),
                            equipmentMap.get(equipmentUsage.getEquipmentId().toString()).getEquipmentClass());
            if (equipmentUsage.getStartDate() != null) {
                LocalDate startDate = equipmentUsage.getStartDate();
                if (startDate.isBefore(LocalDate.now()) || startDate.isEqual(LocalDate.now())) {
                    List<EquipmentUsage> equipmentUsages = equipmentUsageRepository.findByEquipmentIdAndDate(equipmentUsage.getEquipmentId());
                    Project project = projectRepository.findById(equipmentUsage.getProjectId()).orElseThrow(IllegalArgumentException::new);
                    equipmentUsage.setProject(project);
                    equipmentUsage.setEquipment(equipmentMap.get(equipmentUsage.getEquipmentId().toString()));
                    equipmentUsage.setStartedBy(userId);
                    equipmentUsages.add(equipmentUsage);
                    checkEquipmentScheduleConflict(equipmentUsages);
                }
            }
            if (equipmentUsage.getEndDate() != null) {
                equipmentUsage.setEndedBy(userId);
            }
            equipmentPriceOptional.ifPresent(ep -> {
                equipmentUsage.setDailyRate(ep.getDailyRate());
                equipmentUsage.setWeeklyRate(ep.getWeeklyRate());
                equipmentUsage.setMonthlyRate(ep.getMonthlyRate());
            });
            equipmentUsage.setEquipmentId(equipmentMap.get(equipmentUsage.getEquipmentId().toString()).getId());
        });
        return equipmentUsageRepository.saveAll(equipmentUsagesList);
    }

    @Override
    public void deactivateEquipmentUsage(Long id) {
        equipmentUsageRepository.findById(id).ifPresent(e -> e.setActive(false));
    }

    @Override
    public void checkEquipmentScheduleConflict() {
        List<EquipmentUsage> equipmentUsages = getCurrentEquipmentUsages();
        checkEquipmentScheduleConflict(equipmentUsages);
    }

    @Override
    public void saveEquipmentAllocationSubmissions(Set<EquipmentAllocationSubmission> submissions) {
        submissions.forEach(s -> {
            if (!CollectionUtils.isEmpty(s.getEquipmentAllocationTimes())) {
                s.getEquipmentAllocationTimes().forEach(t -> {
                    t.setEquipmentAllocationSubmission(s);
                    if (!CollectionUtils.isEmpty(t.getEquipmentCostCodePercs())) {
                        t.getEquipmentCostCodePercs().forEach(c -> c.setEquipmentAllocationTime(t));
                    }
                });
            }
        });
        equipmentAllocationSubmissionRepository.saveAll(submissions);
    }

    @Override
    public void saveEquipmentAllocationTimes(Set<EquipmentAllocationTime> equipmentAllocationTimes) {
        equipmentAllocationTimeRepository.saveAll(equipmentAllocationTimes);
    }

    @Override
    public EquipmentWeeklyProcess saveEquipmentWeeklyProcess(EquipmentWeeklyProcess weeklyProcess) {
        return equipmentWeeklyProcessRepository.save(weeklyProcess);
    }

    @Override
    public Equipment[] getAllEquipmentByOwnershipType(EquipmentOwnershipType ownershipType) {
        return equipmentRepository.findByOwnershipType(ownershipType, Sort.by(Sort.Direction.ASC, "equipmentId")).toArray(Equipment[]::new);
    }

    @Override
    public Page<EquipmentPriceDto> getEquipmentPricesByProjectId(Long projectId, PagingRequest pagingRequest) {
        Page<EquipmentPriceDto> page = new Page<>();
        page.setDraw(pagingRequest.getDraw());
        Specification<EquipmentPrice> specificationAll = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(criteriaBuilder.equal(root.get("projectId"), projectId));
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
        List<EquipmentPrice> allEquipmentPrices = equipmentPriceRepository.findAll(specificationAll);
        page.setRecordsTotal(allEquipmentPrices.size());
        Map<String, String> columnMap = new HashMap<>();
        Specification<EquipmentPrice> specification = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(criteriaBuilder.equal(root.get("projectId"), projectId));
            return PagingRequestUtil.getPredicate(root, criteriaQuery, criteriaBuilder, pagingRequest, columnMap,
                    predicates);
        };
        long recordsFiltered;
        List<EquipmentPrice> equipmentPrices;
        Pageable pageable = PagingRequestUtil.Paging2Pageable(pagingRequest, columnMap);
        if (pageable != null) {
            org.springframework.data.domain.Page<EquipmentPrice> rowPage = equipmentPriceRepository.findAll(specification,
                    pageable);
            equipmentPrices = rowPage.getContent();
            recordsFiltered = equipmentPriceRepository.count(specification);
        } else {
            equipmentPrices = equipmentPriceRepository.findAll(specification);
            recordsFiltered = equipmentPrices.size();
        }
        page.setRecordsFiltered(recordsFiltered);
        List<EquipmentPriceDto> rows = equipmentPrices.stream()
                .map(EquipmentPriceDto::new).collect(Collectors.toList());
        page.setData(rows);
        return page;
    }

    @Override
    public EquipmentPrice getEquipmentPriceById(Long id) {
        return equipmentPriceRepository.findById(id).orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public void saveEquipmentPrice(EquipmentPrice equipmentPrice) {
        equipmentPriceRepository.save(equipmentPrice);
    }

    @Override
    public void deleteEquipmentPriceById(Long id) {
        equipmentPriceRepository.deleteById(id);
    }

    @Override
    public Equipment[] getRentalAndBeingUsedEquipments(Long projectId) {
        return equipmentRepository.getRentalAndBeingUsedEquipments(projectId, EquipmentOwnershipType.RENTAL).toArray(Equipment[]::new);
    }

    @Override
    public Equipment[] getAllEquipmentByActive(boolean active) {
        return equipmentRepository.findByActive(active, Sort.by(Sort.Direction.ASC, "equipmentId")).toArray(Equipment[]::new);
    }

    @Override
    public EquipmentUsage[] getEquipmentUsagesByIdsAndDates(Long equipmentId, Long projectId, LocalDate startDate, LocalDate endDate) {
        return equipmentUsageRepository.findByIdsAndDates(equipmentId, projectId, startDate, endDate).toArray(EquipmentUsage[]::new);
    }

    private void checkEquipmentScheduleConflict(List<EquipmentUsage> equipmentUsages) {
        Map<String, List<EquipmentUsage>> equipmentUsagesMap = equipmentUsages.stream()
                .collect(Collectors.groupingBy(e -> e.getEquipment().getEquipmentId(),
                        Collectors.mapping(Function.identity(),
                                Collectors.collectingAndThen(Collectors.toList(),
                                        e -> e.stream()
                                                .sorted(Comparator.comparing(EquipmentUsage::getStartDate))
                                                .collect(Collectors.toList())))));
        LocalDate today = LocalDate.now();
        Map<Long, Set<User>> projectIdUsers = new HashMap<>();
        List<Notification> notifications = new ArrayList<>();
        for (Map.Entry<String, List<EquipmentUsage>> entry : equipmentUsagesMap.entrySet()) {
            if (entry.getValue().size() < 2) continue;
            List<EquipmentUsage> usages = entry.getValue();
            EquipmentUsage firstUsage = usages.get(0);
            Set<Project> projects = new HashSet<>();
            for (int i = 1; i < usages.size(); i++) {
                EquipmentUsage usage = usages.get(i);
                if (usage.getStartDate().isAfter(today)) continue;
                // has conflict
                projects.add(usage.getProject());
            }
            if (!CollectionUtils.isEmpty(projects)) {
                Long firstProjectId = firstUsage.getProjectId();
                for (Project project : projects) {
                    Set<User> users;
                    projectIdUsers.putIfAbsent(project.getId(), accountService.getAdminManagerUsersByProjectId(project.getId()));
                    users = projectIdUsers.get(project.getId());
                    projectIdUsers.putIfAbsent(firstProjectId, accountService.getAdminManagerUsersByProjectId(firstProjectId));
                    users.addAll(projectIdUsers.get(firstProjectId));
                    if (!CollectionUtils.isEmpty(users)) {
                        for (User user : users) {
                            Notification notification = new Notification("System",
                                    String.format("Request to use equipment %s failed", entry.getKey()),
                                    String.format("There was a problem with your request to start to use Equipment %s" +
                                                    " for %s. It's using by %s", entry.getKey(), project.getJobSubJob(),
                                            firstUsage.getProject().getJobSubJob()),
                                    user.getId());
                            notifications.add(notification);
                        }
                    }
                }
            }
        }
        notificationService.saveNotifications(notifications);
    }

    @Override
    public EquipmentAllocationTime[] getEquipmentAllocationTimesByProjectIdAndDateAndStatus(EquipmentAllocationTimeFilter filter) {
        LocalDate startDate = filter.getStartDate();
        LocalDate endDate = filter.getEndDate();
        LocalDate payrollStartDate = filter.getPayrollStartDate();
        LocalDate payrollEndDate = filter.getPayrollEndDate();
        Long projectId = filter.getProjectId();
        EquipmentAllocationSubmissionStatus status = filter.getStatus();
        EquipmentAllocationSubmissionStatus notStatus = filter.getNotStatus();
        boolean ignoreDates = false;
        Set<LocalDate> dates = filter.getDates();
        if (CollectionUtils.isEmpty(dates)) {
            ignoreDates = true;
        }
        Set<EquipmentAllocationTime> equipmentAllocationTimes = equipmentAllocationTimeRepository
                .findByDateRangeAndProjectIdAndStatus(startDate, endDate, ignoreDates, dates, payrollStartDate, payrollEndDate, projectId, status, notStatus);
        return equipmentAllocationTimes.toArray(EquipmentAllocationTime[]::new);

    }

    @Override
    public Set<EquipmentAllocationTime> getEquipmentAllocationTimesById(Set<Long> ids) {
        return equipmentAllocationTimeRepository.findAllByIdFetching(ids);
    }

    @Override
    public EquipmentPrice[] getEquipmentPricesByProjectIdAndClasses(Long projectId, Collection<String> equipmentClasses) {
        if (CollectionUtils.isEmpty(equipmentClasses)) {
            return equipmentPriceRepository.findByProjectId(projectId).toArray(new EquipmentPrice[0]);
        } else {
            return equipmentPriceRepository.findAllByProjectIdAndEquipmentClassIn(projectId, equipmentClasses).toArray(new EquipmentPrice[0]);
        }
    }

    @Override
    public EquipmentWeeklyProcess getEquipmentWeeklyProcessByProjectIdAndDateAndType(Long projectId, LocalDate weekEndDate, EquipmentWeeklyProcessType type) {
        return equipmentWeeklyProcessRepository.findByProjectIdAndWeekEndDateAndType(projectId, weekEndDate, type);
    }

    @Override
    public EquipmentTag[] getEquipmentTags() {
        List<EquipmentTag> equipmentTags = equipmentTagRepository.findAll();
        return equipmentTags.toArray(EquipmentTag[]::new);
    }

    @Override
    public void saveEquipmentTag(String equipmentId, String tag) {
        EquipmentTag equipmentTag = equipmentTagRepository.findByEquipmentIdAndTag(equipmentId, tag);
        if (null == equipmentTag) {
            equipmentTag = new EquipmentTag(null, equipmentId, tag);
            equipmentTagRepository.save(equipmentTag);
        }
    }

    @Override
    public void deleteEquipmentTag(String equipmentId, String tag) {
        equipmentTagRepository.deleteByEquipmentIdAndTag(equipmentId, tag);
    }

    @Override
    public Equipment[] getEquipmentByEquipmentIds(Set<String> equipmentIds) {
        return equipmentRepository.findAllByEquipmentIdIn(equipmentIds).toArray(Equipment[]::new);
    }

    @Override
    public List<Equipment> saveEquipmentList(Collection<Equipment> equipmentList) {
        return equipmentRepository.saveAll(equipmentList);
    }

    @Override
    public EquipmentPrice[] getEquipmentPricesByProjectId(Long projectId) {
        return equipmentPriceRepository.findByProjectId(projectId).toArray(EquipmentPrice[]::new);
    }

    @Override
    public List<EquipmentPrice> saveEquipmentPrices(Collection<EquipmentPrice> equipmentPrices) {
        return equipmentPriceRepository.saveAll(equipmentPrices);
    }

    // TM-275
    @Override
    public void deleteEquipmentCostCodePercBatch(List<EquipmentCostCodePerc> list) {
        equipmentCostCodePercRepository.deleteInBatch(list);
    }

    @Override
    public List<EquipmentAllocationTime> getEquipmentAllocationTimesByDate(Long projectId, LocalDate dateOfService, LocalDate payrollDate,
                                                                           EquipmentAllocationSubmissionStatus status, EquipmentOwnershipType type) {
        return equipmentAllocationTimeRepository.findEquipmentAllocationTimesByDate(projectId, dateOfService, payrollDate, status, type);
    }

    @Override
    public List<EquipmentAllocationTime> getEquipmentAllocationTimesByDateRange(Long projectId, LocalDate startDate,
                                                                                LocalDate endDate,
                                                                                Set<EquipmentAllocationSubmissionStatus> statuses) {
        return equipmentAllocationTimeRepository.findEquipmentAllocationTimesByDateRange(projectId, startDate, endDate, statuses);
    }

    @Override
    public List<Equipment> getEquipmentAllOrByActiveTrue(Boolean isAll) {
        return isAll ? equipmentRepository.findAll() : equipmentRepository.findByActiveTrue();
    }
}
