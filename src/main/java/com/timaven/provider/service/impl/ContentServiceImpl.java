package com.timaven.provider.service.impl;

import com.timaven.provider.controller.BaseController;
import com.timaven.provider.dao.repository.*;
import com.timaven.provider.model.*;
import com.timaven.provider.model.dto.AllocationSubmissionDto;
import com.timaven.provider.model.dto.AllocationTimeFilter;
import com.timaven.provider.model.enums.AllocationSubmissionStatus;
import com.timaven.provider.model.enums.CostCodeType;
import com.timaven.provider.model.enums.WeeklyProcessStatus;
import com.timaven.provider.service.ContentService;
import com.timaven.provider.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Transactional
public class ContentServiceImpl implements ContentService {

    private final UserRepository mUserRepository;
    private final ReportSubmissionRepository mReportSubmissionRepository;
    private final StageTransactionRepository mStageTransactionRepository;
    private final TransSubmissionRepository mTransSubmissionRepository;
    private final AllocationSubmissionRepository mAllocationSubmissionRepository;
    private final AllocationTimeRepository mAllocationTimeRepository;
    private final CraftRepository craftRepository;
    private final EmployeeService employeeService;
    private final RuleRepository ruleRepository;
    private final CostCodeTagAssociationRepository costCodeTagAssociationRepository;
    private final PerDiemRepository perDiemRepository;
    private final CostCodePercRepository costCodePercRepository;
    private final WeeklyProcessRepository weeklyProcessRepository;

    @Autowired
    public ContentServiceImpl(UserRepository userRepository,
                              ReportSubmissionRepository reportSubmissionRepository,
                              StageTransactionRepository stageTransactionRepository,
                              TransSubmissionRepository transSubmissionRepository,
                              AllocationSubmissionRepository allocationSubmissionRepository,
                              AllocationTimeRepository allocationTimeRepository,
                              CraftRepository craftRepository,
                              EmployeeService employeeService, RuleRepository ruleRepository,
                              CostCodeTagAssociationRepository costCodeTagAssociationRepository,
                              PerDiemRepository perDiemRepository, CostCodePercRepository costCodePercRepository,
                              WeeklyProcessRepository weeklyProcessRepository) {
        mUserRepository = userRepository;
        mReportSubmissionRepository = reportSubmissionRepository;
        mStageTransactionRepository = stageTransactionRepository;
        mTransSubmissionRepository = transSubmissionRepository;
        mAllocationSubmissionRepository = allocationSubmissionRepository;
        mAllocationTimeRepository = allocationTimeRepository;
        this.craftRepository = craftRepository;
        this.employeeService = employeeService;
        this.ruleRepository = ruleRepository;
        this.costCodeTagAssociationRepository = costCodeTagAssociationRepository;
        this.perDiemRepository = perDiemRepository;
        this.costCodePercRepository = costCodePercRepository;
        this.weeklyProcessRepository = weeklyProcessRepository;
    }

    @Override
    public AllocationSubmission saveAllocationSubmission(AllocationSubmission allocationSubmission) {
        allocationSubmission.getAllocationTimes().forEach(at -> {
            at.setAllocationSubmission(allocationSubmission);
            Set<AllocationRecord> allocationRecordSet = at.getAllocationRecords();
            if (!CollectionUtils.isEmpty(allocationRecordSet)) {
                allocationRecordSet.forEach(ar -> {
                    ar.setAllocationTime(at);
                });
            }
            Set<PunchException> punchExceptionSet = at.getPunchExceptions();
            if (!CollectionUtils.isEmpty(punchExceptionSet)) {
                punchExceptionSet.forEach(e -> {
                    e.setAllocationTime(at);
                });
            }
            Set<AllocationException> allocationExceptions = at.getAllocationExceptions();
            if (!CollectionUtils.isEmpty(allocationExceptions)) {
                allocationExceptions.forEach(e -> e.setAllocationTime(at));
            }
            Set<CostCodePerc> costCodePercs = at.getCostCodePercs();
            if (!CollectionUtils.isEmpty(costCodePercs)) {
                costCodePercs.forEach(ccp -> ccp.setAllocationTime(at));
            }
        });
        try {
            AllocationSubmission submission;
            if (allocationSubmission.getId() != null) {
                List<AllocationTime> allocationTimes =
                        mAllocationTimeRepository.findByAllocationSubmissionId(allocationSubmission.getId());
                Set<Long> allocationTimeIds = allocationTimes.stream()
                        .map(AllocationTime::getId)
                        .collect(Collectors.toSet());
                if (!CollectionUtils.isEmpty(allocationTimeIds)) {
                    costCodeTagAssociationRepository.deleteByKeyInAndCostCodeType(allocationTimeIds, CostCodeType.MOB);
                    costCodeTagAssociationRepository.deleteByKeyInAndCostCodeType(allocationTimeIds,
                            CostCodeType.PERDIEM);
                    List<CostCodePerc> costCodePercs =
                            costCodePercRepository.findByAllocationTimeIdIn(allocationTimeIds);
                    if (!CollectionUtils.isEmpty(costCodePercs)) {
                        Set<Long> costCodePercIds = costCodePercs.stream()
                                .map(CostCodePerc::getId)
                                .collect(Collectors.toSet());
                        costCodeTagAssociationRepository.deleteByKeyInAndCostCodeType(costCodePercIds,
                                CostCodeType.DEFAULT);
                        costCodePercRepository.deleteAll(costCodePercs);
                    }
                }
                submission = mAllocationSubmissionRepository.save(allocationSubmission);
            } else {
                List<AllocationSubmission> allocationSubmissions =
                        mAllocationSubmissionRepository.findByProjectIdAndTeamNameAndDateOfServiceAndPayrollDateAndStatusNot(allocationSubmission.getProjectId(),
                                allocationSubmission.getTeamName(), allocationSubmission.getDateOfService(), allocationSubmission.getPayrollDate(), AllocationSubmissionStatus.DENIED);
                // Invalid submission
                if (!CollectionUtils.isEmpty(allocationSubmissions) && allocationSubmissions.get(0).getStatus() == AllocationSubmissionStatus.APPROVED)
                    return null;
                if (allocationSubmission.getIsManual() && !CollectionUtils.isEmpty(allocationSubmission.getAllocationTimes())) {
                    setAllocationTimeDefaultValues(allocationSubmission.getAllocationTimes(), allocationSubmission.getProjectId());
                }
                if (!CollectionUtils.isEmpty(allocationSubmissions)) {
                    // Merge submission
                    AllocationSubmission existingSubmission = allocationSubmissions.get(0);
                    Map<String, AllocationTime> existingAllocationTimes = existingSubmission.getAllocationTimes().stream()
                            .collect(Collectors.toMap(AllocationTime::getEmpId, Function.identity(), (e1, e2) -> e1.getId() > e2.getId() ? e1 : e2));

                    allocationSubmission.getAllocationTimes().forEach(at -> {
                        if (existingAllocationTimes.containsKey(at.getEmpId())) {
                            AllocationTime allocationTime = existingAllocationTimes.get(at.getEmpId());
                            allocationTime.setStHour(allocationTime.getStHour().add(at.getStHour()));
                            allocationTime.setOtHour(allocationTime.getOtHour().add(at.getOtHour()));
                            allocationTime.setDtHour(allocationTime.getDtHour().add(at.getDtHour()));
                            allocationTime.setTotalHour(allocationTime.getTotalHour().add(at.getTotalHour()));
                            allocationTime.setNetHour(allocationTime.getNetHour().add(at.getNetHour()));
                        } else {
                            at.setAllocationSubmission(existingSubmission);
                        }
                    });
                    allocationSubmission.getAllocationTimes().removeIf(t -> existingAllocationTimes.containsKey(t.getEmpId()));
                    existingSubmission.getAllocationTimes().addAll(allocationSubmission.getAllocationTimes());
                    mAllocationSubmissionRepository.save(existingSubmission);
                    submission = existingSubmission;
                } else {
                    if (allocationSubmission.getIsManual()) {
                        // No existing allocation submission. But Might exist stageTransactions
                        AllocationSubmission existingSubmission = getNewAllocationSubmissionByDateOfService(allocationSubmission.getSubmitUserId(), allocationSubmission.getProjectId(), allocationSubmission.getTeamName(), allocationSubmission.getDateOfService());
                        if (!CollectionUtils.isEmpty(existingSubmission.getAllocationTimes())) {
                            Map<String, AllocationTime> existingAllocationTimes = existingSubmission.getAllocationTimes().stream()
                                    .collect(Collectors.toMap(AllocationTime::getEmpId, Function.identity(), (e1, e2) -> e1.getId() > e2.getId() ? e1 : e2));

                            allocationSubmission.getAllocationTimes().forEach(at -> {
                                if (existingAllocationTimes.containsKey(at.getEmpId())) {
                                    AllocationTime allocationTime = existingAllocationTimes.get(at.getEmpId());
                                    allocationTime.setStHour(allocationTime.getStHour().add(at.getStHour()));
                                    allocationTime.setOtHour(allocationTime.getOtHour().add(at.getOtHour()));
                                    allocationTime.setDtHour(allocationTime.getDtHour().add(at.getDtHour()));
                                    allocationTime.setTotalHour(allocationTime.getTotalHour().add(at.getTotalHour()));
                                    allocationTime.setNetHour(allocationTime.getNetHour().add(at.getNetHour()));
                                } else {
                                    at.setAllocationSubmission(existingSubmission);
                                }
                            });
                            allocationSubmission.getAllocationTimes().removeIf(t -> existingAllocationTimes.containsKey(t.getEmpId()));
                            existingSubmission.getAllocationTimes().addAll(allocationSubmission.getAllocationTimes());
                            mAllocationSubmissionRepository.save(existingSubmission);
                            submission = existingSubmission;
                        } else {
                            submission = mAllocationSubmissionRepository.save(allocationSubmission);
                        }
                    } else {
                        submission = mAllocationSubmissionRepository.save(allocationSubmission);
                    }
                }
            }

            List<CostCodeTagAssociation> costCodeTagAssociations = new ArrayList<>();
            for (AllocationTime allocationTime : allocationSubmission.getAllocationTimes()) {
                List<CostCodeTagAssociation> mobCostCodeTagAssociations = allocationTime.getMobCostCodeTagAssociations();
                List<CostCodeTagAssociation> perDiemCostCodeTagAssociations = allocationTime.getPerDiemCostCodeTagAssociations();
                if (!CollectionUtils.isEmpty(mobCostCodeTagAssociations)) {
                    mobCostCodeTagAssociations.forEach(a -> a.setKey(allocationTime.getId()));
                    costCodeTagAssociations.addAll(mobCostCodeTagAssociations);
                }
                if (!CollectionUtils.isEmpty(perDiemCostCodeTagAssociations)) {
                    perDiemCostCodeTagAssociations.forEach(a -> a.setKey(allocationTime.getId()));
                    costCodeTagAssociations.addAll(perDiemCostCodeTagAssociations);
                }
            }
            Set<CostCodePerc> costCodePercsWithoutAssociations = allocationSubmission.getAllocationTimes()
                    .stream()
                    .map(AllocationTime::getCostCodePercs)
                    .filter(Objects::nonNull)
                    .flatMap(Collection::stream)
                    .filter(ccp -> CollectionUtils.isEmpty(ccp.getCostCodeTagAssociations()))
                    .collect(Collectors.toSet());
            costCodePercRepository.saveAll(costCodePercsWithoutAssociations);
            Set<CostCodePerc> costCodePercsWithAssociations = allocationSubmission.getAllocationTimes().stream()
                    .map(AllocationTime::getCostCodePercs)
                    .filter(Objects::nonNull)
                    .flatMap(Collection::stream)
                    .filter(ccp -> !CollectionUtils.isEmpty(ccp.getCostCodeTagAssociations()))
                    .collect(Collectors.toSet());
            if (!CollectionUtils.isEmpty(costCodePercsWithAssociations)) {
                for (CostCodePerc costCodePerc : costCodePercsWithAssociations) {
                    CostCodePerc managedCostCodePerc = costCodePercRepository.save(costCodePerc);
                    List<CostCodeTagAssociation> associations = costCodePerc.getCostCodeTagAssociations();
                    associations.forEach(a -> a.setKey(managedCostCodePerc.getId()));
                    costCodeTagAssociations.addAll(associations);
                }
            }
            // If Persist X, X becomes managed state and will have an id. If Merge X, a copy X' will be returned.
            costCodeTagAssociationRepository.saveAll(costCodeTagAssociations);
            Set<StageTransaction> stageTransactions = mStageTransactionRepository.findByProjectIdTeamAndDateOfServiceAndStatus(allocationSubmission.getProjectId(), allocationSubmission.getTeamName(), allocationSubmission.getDateOfService(), StageTransaction.Status.Pending);
            if (!CollectionUtils.isEmpty(stageTransactions)) {
                stageTransactions.forEach(st -> {
                    st.setStatus(StageTransaction.Status.Processed);
                    st.setProcessedAt(LocalDateTime.now());
                });
            }
            return submission;
        } catch (DataIntegrityViolationException ignore) {
            // Ignore duplicate violation
        }
        return null;
    }

    @Override
    public AllocationSubmissionDto getLastAllocationSubmissionDto(Long projectId, String teamName, LocalDate dateOfService) {
        AllocationSubmission latestSubmission = mAllocationSubmissionRepository.findTopByProjectIdAndTeamNameAndDateOfServiceBeforeAndIsBySystemFalseOrderByDateOfServiceDesc(projectId, teamName, dateOfService);
        if (null != latestSubmission) {
            AllocationSubmission latestSubmissionFetch = mAllocationSubmissionRepository.getAllocationSubmissionFetchingCostCodePercs(latestSubmission.getId());
            Set<AllocationTime> allocationTimes = latestSubmissionFetch.getAllocationTimes();
            if (!CollectionUtils.isEmpty(allocationTimes)) {
                fetchCostCodeTagAssociations(allocationTimes);
            }
            return new AllocationSubmissionDto(latestSubmissionFetch);
        }
        return null;
    }

    @Override
    public void deleteAllocationTimeById(Long id) {
        mAllocationTimeRepository.deleteById(id);
    }

    @Override
    public void deleteStageTransactionsByProjectIdAndEmpIdAndTeamAndDateOfService(Long projectId, String empId, String team, LocalDate dateOfService) {
        team = team == null ? "" : team;
        mStageTransactionRepository.deleteStageTransactionsByProjectIdAndEmpIdAndTeamAndDateOfService(projectId, empId, team, dateOfService);
    }

    @Override
    public void updateAllocationSubmissionExport(boolean exported, Collection<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) return;
        List<AllocationSubmission> submissions = mAllocationSubmissionRepository.findAllById(ids);
        submissions.forEach(s -> s.setExported(exported));
    }

    @Override
    public void updateWeeklyProcessesStatus(Collection<Long> ids, WeeklyProcessStatus status) {
        if (CollectionUtils.isEmpty(ids)) return;
        List<WeeklyProcess> weeklyProcesses = weeklyProcessRepository.findAllById(ids);
        List<AllocationSubmission> submissions = mAllocationSubmissionRepository.findAllByWeeklyProcessId(ids);
        final LocalDateTime now = LocalDateTime.now();
        Long userId = BaseController.getUserId();
        weeklyProcesses.forEach(weeklyProcess -> {
            switch (status) {
                case REJECT -> {
                    weeklyProcess.setFinalizedAt(null);
                    weeklyProcess.setFinalizeUserId(null);
                    weeklyProcess.setApprovedAt(null);
                    weeklyProcess.setApproveUserId(null);

                    // TM-385 clear report status
                    weeklyProcess.setReportGeneratedAt(null);
                    weeklyProcess.setReportUserId(null);

                    weeklyProcess.setRejectAt(now);
                    weeklyProcess.setRejectUserId(userId);
                }
                case FINALIZE -> {
                    weeklyProcess.setRejectUserId(null);
                    weeklyProcess.setRejectAt(null);

                    weeklyProcess.setFinalizedAt(now);
                    weeklyProcess.setFinalizeUserId(userId);
                }
                case UNFINALIZE -> {
                    weeklyProcess.setFinalizedAt(null);
                    weeklyProcess.setFinalizeUserId(null);
                }
            }
        });
        if (!CollectionUtils.isEmpty(submissions)) {
            submissions.forEach(s -> {
                switch (status) {
                    case REJECT, UNFINALIZE -> {
                        s.setFinalizedAt(null);
                        s.setFinalizeUserId(null);
                        s.setExported(false);
                    }
                    case FINALIZE -> {
                        s.setFinalizedAt(LocalDateTime.now());
                        s.setFinalizeUserId(BaseController.getUserId());
                    }
                }
            });
        }
    }

    @Override
    public AllocationSubmissionDto getAllocationSubmissionDtoById(Long allocationSubmissionId) {
        AllocationSubmission allocationSubmission =
                mAllocationSubmissionRepository.getAllocationSubmissionFetchAll(allocationSubmissionId);
        Long projectId = allocationSubmission.getProjectId();
        Set<AllocationTime> allocationTimes = allocationSubmission.getAllocationTimes();
        if (!CollectionUtils.isEmpty(allocationTimes)) {
            fetchEmployeeAndCraft(allocationTimes, projectId);
            fetchCostCodeTagAssociations(allocationTimes);
        }
        return new AllocationSubmissionDto(allocationSubmission);
    }

    private void fetchCostCodeTagAssociations(Set<AllocationTime> allocationTimes) {
        Set<CostCodePerc> costCodePercs = allocationTimes.stream()
                .map(AllocationTime::getCostCodePercs)
                .filter(ccp -> !CollectionUtils.isEmpty(ccp))
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
        Set<Long> costCodePercKeys = costCodePercs.stream()
                .map(CostCodePerc::getId).collect(Collectors.toSet());
        if (!CollectionUtils.isEmpty(costCodePercKeys)) {
            Set<CostCodeTagAssociation> costCodeTagAssociations = costCodeTagAssociationRepository.findByKeyInAndCostCodeType(costCodePercKeys, CostCodeType.DEFAULT);
            if (!CollectionUtils.isEmpty(costCodeTagAssociations)) {
                Map<Long, List<CostCodeTagAssociation>> keyAssociationMap = costCodeTagAssociations.stream()
                        .collect(Collectors.groupingBy(CostCodeTagAssociation::getKey));
                costCodePercs.forEach(ccp -> {
                    ccp.setCostCodeTagAssociations(keyAssociationMap.getOrDefault(ccp.getId(), new ArrayList<>()));
                });
            }
        }
        Set<Long> allocationTimeIds = allocationTimes.stream()
                .map(AllocationTime::getId)
                .collect(Collectors.toSet());
        if (!CollectionUtils.isEmpty(allocationTimeIds)) {
            Set<CostCodeTagAssociation> mobCostCodeTagAssociations = costCodeTagAssociationRepository.findByKeyInAndCostCodeType(allocationTimeIds, CostCodeType.MOB);
            if (!CollectionUtils.isEmpty(mobCostCodeTagAssociations)) {
                Map<Long, List<CostCodeTagAssociation>> keyAssociationMap = mobCostCodeTagAssociations.stream()
                        .collect(Collectors.groupingBy(CostCodeTagAssociation::getKey));
                allocationTimes.forEach(t -> {
                    t.setMobCostCodeTagAssociations(keyAssociationMap.getOrDefault(t.getId(), new ArrayList<>()));
                });
            }
            Set<CostCodeTagAssociation> perDiemCostCodeTagAssociations = costCodeTagAssociationRepository.findByKeyInAndCostCodeType(allocationTimeIds, CostCodeType.PERDIEM);
            if (!CollectionUtils.isEmpty(perDiemCostCodeTagAssociations)) {
                Map<Long, List<CostCodeTagAssociation>> keyAssociationMap = perDiemCostCodeTagAssociations.stream()
                        .collect(Collectors.groupingBy(CostCodeTagAssociation::getKey));
                allocationTimes.forEach(t -> {
                    t.setPerDiemCostCodeTagAssociations(keyAssociationMap.getOrDefault(t.getId(), new ArrayList<>()));
                });
            }
        }
    }

    private void fetchUser(Set<AllocationSubmission> allocationSubmissions) {
        for (AllocationSubmission allocationSubmission : allocationSubmissions) {
            Long submitUserId = allocationSubmission.getSubmitUserId();
            if (null != submitUserId) {
                allocationSubmission.setSubmitUser(mUserRepository.findById(submitUserId).orElseThrow(RuntimeException::new));
            }
            Long approveUserId = allocationSubmission.getApproveUserId();
            if (null != approveUserId) {
                allocationSubmission.setApproveUser(mUserRepository.findById(approveUserId).orElseThrow(RuntimeException::new));
            }
        }
    }

    @Override
    public void saveReports(Long userId, String teamName, List<ReportRecord> reportRecords, Long projectId) {
        ReportSubmission reportSubmission = new ReportSubmission(userId, teamName, projectId);
        Set<ReportPerc> reportPercSet = new LinkedHashSet<>();
        for (ReportRecord record : reportRecords) {
            ReportPerc reportPerc = new ReportPerc(record.getCostCode(), record.getPurchaseOrder(),
                    record.getPercentage());
            reportPerc.setReportSubmission(reportSubmission);
            reportPercSet.add(reportPerc);
        }
        reportSubmission.setReportPercs(reportPercSet);
        mReportSubmissionRepository.save(reportSubmission);
    }

    @Override
    public TransSubmission saveTransSubmission(TransSubmission transSubmission) {
        Set<StageTransaction> stageTransactions = transSubmission.getStageTransactions();
        stageTransactions.forEach(s -> {
            s.setTransSubmission(transSubmission);
            if (!CollectionUtils.isEmpty(s.getStageExceptions())) {
                s.getStageExceptions().forEach(e -> e.setStageTransaction(s));
            }
            if (!CollectionUtils.isEmpty(s.getStageRecords())) {
                s.getStageRecords().forEach(r -> r.setStageTransaction(s));
            }
        });
        return mTransSubmissionRepository.save(transSubmission);
    }

    @Override
    public List<StageTransaction> getStageTransactions(Long projectId, String teamName, LocalDate dateOfService) {
        return mStageTransactionRepository.findByProjectIdTeamAndDateOfService(projectId, teamName, dateOfService);
    }


    @Override
    public void saveAllocationTimes(Set<AllocationTime> allocationTimes) {
        mAllocationTimeRepository.saveAll(allocationTimes);
        allocationTimes.forEach(at -> {
            Set<CostCodePerc> costCodePercs = at.getCostCodePercs();
            if (!CollectionUtils.isEmpty(costCodePercs)) {
                costCodePercs.forEach(ccp -> ccp.setAllocationTime(at));
            }
        });
        Set<CostCodePerc> costCodePercs = allocationTimes.stream()
                .map(AllocationTime::getCostCodePercs)
                .filter(ccp -> !CollectionUtils.isEmpty(ccp))
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
        costCodePercRepository.saveAll(costCodePercs);
    }

    @Override
    public Set<AllocationTime> getAllocationTimesById(Long projectId, Set<Long> allocationTimeIds, Set<String> includes) {
        Set<AllocationTime> allocationTimes;
        if (includes != null && includes.contains("costCodePerc")) {
            allocationTimes = mAllocationTimeRepository.findAllByIdFetchingCostCodePerc(allocationTimeIds);
        } else {
            allocationTimes = mAllocationTimeRepository.findAllByIdFetchingAllocationSubmission(allocationTimeIds);
        }
        if (includes != null && includes.contains("crafts")) {
            fetchEmployeeAndCraft(allocationTimes, projectId);
        }
        return allocationTimes;
    }

    @Override
    public void fetchEmployeeAndCraft(Set<AllocationTime> allocationTimes, Long projectId) {
        fetchEmployeeAndCraft(allocationTimes, projectId, false);
    }

    @Override
    public void fetchEmployeeAndCraft(Set<AllocationTime> allocationTimes, Long projectId, boolean excludePerDiem) {
        if (allocationTimes == null || allocationTimes.size() == 0) return;
        Optional<LocalDate> startDateOpt = allocationTimes.stream()
                .map(AllocationTime::getAllocationSubmission)
                .map(AllocationSubmission::getDateOfService)
                .min(LocalDate::compareTo);
        Optional<LocalDate> endDateOpt = allocationTimes.stream()
                .map(AllocationTime::getAllocationSubmission)
                .map(AllocationSubmission::getDateOfService)
                .max(LocalDate::compareTo);
        if (endDateOpt.isEmpty()) return;
        LocalDate startDate = startDateOpt.get();
        LocalDate endDate = endDateOpt.get();

        Set<Craft> crafts = craftRepository.findByProjectIdAndDateRange(projectId, startDate, endDate);

        Map<String, List<Craft>> codeCraftsMap = crafts.stream()
                .collect(Collectors.groupingBy(Craft::getCode));

        Set<String> empIds = allocationTimes.stream()
                .map(AllocationTime::getEmpId)
                .filter(StringUtils::hasText)
                .collect(Collectors.toSet());
        Set<String> badges = allocationTimes.stream()
                .map(AllocationTime::getBadge)
                .filter(StringUtils::hasText)
                .collect(Collectors.toSet());
        Set<String> clientEmpIds = allocationTimes.stream()
                .map(AllocationTime::getClientEmpId)
                .filter(StringUtils::hasText)
                .collect(Collectors.toSet());

        EmployeeFilter filter = new EmployeeFilter(projectId, null, empIds, badges, clientEmpIds, startDate, endDate);
        Set<Employee> employees = employeeService.findEmployees(filter);

        Map<Long, PerDiem> idPerDiemMap = new HashMap<>();
        Rule rule = null;
        if (!excludePerDiem) {
            rule = ruleRepository.getRuleByProjectId(projectId);
            Set<Long> employeePerDiemIds = employees.stream()
                    .map(Employee::getPerDiemId)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());
            Set<Long> craftPerDiemIds = crafts.stream()
                    .map(Craft::getPerDiemId)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());
            Set<Long> perDiemIds = Stream.of(employeePerDiemIds, craftPerDiemIds).flatMap(Collection::stream).collect(Collectors.toSet());
            if (rule.getPerDiemId() != null) {
                perDiemIds.add(rule.getProjectId());
            }

            List<PerDiem> perDiems = perDiemRepository.findAllById(perDiemIds);
            idPerDiemMap = perDiems.stream()
                    .collect(Collectors.toMap(PerDiem::getId, Function.identity()));
        }

        Map<String, List<Employee>> idEmployeesMap = employees.stream()
                .filter(e -> StringUtils.hasText(e.getEmpId()))
                .collect(Collectors.groupingBy(Employee::getEmpId));
        Map<String, List<Employee>> badgeEmployeesMap = employees.stream()
                .filter(e -> StringUtils.hasText(e.getBadge()))
                .collect(Collectors.groupingBy(Employee::getBadge));
        Map<String, List<Employee>> clientEmployeesMap = employees.stream()
                .filter(e -> StringUtils.hasText(e.getClientEmpId()))
                .collect(Collectors.groupingBy(Employee::getClientEmpId));

        for (AllocationTime allocationTime : allocationTimes) {
            LocalDate dateOfService = allocationTime.getAllocationSubmission().getDateOfService();

            String empId = allocationTime.getEmpId();
            String badge = allocationTime.getBadge();
            String clientEmpId = allocationTime.getClientEmpId();

            Optional<Employee> employeeOpt = Optional.empty();
            if (StringUtils.hasText(empId)) {
                employeeOpt = idEmployeesMap.getOrDefault(empId, new ArrayList<>()).stream()
                        .filter(e -> (e.getEffectedOn().isBefore(dateOfService)
                                || e.getEffectedOn().isEqual(dateOfService)))
                        .max(Comparator.comparing(Employee::getCreatedAt,
                                Comparator.nullsFirst(Comparator.naturalOrder())));
            }
            if (employeeOpt.isEmpty() && StringUtils.hasText(badge)) {
                employeeOpt = badgeEmployeesMap.getOrDefault(badge, new ArrayList<>()).stream()
                        .filter(e -> (e.getEffectedOn().isBefore(dateOfService)
                                || e.getEffectedOn().isEqual(dateOfService)))
                        .max(Comparator.comparing(Employee::getCreatedAt,
                                Comparator.nullsFirst(Comparator.naturalOrder())));
            }
            if (employeeOpt.isEmpty() && StringUtils.hasText(clientEmpId)) {
                employeeOpt = clientEmployeesMap.getOrDefault(clientEmpId, new ArrayList<>()).stream()
                        .filter(e -> (e.getEffectedOn().isBefore(dateOfService)
                                || e.getEffectedOn().isEqual(dateOfService)))
                        .max(Comparator.comparing(Employee::getCreatedAt,
                                Comparator.nullsFirst(Comparator.naturalOrder())));
            }

            Employee employee = null;
            Craft craft = null;
            if (employeeOpt.isPresent() && (employeeOpt.get().getHiredAt().isBefore(dateOfService)
                    || employeeOpt.get().getHiredAt().isEqual(dateOfService))
                    && (employeeOpt.get().getTerminatedAt() == null
                    || employeeOpt.get().getTerminatedAt().isAfter(dateOfService)
                    || employeeOpt.get().getTerminatedAt().isEqual(dateOfService))) {
                try {
                    employee = (Employee) employeeOpt.get().clone();
                    List<Craft> craftList = codeCraftsMap.getOrDefault(employee.getCraft(), new ArrayList<>());
                    Optional<Craft> craftOpt = craftList.stream()
                            .filter(c -> (c.getStartDate().isBefore(dateOfService)
                                    || c.getStartDate().isEqual(dateOfService)))
                            .max(Comparator.comparing(Craft::getCreatedAt, Comparator.nullsFirst(Comparator.naturalOrder())));
                    if (craftOpt.isPresent() && (craftOpt.get().getEndDate() == null
                            || craftOpt.get().getEndDate().isAfter(dateOfService)
                            || craftOpt.get().getEndDate().isEqual(dateOfService))) {
                        craft = (Craft) craftOpt.get().clone();
                    }
                } catch (CloneNotSupportedException e) {
                    e.printStackTrace();
                }
            }
            if (employee == null) {
                employee = new Employee();
                employee.setCrew("Unknown");
                employee.setCraft("Unknown");
                employee.setBaseST(BigDecimal.ZERO);
                employee.setBaseOT(BigDecimal.ZERO);
                employee.setBaseDT(BigDecimal.ZERO);
                employee.setInvalidReason("This employee does not on the roster.");
                employee.setActive(false);
            }

            if (craft == null) {
                craft = new Craft();
                craft.setProjectId(projectId);
                craft.setCode("Unknown");
                craft.setBillableST(BigDecimal.ZERO);
                craft.setBillableOT(BigDecimal.ZERO);
                craft.setBillableDT(BigDecimal.ZERO);
                craft.setPerDiem(BigDecimal.ZERO);
                craft.setRigPay(BigDecimal.ZERO);
            }

            employee.setCraftEntity(craft);

            allocationTime.setEmployee(employee);
            if (!excludePerDiem) {
                if (employee.getPerDiemId() != null && idPerDiemMap.containsKey(employee.getPerDiemId())) {
                    allocationTime.setPerDiem(idPerDiemMap.get(employee.getPerDiemId()));
                } else if (craft.getPerDiemId() != null && idPerDiemMap.containsKey(craft.getPerDiemId())) {
                    allocationTime.setPerDiem(idPerDiemMap.get(craft.getPerDiemId()));
                } else if (rule.getPerDiemId() != null && idPerDiemMap.containsKey(rule.getPerDiemId())) {
                    allocationTime.setPerDiem(idPerDiemMap.get(rule.getPerDiemId()));
                } else {
                    PerDiem perDiem = new PerDiem();
                    perDiem.setName("No perdiem");
                    perDiem.setProjectId(projectId);
                    perDiem.setDailyThreshold(1000);
                    perDiem.setWeeklyThreshold(1000);
                    allocationTime.setPerDiem(perDiem);
                }
            }
        }
    }

    @Override
    public void saveAllocationSubmissions(Set<AllocationSubmission> allocationSubmissions) {
        // TM-373 add allocation time id
        allocationSubmissions.forEach(e -> {
            if (!CollectionUtils.isEmpty(e.getAllocationTimes())) {
                e.getAllocationTimes().forEach(all -> {
                    if (!CollectionUtils.isEmpty(all.getActivityCodePercs())) {
                        all.getActivityCodePercs().forEach(activityCodePerc -> {
                            activityCodePerc.setAllocationTime(all);
                        });
                    }
                });
            }
        });
        allocationSubmissions.forEach(s -> {
            if (!CollectionUtils.isEmpty(s.getAllocationTimes())) {
                s.getAllocationTimes().forEach(t -> {
                    t.setAllocationSubmission(s);
                    if (!CollectionUtils.isEmpty(t.getCostCodePercs())) {
                        t.getCostCodePercs().forEach(c -> c.setAllocationTime(t));
                    }
                });
            }
        });
        // If generated by system, allow them to be merged to existing allocation submissions
        Set<AllocationSubmission> systemAllocationSubmissions = allocationSubmissions.stream()
                .filter(s -> s.getIsBySystem() != null && s.getIsBySystem())
                .collect(Collectors.toSet());
        if (!CollectionUtils.isEmpty(systemAllocationSubmissions)) {
            List<AllocationTime> allocationTimesToBeAdded = new ArrayList<>();
            for (AllocationSubmission submission : systemAllocationSubmissions) {
                List<AllocationSubmission> existingSubmissions = mAllocationSubmissionRepository
                        .findByProjectIdAndTeamNameAndDateOfServiceAndPayrollDateAndStatusNot(submission.getProjectId(),
                                submission.getTeamName(), submission.getDateOfService(), submission.getPayrollDate(), AllocationSubmissionStatus.DENIED);
                if (!CollectionUtils.isEmpty(existingSubmissions)) {
                    // Merge
                    allocationSubmissions.remove(submission);
                    AllocationSubmission existingSubmission = existingSubmissions.get(0);
                    Map<String, AllocationTime> empIdTime = existingSubmission.getAllocationTimes().stream()
                            .collect(Collectors.toMap(AllocationTime::getEmpId, Function.identity(), (e1, e2) -> e2));
                    for (AllocationTime newAllocationTime : submission.getAllocationTimes()) {
                        // Update existing allocation time to have perdiem
                        if (empIdTime.containsKey(newAllocationTime.getEmpId())) {
                            AllocationTime existingTime = empIdTime.get(newAllocationTime.getEmpId());
                            if (!existingTime.isHasPerDiem()) {
                                existingTime.setHasPerDiem(true);
                                existingTime.setPerDiemCostCode(newAllocationTime.getPerDiemCostCode());
                                existingTime.setPerDiemAmount(newAllocationTime.getPerDiemAmount());
                            }
                        } else {
                            newAllocationTime.setAllocationSubmission(existingSubmission);
                            allocationTimesToBeAdded.add(newAllocationTime);
                        }
                    }
                }
            }
            mAllocationTimeRepository.saveAll(allocationTimesToBeAdded);
        }
        mAllocationSubmissionRepository.saveAll(allocationSubmissions);
    }

    @Override
    public AllocationSubmission[] getAllocationSubmissionsByProjectIdAndDateAndStatus(LocalDate startDate,
                                                                                      LocalDate endDate,
                                                                                      Long projectId,
                                                                                      String teamName,
                                                                                      AllocationSubmissionStatus status,
                                                                                      AllocationSubmissionStatus notStatus,
                                                                                      List<String> includes) {
        Set<AllocationSubmission> allocationSubmissions;
        if (includes == null) includes = new ArrayList<>();
        if (includes.contains("allocationTimes") || includes.contains("crafts")) {
            allocationSubmissions = mAllocationSubmissionRepository
                    .getAllocationSubmissionsFetchingAllocationTimes(startDate, endDate, projectId, teamName, status, notStatus);
            if (includes.contains("crafts")) {
                Set<AllocationTime> allocationTimes = allocationSubmissions.stream()
                        .map(AllocationSubmission::getAllocationTimes)
                        .flatMap(Collection::stream)
                        .collect(Collectors.toSet());
                fetchEmployeeAndCraft(allocationTimes, projectId);
            }
        } else {
            allocationSubmissions = mAllocationSubmissionRepository
                    .getAllocationSubmissions(startDate, endDate, projectId, teamName, status, notStatus);
        }
        if (includes.contains("users")) {
            fetchUser(allocationSubmissions);
        }
        return allocationSubmissions.toArray(AllocationSubmission[]::new);
    }

    @Override
    public StageTransaction[] getStageTransactions(Long projectId, String teamName, LocalDate startDate, LocalDate endDate, List<String> includes) {
        if (CollectionUtils.isEmpty(includes)) includes = new ArrayList<>();
        if (includes.contains("stageExceptions") || includes.contains("stageRecords")) {
            return mStageTransactionRepository.findByProjectIdAndTeamAndDatesFetching(projectId, teamName, startDate, endDate, StageTransaction.Status.Pending).toArray(StageTransaction[]::new);
        } else {
            return mStageTransactionRepository.findByProjectIdAndTeamAndDates(projectId, teamName, startDate, endDate, StageTransaction.Status.Pending).toArray(StageTransaction[]::new);
        }
    }

    @Override
    public TransSubmission[] getTransSubmissionsByProjectIdAndDates(Long projectId, LocalDate startDate, LocalDate endDate, List<String> includes) {
        List<TransSubmission> transSubmissions;
        LocalDateTime startDateTime = startDate == null ? null : startDate.atStartOfDay();
        LocalDateTime endDateTime = endDate == null ? null : endDate.atStartOfDay();
        if (includes == null) includes = new ArrayList<>();
        if (includes.contains("stageTransactions")) {
            transSubmissions = mTransSubmissionRepository.findTransSubmissionByProjectIdAndDatesFetching(projectId, startDateTime, endDateTime);
        } else {
            transSubmissions = mTransSubmissionRepository.findTransSubmissionByProjectIdAndDates(projectId, startDateTime, endDateTime);
        }
        return transSubmissions.toArray(TransSubmission[]::new);
    }

    @Override
    public ReportSubmission[] getReportSubmissions(Long projectId, Long userId, String teamName, LocalDate startDate, LocalDate endDate, List<String> includes) {
        List<ReportSubmission> reportSubmissions;
        if (includes == null) includes = new ArrayList<>();
        if (includes.contains("reportPercs")) {
            reportSubmissions = mReportSubmissionRepository.findReportSubmissionsByIdsAndTeamAndDatesFetching(projectId, userId, teamName, startDate.atStartOfDay(), endDate == null ? null : endDate.atStartOfDay());
        } else {
            reportSubmissions = mReportSubmissionRepository.findReportSubmissionsByIdsAndTeamAndDates(projectId, userId, teamName, startDate.atStartOfDay(), endDate == null ? null : endDate.atStartOfDay());
        }
        return reportSubmissions.toArray(ReportSubmission[]::new);
    }

    @Override
    public AllocationSubmission getAllocationSubmissionById(Long id) {
        return mAllocationSubmissionRepository.findById(id).orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public AllocationTime[] getAllocationTimesByProjectIdAndDateAndStatus(AllocationTimeFilter filter) {
        LocalDate startDate = filter.getStartDate();
        LocalDate endDate = filter.getEndDate();
        LocalDate payrollStartDate = filter.getPayrollStartDate();
        LocalDate payrollEndDate = filter.getPayrollEndDate();
        Long projectId = filter.getProjectId();
        String teamName = filter.getTeamName();
        Boolean isAllProjects = filter.getIsAllProjects();
        isAllProjects = isAllProjects != null && isAllProjects;
        AllocationSubmissionStatus status = filter.getStatus();
        AllocationSubmissionStatus notStatus = filter.getNotStatus();
        List<String> includes = filter.getIncludes();
        String shift = filter.getShift();
        Set<String> empIds = filter.getEmpIds();
        boolean ignoreEmpId = false;
        boolean excludePerDiem = filter.getExcludePerDiem() != null && filter.getExcludePerDiem();
        if (CollectionUtils.isEmpty(empIds)) {
            empIds = Set.of("");
            ignoreEmpId = true;
        }
        boolean ignoreDates = false;
        Set<LocalDate> dates = filter.getDates();
        if (CollectionUtils.isEmpty(dates)) {
            ignoreDates = true;
        }
        if (includes == null) includes = new ArrayList<>();
        Set<AllocationTime> allocationTimes;
        if (includes.contains("all")) {
            allocationTimes = mAllocationTimeRepository
                    .findByDateRangeAndProjectIdAndStatusFetchingAll(startDate, endDate, ignoreDates, dates, payrollStartDate, payrollEndDate, isAllProjects, projectId, teamName, status, notStatus, ignoreEmpId, empIds);
        } else {
            allocationTimes = mAllocationTimeRepository
                    .findByDateRangeAndProjectIdAndStatus(startDate, endDate, ignoreDates, dates, payrollStartDate, payrollEndDate, isAllProjects, projectId, teamName, status, notStatus, ignoreEmpId, empIds);
        }

        if (includes.contains("crafts") || StringUtils.hasText(shift)) {
            fetchEmployeeAndCraft(allocationTimes, projectId, excludePerDiem);
            if (StringUtils.hasText(shift) && !CollectionUtils.isEmpty(allocationTimes)) {
                // Filter allocationTimes by shift
                assert projectId != null;
                allocationTimes = allocationTimes.stream()
                        .filter(t -> StringUtils.hasText(t.getEmpId()))
                        .filter(t -> {
                            String employeeShift = t.getEmployee().getShift();
                            return StringUtils.hasText(employeeShift) && employeeShift.equals(shift);
                        })
                        .collect(Collectors.toSet());
            }

        }
        return allocationTimes.toArray(AllocationTime[]::new);
    }

    @Override
    public AllocationSubmissionDto getAllocationSubmissionDtoByDateOfService(Long userId, Long projectId, String teamName, LocalDate dateOfService) {
        List<AllocationSubmission> allocationSubmissions = mAllocationSubmissionRepository.findByProjectIdAndTeamNameAndDateOfServiceAndStatusNot(projectId, teamName, dateOfService, AllocationSubmissionStatus.DENIED);
        if (!CollectionUtils.isEmpty(allocationSubmissions)) {
            return getAllocationSubmissionDtoById(allocationSubmissions.stream()
                    .map(AllocationSubmission::getId)
                    .max(Long::compareTo)
                    .orElseThrow(IllegalArgumentException::new));
        }
        return new AllocationSubmissionDto(getNewAllocationSubmissionByDateOfService(userId, projectId, teamName, dateOfService));
    }

    private AllocationSubmission getNewAllocationSubmissionByDateOfService(Long userId, Long projectId, String teamName, LocalDate dateOfService) {
        Rule rule = ruleRepository.getRuleByProjectId(projectId);
        AllocationSubmission allocationSubmission = new AllocationSubmission();
        allocationSubmission.setSubmitUserId(userId);
        allocationSubmission.setProjectId(projectId);
        allocationSubmission.setDateOfService(dateOfService);
        allocationSubmission.setPayrollDate(rule.getEndOfWeek(dateOfService));
        allocationSubmission.setStatus(AllocationSubmissionStatus.PENDING);
        ReportSubmission[] reportSubmissions = getReportSubmissions(projectId, userId, teamName, LocalDate.now().minusDays(1), null, List.of("reportPercs"));
        ReportSubmission reportSubmission = null;
        if (reportSubmissions != null && reportSubmissions.length > 0) reportSubmission = reportSubmissions[0];
        Set<ReportPerc> reportPercSet = null;
        if (null != reportSubmission) {
            reportPercSet = reportSubmission.getReportPercs();
            allocationSubmission.setReportSubmissionId(reportSubmission.getId());
        }
        allocationSubmission.setTeamName(teamName);

        List<StageTransaction> stageTransactionList = getStageTransactions(projectId, teamName, dateOfService);
        Map<String, StageTransaction> transactionMap = stageTransactionList.stream()
                .collect(Collectors.toMap(StageTransaction::getEmpId, Function.identity(), (e1, e2) -> e1.getCreatedAt().isBefore(e2.getCreatedAt()) ? e2 : e1));
        List<StageTransaction> distinctTransactions = new ArrayList<>(transactionMap.values());
        Set<Long> distinctIds = distinctTransactions.stream().map(StageTransaction::getId).collect(Collectors.toSet());
        List<StageTransaction> invalidTransactions = stageTransactionList.stream()
                .filter(st -> !distinctIds.contains(st.getId()))
                .collect(Collectors.toList());
        invalidTransactions.forEach(st -> {
            st.setStatus(StageTransaction.Status.Processed);
            st.setProcessedAt(LocalDateTime.now());
        });


        Set<AllocationTime> allocationTimeSet = new TreeSet<>();

        for (StageTransaction stageTransaction : distinctTransactions) {

            final AllocationTime allocationTime = generateAllocationTime(stageTransaction);
            allocationTime.setAllocationSubmission(allocationSubmission);

            if (null != reportPercSet) {
                BigDecimal stHour = stageTransaction.getStHour();
                BigDecimal otHour = stageTransaction.getOtHour();
                BigDecimal dtHour = stageTransaction.getDtHour();
                BigDecimal totalHour = stageTransaction.getTotalHour();

                BigDecimal remainingSTHour = stHour;
                BigDecimal remainingOTHour = otHour;
                BigDecimal remainingDTHour = dtHour;
                BigDecimal remainingTotalHour = totalHour;

                for (ReportPerc reportPerc : reportPercSet) {
                    CostCodePerc costCodePerc = new CostCodePerc();

                    String fullCostCode = reportPerc.getCostCodeFull();
                    costCodePerc.setCostCodeFull(fullCostCode);
                    costCodePerc.setPurchaseOrder(reportPerc.getOrderCode());
                    Integer percentage = reportPerc.getPercentage();
                    BigDecimal decimalPerc = BigDecimal.valueOf((double) percentage / 100);
                    if (null != stHour) {
                        BigDecimal subSTHour = stHour.multiply(decimalPerc)
                                .setScale(2, RoundingMode.HALF_UP).stripTrailingZeros();
                        costCodePerc.setStHour(subSTHour.min(remainingSTHour));
                        remainingSTHour = BigDecimal.ZERO.max(remainingSTHour.subtract(subSTHour));
                    }
                    if (null != otHour) {
                        BigDecimal subOTHour = otHour.multiply(decimalPerc)
                                .setScale(2, RoundingMode.HALF_UP).stripTrailingZeros();
                        costCodePerc.setOtHour(subOTHour.min(remainingOTHour));
                        remainingOTHour = BigDecimal.ZERO.max(remainingOTHour.subtract(subOTHour));
                    }
                    if (null != dtHour) {
                        BigDecimal subDTHour = dtHour.multiply(decimalPerc)
                                .setScale(2, RoundingMode.HALF_UP).stripTrailingZeros();
                        costCodePerc.setDtHour(subDTHour.min(remainingDTHour));
                        remainingDTHour = BigDecimal.ZERO.max(remainingDTHour.subtract(subDTHour));
                    }
                    if (null != totalHour) {
                        BigDecimal subTotalHour = totalHour.multiply(decimalPerc)
                                .setScale(2, RoundingMode.HALF_UP).stripTrailingZeros();
                        costCodePerc.setTotalHour(subTotalHour.min(remainingTotalHour));
                        remainingTotalHour = BigDecimal.ZERO.max(remainingTotalHour.subtract(subTotalHour));
                    }
                    allocationTime.addCostCodePerc(costCodePerc);
                }
            }
            allocationTimeSet.add(allocationTime);
        }
        setAllocationTimeDefaultValues(allocationTimeSet, projectId);
        allocationSubmission.setAllocationTimes(allocationTimeSet);
        return allocationSubmission;
    }

    private void setAllocationTimeDefaultValues(Collection<AllocationTime> allocationTimes, Long projectId) {
        fetchEmployeeAndCraft(new HashSet<>(allocationTimes), projectId);
        allocationTimes.forEach(t -> {
            if (!StringUtils.hasText(t.getEmployee().getFirstName())) {
                t.setFirstName(t.getEmployee().getFirstName());
            }
            if (!StringUtils.hasText(t.getEmployee().getLastName())) {
                t.setLastName(t.getEmployee().getLastName());
            }
            BigDecimal craftPerDiemAmount = t.getEmployee().getCraftEntity().getPerDiem();
            craftPerDiemAmount = craftPerDiemAmount == null ? BigDecimal.ZERO : craftPerDiemAmount;
            BigDecimal perDiemAmount = t.getPerDiem().getOverwriteAmount();
            t.setPerDiemAmount(perDiemAmount == null ? craftPerDiemAmount : perDiemAmount);
        });
    }

    @Override
    public void deleteAllocationSubmissionById(Long allocationSubmissionId) {
        AllocationSubmission submission = mAllocationSubmissionRepository.getAllocationSubmissionFetchingCostCodePercs(allocationSubmissionId);
        if (!CollectionUtils.isEmpty(submission.getAllocationTimes())) {
            Set<Long> laborKeys = submission.getAllocationTimes().stream()
                    .map(AllocationTime::getCostCodePercs)
                    .filter(Objects::nonNull)
                    .flatMap(Collection::stream)
                    .map(CostCodePerc::getId)
                    .collect(Collectors.toSet());
            Set<Long> allocationTimeIds = submission.getAllocationTimes().stream()
                    .map(AllocationTime::getId)
                    .collect(Collectors.toSet());
            costCodeTagAssociationRepository.deleteByKeyInAndCostCodeType(laborKeys, CostCodeType.DEFAULT);
            costCodeTagAssociationRepository.deleteByKeyInAndCostCodeType(allocationTimeIds, CostCodeType.PERDIEM);
            costCodeTagAssociationRepository.deleteByKeyInAndCostCodeType(allocationTimeIds, CostCodeType.MOB);
        }

        mAllocationSubmissionRepository.deleteById(allocationSubmissionId);
    }

    @Override
    public void deleteStageTransactionsByTeamAndDateOfService(String team, LocalDate dateOfService, Long projectId) {
        team = team == null ? "" : team;
        mStageTransactionRepository.deleteByTeamNameAndDateOfService(team, dateOfService, projectId);
    }

    private AllocationTime generateAllocationTime(StageTransaction stageTransaction) {

        final AllocationTime allocationTime = new AllocationTime();
        allocationTime.setFirstName(stageTransaction.getFirstName());
        allocationTime.setLastName(stageTransaction.getLastName());
        allocationTime.setEmpId(stageTransaction.getEmpId());
        allocationTime.setBadge(stageTransaction.getBadge());
        allocationTime.setClientEmpId(stageTransaction.getClientEmpId());
        allocationTime.setTeamName(stageTransaction.getTeamName());
        BigDecimal stHour = stageTransaction.getStHour();
        BigDecimal otHour = stageTransaction.getOtHour();
        BigDecimal dtHour = stageTransaction.getDtHour();
        BigDecimal totalHour = stageTransaction.getTotalHour();
        BigDecimal netHour = stageTransaction.getNetHour();
        allocationTime.setStHour(stHour);
        allocationTime.setDtHour(dtHour);
        allocationTime.setOtHour(otHour);
        allocationTime.setTotalHour(totalHour);
        allocationTime.setNetHour(netHour);
        Set<StageException> exceptions = stageTransaction.getStageExceptions();
        if (!CollectionUtils.isEmpty(exceptions)) {
            final Set<PunchException> punchExceptionSet = new TreeSet<>();
            exceptions.forEach(
                    e -> {
                        PunchException punchException = new PunchException();
                        punchException.setException(e.getException());
                        punchException.setOrder(e.getOrder());
                        punchException.setAllocationTime(allocationTime);
                        punchException.setExceptionType(e.getExceptionType());
                        punchExceptionSet.add(punchException);
                    }
            );
            allocationTime.setPunchExceptions(punchExceptionSet);
        }
        Set<StageRecord> stageRecords = stageTransaction.getStageRecords();
        if (!CollectionUtils.isEmpty(stageRecords)) {
            final Set<AllocationRecord> allocationRecordSet = new TreeSet<>();
            stageRecords.forEach(
                    e -> {
                        AllocationRecord allocationRecord = new AllocationRecord();
                        allocationRecord.setDoorName(e.getDoorName());
                        allocationRecord.setSide(e.getSide());
                        allocationRecord.setAccessGranted(e.isAccessGranted());
                        allocationRecord.setValid(e.isValid());
                        allocationRecord.setAdjustedTime(e.getAdjustedTime());
                        allocationRecord.setNetEventTime(e.getNetEventTime());
                        allocationRecord.setAllocationTime(allocationTime);
                        allocationRecordSet.add(allocationRecord);
                    }
            );
            allocationTime.setAllocationRecords(allocationRecordSet);
        }
        return allocationTime;
    }
}
