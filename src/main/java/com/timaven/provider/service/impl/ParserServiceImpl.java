package com.timaven.provider.service.impl;

import com.timaven.provider.dao.repository.ProjectShiftRepository;
import com.timaven.provider.dao.repository.ShiftRepository;
import com.timaven.provider.model.*;
import com.timaven.provider.service.*;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

@Service
public class ParserServiceImpl implements ParserService {

    private final StageService stageService;
    private final CostCodeService costCodeService;
    private final EmployeeService employeeService;
    private final CraftService craftService;
    private final ShiftRepository shiftRepository;
    private final ProjectShiftRepository projectShiftRepository;
    private final PerDiemService perDiemService;

    @Autowired
    public ParserServiceImpl(StageService stageService,
                             CostCodeService costCodeService,
                             EmployeeService employeeService,
                             CraftService craftService,
                             PerDiemService perDiemService,
                             ShiftRepository shiftRepository,
                             ProjectShiftRepository projectShiftRepository) {
        this.stageService = stageService;
        this.costCodeService = costCodeService;
        this.employeeService = employeeService;
        this.craftService = craftService;
        this.perDiemService = perDiemService;
        this.shiftRepository = shiftRepository;
        this.projectShiftRepository = projectShiftRepository;
    }

    private void upsertRoster(StageSubmission submission, Set<Employee> employeeSet) {
        mergeEmployees(employeeSet, submission);
    }

    private void upsertTeamCostCode(Set<TeamCostCode> teamCostCodeSet, StageSubmission submission) {
        mergeTeamCostCodes(teamCostCodeSet, submission);
    }

    private void upsertCrafts(Set<Craft> craftSet, StageSubmission submission) {
        mergeCrafts(craftSet, submission);
    }

    private void mergeEmployees(Set<Employee> employeeSet, StageSubmission submission) {
        if (CollectionUtils.isEmpty(employeeSet)) return;
        Set<String> ids = employeeSet.stream().map(Employee::getEmpId).collect(toSet());
        EmployeeFilter employeeFilter = new EmployeeFilter();
        employeeFilter.setProjectId(submission.getProjectId());
        employeeFilter.setEmpIds(ids);
        employeeFilter.setStartDate(submission.getEffectiveOn());
        List<Employee> result = new ArrayList<>(employeeService.findEmployees(employeeFilter));

        final Set<String> empIds = result.stream().map(Employee::getEmpId).collect(toSet());

        Set<Employee> persistSet =
                employeeSet.stream().filter(e -> !empIds.contains(e.getEmpId())).collect(toSet());
//        Set<Employee> mergeSet = new HashSet<>();

        Set<Employee> newEmpToBeMerged =
                employeeSet.stream().filter(e -> empIds.contains(e.getEmpId())).collect(toSet());
        if (!CollectionUtils.isEmpty(newEmpToBeMerged)) {
            Map<String, Employee> latestEmpMap = result.stream()
                    .sorted(Comparator.comparing(Employee::getCreatedAt)).collect(toMap(Employee::getEmpId,
                            Function.identity(), (e1, e2) -> e2));
            Comparator<Employee> employeeComparator = Comparator.comparing(Employee::getEmpId, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getFirstName, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getLastName, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getMiddleName, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getGender, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getSignInSheet, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getBadge, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getCraft, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getBaseST, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getBaseOT, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getBaseDT, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getHolidayRate, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getSickLeaveRate, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getTravelRate, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getVacationRate, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getClientEmpId, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getClientCraft, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getJobNumber, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getCompany, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getTeamName, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getCrew, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getHiredAt, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getTerminatedAt, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getHasPerDiem, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getHasRigPay, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getShift, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getScheduleStart, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getScheduleEnd, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getLunchStart, Comparator.nullsLast(Comparator.naturalOrder()))
                    .thenComparing(Employee::getLunchEnd, Comparator.nullsLast(Comparator.naturalOrder()));
            for (Employee employee : newEmpToBeMerged) {
                String empId = employee.getEmpId();
                if (latestEmpMap.containsKey(empId)) {
                    Employee oldEmployee = latestEmpMap.get(empId);
                    if (employeeComparator.compare(employee, oldEmployee) != 0) {
//                        if (oldEmployee.getTerminatedAt() == null) {
//                            oldEmployee.setTerminatedAt(submission.getEffectiveOn());
//                            mergeSet.add(oldEmployee);
//                        }
                        employee.setPerDiemId(oldEmployee.getPerDiemId());
                        persistSet.add(employee);
                    }
                }
            }
        }

        // For easy audit purpose
//        employeeService.saveEmployees(mergeSet);
        employeeService.saveEmployees(persistSet);
    }

    public void mergeTeamCostCodes(Set<TeamCostCode> teamCostCodeSet, StageSubmission submission) {
        if (CollectionUtils.isEmpty(teamCostCodeSet)) return;
        Set<String> teamNames = teamCostCodeSet.stream()
                .map(TeamCostCode::getTeamName).collect(toSet());
        LocalDate effectiveOn = submission.getEffectiveOn();
        List<TeamCostCode> result = Arrays.stream(costCodeService.getTeamCostCodesByProjectIdAndTeamNames(submission.getProjectId(), teamNames, effectiveOn, null)).collect(Collectors.toList());

        Set<Integer> hashCodes = teamCostCodeSet.stream().map(TeamCostCode::customHashCode).collect(toSet());
        List<TeamCostCode> teamCostCodesToBeEnded = result.stream()
                .filter(tcc -> !hashCodes.contains(tcc.customHashCode())).collect(Collectors.toList());

        teamCostCodesToBeEnded.forEach(tcc -> {
            if (tcc.getEndDate() == null || tcc.getEndDate().isAfter(effectiveOn)) {
                tcc.setEndDate(effectiveOn);
            }
        });

        Map<Integer, TeamCostCode> codeTeamCodeMap = result.stream()
                .sorted(Comparator.comparing(TeamCostCode::getCreatedAt))
                .collect(Collectors.toMap(TeamCostCode::customHashCode, Function.identity(), (e1, e2) -> e2));

        Set<TeamCostCode> persistSet = new HashSet<>(teamCostCodesToBeEnded);
//        Set<TeamCostCode> mergeSet = new HashSet<>();
        for (TeamCostCode teamCostCode : teamCostCodeSet) {
            TeamCostCode existingTeamCostCode = codeTeamCodeMap.get(teamCostCode.customHashCode());
            if (null == existingTeamCostCode) {
                persistSet.add(teamCostCode);
            } else if (!existingTeamCostCode.getStartDate().isEqual(submission.getEffectiveOn())) {
//                existingTeamCostCode.setEndDate(submission.getEffectiveOn());
//                mergeSet.add(existingTeamCostCode);
                persistSet.add(teamCostCode);
            }
        }

        // For easy audit purpose
//        costCodeService.saveTeamCostCodes(mergeSet);
        costCodeService.saveTeamCostCodes(persistSet);
    }

    private void mergeCrafts(Set<Craft> craftSet, StageSubmission submission) {
        if (CollectionUtils.isEmpty(craftSet)) return;
        Set<String> codes = craftSet.stream().map(Craft::getCode).collect(toSet());
        List<Craft> result = Arrays.stream(craftService.findByProjectIdAndCodes(submission.getProjectId(), codes)).collect(Collectors.toList());

        Map<String, Craft> latestCraftMap = result.stream()
                .sorted(Comparator.comparing(Craft::getCreatedAt))
                .collect(Collectors.toMap(Craft::getCode, Function.identity(), (e1, e2) -> e2));

        Set<Craft> persistSet = new HashSet<>();
//        Set<Craft> mergeSet = new HashSet<>();

        for (Craft craft : craftSet) {
            Craft existingCraft = latestCraftMap.get(craft.getCode());
            if (existingCraft == null || existingCraft.compareTo(craft) != 0) {
                //existingCraft.setEndDate(submission.getEffectiveOn());
//                mergeSet.add(existingCraft);
                if (craft.getStartDate() == null) {
                    craft.setStartDate(LocalDate.now());
                }
                persistSet.add(craft);
            }
        }

        // For easy audit purpose
//        craftService.saveCrafts(mergeSet);
        craftService.saveCrafts(persistSet);
    }

    @Override
    public CommonResult updateProjectSetUp() {
        CommonResult commonResult = new CommonResult();
        LocalDate today = LocalDate.now();
        LocalDateTime now = LocalDateTime.now();

        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        boolean noAuthUser = authentication == null || !authentication.isAuthenticated();
        // Process rosters
        List<StageSubmission> rosterStageSubmissions =
                Arrays.stream(stageService.findStageSubmissions(today, StageSubmission.Status.Pending,
                        StageSubmission.Type.Roster)).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(rosterStageSubmissions)) {
            if (noAuthUser) {
                MDC.put("username", rosterStageSubmissions.get(0).getUsername());
            }
            IntStream.range(0, rosterStageSubmissions.size()).forEach(idx -> {
                StageSubmission submission = rosterStageSubmissions.get(idx);
                if (idx == 0) {
                    submission.setStatus(StageSubmission.Status.Imported);
                    parseRoster(submission);
                } else {
                    submission.setStatus(StageSubmission.Status.Ignored);
                }
                submission.setProcessedAt(now);
                stageService.saveStageSubmission(submission);
            });
            if (noAuthUser) {
                MDC.remove("username");
            }
        }

        // Process team_costCode
        List<StageSubmission> teamCostCodeStageSubmissions =
                Arrays.stream(stageService.findStageSubmissions(today, StageSubmission.Status.Pending,
                        StageSubmission.Type.TeamCC)).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(teamCostCodeStageSubmissions)) {
            if (noAuthUser) {
                MDC.put("username", teamCostCodeStageSubmissions.get(0).getUsername());
            }
            IntStream.range(0, teamCostCodeStageSubmissions.size()).forEach(idx -> {
                StageSubmission submission = teamCostCodeStageSubmissions.get(idx);
                if (idx == 0) {
                    submission.setStatus(StageSubmission.Status.Imported);
                    parseTeamCostCode(submission, commonResult);
                } else {
                    submission.setStatus(StageSubmission.Status.Ignored);
                }
                submission.setProcessedAt(now);
                stageService.saveStageSubmission(submission);
            });
            if (noAuthUser) {
                MDC.remove("username");
            }
        }

        // Process craft
        processCraft(today, now, noAuthUser, commonResult);

        return commonResult;
    }

    @Override
    public CommonResult updateCraftSubmission() {
        LocalDate today = LocalDate.now();
        LocalDateTime now = LocalDateTime.now();
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        boolean noAuthUser = authentication == null || !authentication.isAuthenticated();

        CommonResult cr = new CommonResult();
        processCraft(today, now, noAuthUser, cr);
        return cr;
    }

    private void processCraft(LocalDate today, LocalDateTime now, boolean noAuthUser, CommonResult cr) {
        List<StageSubmission> craftStageSubmissions =
                Arrays.stream(stageService.findStageSubmissions(today, StageSubmission.Status.Pending,
                        StageSubmission.Type.Crafts)).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(craftStageSubmissions)) {
            if (noAuthUser) {
                MDC.put("username", craftStageSubmissions.get(0).getUsername());
            }
            IntStream.range(0, craftStageSubmissions.size()).forEach(idx -> {
                StageSubmission submission = craftStageSubmissions.get(idx);
                if (idx == 0) {
                    submission.setStatus(StageSubmission.Status.Imported);
                    parseCrafts(submission, cr);
                } else {
                    submission.setStatus(StageSubmission.Status.Ignored);
                }
                submission.setProcessedAt(now);
                stageService.saveStageSubmission(submission);
            });
            if (noAuthUser) {
                MDC.remove("username");
            }
        }
    }

    private void parseRoster(StageSubmission submission) {
        List<StageRoster> stageRosters = Arrays.stream(stageService.getStageRostersBySubmissionId(submission.getId())).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(stageRosters)) return;
        Map<String, Employee> idEmployeeMap = new HashMap<>();
        Long projectId = submission.getProjectId();
        Set<Shift> shifts = shiftRepository.getShiftsByProjectId(projectId);
        Map<String, Shift> nameShiftMap = shifts.stream()
                .collect(Collectors.toMap(Shift::getName, Function.identity(),
                        (e1, e2) -> e1.getCreatedAt().isBefore(e2.getCreatedAt()) ? e2 : e1));
        ProjectShift projectShift = projectShiftRepository.getProjectShiftByProjectId(projectId);
        Long defaultShiftId = projectShift.getShiftId();
        Shift shift = new Shift();
        shift.setName("DAY");
        shift.setScheduleStart(LocalTime.of(6, 0));
        shift.setScheduleEnd(LocalTime.of(17, 0));
        shift.setBreakStart(LocalTime.of(12, 0));
        shift.setBreakEnd(LocalTime.of(12, 30));
        shift.setPaidBreakDuration(0);
        shift.setProjectId(projectId);
        Shift defaultShift = shifts.stream()
                .filter(s -> defaultShiftId != null && s.getId().equals(projectShift.getShiftId()))
                .findFirst()
                .orElse(shift);
        stageRosters.forEach(r -> {
            Employee employee = new Employee();
            employee.setBadge(r.getBadge());
            String fullName = r.getName();
            if (!StringUtils.hasText(fullName)) return;
            if (fullName.contains(",")) {
                String[] nameArray = Arrays.stream(fullName.split(",")).map(String::trim).filter(StringUtils::hasText).toArray(String[]::new);
                if (nameArray.length == 2) {
                    employee.setLastName(nameArray[0]);
                    employee.setFirstName(nameArray[1]);
                } else if (nameArray.length == 1) {
                    employee.setLastName(nameArray[0]);
                } else {
                    return;
                }
            } else {
                String[] nameArray = Arrays.stream(fullName.split(" ")).map(String::trim).filter(StringUtils::hasText).toArray(String[]::new);
                if (nameArray.length == 3) {
                    employee.setFirstName(nameArray[0]);
                    employee.setMiddleName(nameArray[1]);
                    employee.setLastName(nameArray[2]);
                } else if (nameArray.length == 2) {
                    employee.setFirstName(nameArray[0]);
                    employee.setLastName(nameArray[1]);
                } else if (nameArray.length == 1) {
                    employee.setLastName(nameArray[0]);
                } else {
                    return;
                }
            }
            employee.setEmpId(r.getEmployeeId());
            employee.setClientEmpId(r.getClientEmpId());
            employee.setClientCraft(r.getClientCraft());
            employee.setCompany(r.getCompany());
            employee.setTerminatedAt(r.getTerminatedAt());
            employee.setCraft(r.getCraftCode());
            employee.setHasPerDiem(r.getHasPerDiem());
            employee.setHasRigPay(r.getHasRigPay());
            employee.setProjectId(projectId);
            employee.setJobNumber(r.getJobNumber());
            employee.setBaseST(r.getBaseST());
            employee.setBaseOT(r.getBaseOT());
            employee.setBaseDT(r.getBaseDT());
            employee.setHolidayRate(r.getHolidayRate());
            employee.setSickLeaveRate(r.getSickLeaveRate());
            employee.setTravelRate(r.getTravelRate());
            employee.setVacationRate(r.getVacationRate());
            employee.setSignInSheet(r.getSignInSheet());
            if (StringUtils.hasText(r.getShift())
                    && r.getScheduleStart() != null
                    && r.getScheduleEnd() != null
                    && r.getLunchStart() != null
                    && r.getLunchEnd() != null) {
                employee.setShift(r.getShift());
                employee.setScheduleStart(r.getScheduleStart());
                employee.setScheduleEnd(r.getScheduleEnd());
                employee.setLunchStart(r.getLunchStart());
                employee.setLunchEnd(r.getLunchEnd());
            } else {
                Shift shift1 = (StringUtils.hasText(r.getShift()) && nameShiftMap.containsKey(r.getShift())) ? nameShiftMap.get(r.getShift()) : defaultShift;
                employee.setShift(shift1.getName());
                employee.setScheduleStart(shift1.getScheduleStart());
                employee.setScheduleEnd(shift1.getScheduleEnd());
                employee.setLunchStart(shift1.getBreakStart());
                employee.setLunchEnd(shift1.getBreakEnd());
            }
            employee.setTeamName(r.getTeam());
            employee.setCrew(r.getCrew());
            employee.setEffectedOn(submission.getEffectiveOn());
            employee.setHiredAt(r.getHiredAt() == null ? submission.getEffectiveOn() : r.getHiredAt());
            idEmployeeMap.put(r.getEmployeeId(), employee);
        });
        upsertRoster(submission, new HashSet<>(idEmployeeMap.values()));
    }

    private void parseTeamCostCode(StageSubmission submission, CommonResult commonResult) {
        List<StageTeamCostCode> stageTeamCostCodes =
                Arrays.stream(stageService.getStageTeamCostCodesBySubmissionId(submission.getId())).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(stageTeamCostCodes)) return;
        List<CostCodeLog> costCodeLogs = Arrays.stream(costCodeService.findCostCodesByProjectAndDate(submission.getProjectId(), submission.getEffectiveOn(), null, null, false)).collect(Collectors.toList());
        Set<String> costCodes = costCodeLogs.stream()
                .map(CostCodeLog::getCostCodeFull)
                .collect(toSet());
        Set<TeamCostCode> teamCostCodeSet = new HashSet<>();
        Set<String> unknownCostCode = new HashSet<>();
        stageTeamCostCodes.stream()
                .filter(tcc -> costCodes.contains(tcc.getCostCode()))
                .forEach(tcc -> {
                    TeamCostCode teamCostCode = new TeamCostCode();
                    teamCostCode.setStartDate(submission.getEffectiveOn());
                    teamCostCode.setTeamName(tcc.getTeam());
                    teamCostCode.setCostCode(tcc.getCostCode());
                    teamCostCode.setProjectId(submission.getProjectId());
                    teamCostCodeSet.add(teamCostCode);
                });
        stageTeamCostCodes.stream()
                .filter(tcc -> !costCodes.contains(tcc.getCostCode()))
                .forEach(tcc -> unknownCostCode.add(tcc.getCostCode()));
        unknownCostCode.forEach(cc -> commonResult.addWarning(String.format("Code %s not found in the system", cc)));
        upsertTeamCostCode(teamCostCodeSet, submission);
    }

    private void parseCrafts(StageSubmission submission, CommonResult cr) {
        List<StageCraft> stageCrafts = Arrays.stream(stageService.getStageCraftsBySubmissionId(submission.getId()))
                .collect(Collectors.toList());
        if (CollectionUtils.isEmpty(stageCrafts)) return;

        // Resolve per diem rule
        Long projectId = submission.getProjectId();
        Map<String, PerDiem> perDiemMap = null;
        Map<String, Boolean> suppressWarningMap = new HashMap<>();
        if (projectId != null) {
            final String format = "Per Diem Rule %s does not exists and will be ignored";

            Map<String, PerDiem> pdMap = perDiemMap = Arrays.stream(
                    perDiemService.getPerDiemsByProjectId(projectId)
                )
                .collect(Collectors.toMap(
                    PerDiem::getName,
                    Function.identity(),
                    BinaryOperator.maxBy(Comparator.comparing(PerDiem::getId))
                    )
                );
            stageCrafts.stream()
                .filter(sc -> StringUtils.hasText(sc.getPerDiemRule()))
                .filter(sc -> !pdMap.containsKey(sc.getPerDiemRule()))
                .forEach(sc -> {
                    String key = sc.getPerDiemRule();
                    Boolean value = suppressWarningMap.get(key);
                    if (value == null) {
                        suppressWarningMap.put(key, true);
                        cr.addWarning(String.format(format, sc.getPerDiemRule()));
                    }
                });
        }

        Set<Craft> craftSet = new LinkedHashSet<>();
        for (StageCraft c : stageCrafts) {
            Craft craft = new Craft();
            craft.setCode(c.getCraftCode());
            craft.setDescription(c.getDescription());
            craft.setBillableST(c.getBillableST());
            craft.setBillableOT(c.getBillableOT());
            craft.setBillableDT(c.getBillableDT());
            craft.setPerDiem(c.getPerDiem());
            craft.setRigPay(c.getRigPay());
            craft.setStartDate(submission.getEffectiveOn());
            craft.setProjectId(submission.getProjectId());
            craft.setCompany(c.getCompany());
            craft.setStartDate(c.getStartDate());
            craft.setEndDate(c.getEndDate());
            String perDiemRule = c.getPerDiemRule();
            if (perDiemMap != null
                    && StringUtils.hasText(perDiemRule)
                    && perDiemMap.containsKey(perDiemRule)) {
                craft.setPerDiemId(perDiemMap.get(perDiemRule).getId());
            }

            craftSet.add(craft);
        }

        upsertCrafts(craftSet, submission);
    }
}
