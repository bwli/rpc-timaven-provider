package com.timaven.provider.service.impl;

import com.timaven.provider.dao.repository.EmployeeRepository;
import com.timaven.provider.dao.repository.ProjectShiftRepository;
import com.timaven.provider.dao.repository.ShiftRepository;
import com.timaven.provider.model.ProjectShift;
import com.timaven.provider.model.Shift;
import com.timaven.provider.service.ShiftService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Transactional
public class ShiftServiceImpl implements ShiftService {

    private final ShiftRepository mShiftRepository;
    private final ProjectShiftRepository projectShiftRepository;
    private final ShiftRepository shiftRepository;
    private final EmployeeRepository employeeRepository;

    @Autowired
    public ShiftServiceImpl(ShiftRepository shiftRepository, ProjectShiftRepository projectShiftRepository, ShiftRepository shiftRepository1, EmployeeRepository employeeRepository) {
        mShiftRepository = shiftRepository;
        this.projectShiftRepository = projectShiftRepository;
        this.shiftRepository = shiftRepository1;
        this.employeeRepository = employeeRepository;
    }

    @Override
    public Set<Shift> getShiftsByProjectId(Long projectId) {
        return mShiftRepository.getShiftsByProjectId(projectId);
    }

    @Override
    public Shift getShiftById(Long shiftId) {
        return mShiftRepository.findById(shiftId).orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public Shift saveShift(Shift shift) {
        return mShiftRepository.save(shift);
    }

    @Override
    public void deleteShiftById(Long id) {
        mShiftRepository.deleteById(id);
    }

    @Override
    public Set<String> getAllShiftNamesByProjectId(Long projectId) {
//        Set<String> projectShiftNames = mShiftRepository.getShiftsByProjectId(projectId).stream()
//                .map(Shift::getName).collect(Collectors.toSet());
        Set<String> employeeShiftNames = employeeRepository.findShiftsByProjectIdAndDate(projectId, LocalDate.now());
        return employeeShiftNames.stream()
                .collect(Collectors.toCollection(() -> new TreeSet<>(String.CASE_INSENSITIVE_ORDER)));
    }

    @Override
    public Shift findDefaultShiftByProjectId(Long projectId) {
        ProjectShift projectShift = projectShiftRepository.getProjectShiftByProjectId(projectId);
        Shift shift = null;
        if (null != projectShift) {
            shift = shiftRepository.findById(projectShift.getShiftId()).orElseThrow(IllegalArgumentException::new);
        }
        return shift;
    }

    @Override
    public Shift getShiftByProjectIdAndName(Long projectId, String name) {
        return shiftRepository.findByProjectIdAndName(projectId, name);
    }
}
