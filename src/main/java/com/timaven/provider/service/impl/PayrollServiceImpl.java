package com.timaven.provider.service.impl;

import com.timaven.provider.dao.repository.ProjectPayrollRepository;
import com.timaven.provider.model.ProjectPayroll;
import com.timaven.provider.model.dto.ProjectPayrollDto;
import com.timaven.provider.model.paging.Page;
import com.timaven.provider.model.paging.PagingRequest;
import com.timaven.provider.model.utils.PagingRequestUtil;
import com.timaven.provider.service.PayrollService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Transactional
public class PayrollServiceImpl implements PayrollService {

    private final ProjectPayrollRepository projectPayrollRepository;

    @Autowired
    public PayrollServiceImpl(ProjectPayrollRepository projectPayrollRepository) {
        this.projectPayrollRepository = projectPayrollRepository;
    }

    @Override
    public Page<ProjectPayrollDto> getProjectPayrollPage(Long projectId, PagingRequest pagingRequest) {
        Page<ProjectPayrollDto> page = new Page<>();
        page.setDraw(pagingRequest.getDraw());
        Specification<ProjectPayroll> specificationAll = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(criteriaBuilder.equal(root.get("projectId"), projectId));
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
        List<ProjectPayroll> allProjectPayrolls = projectPayrollRepository.findAll(specificationAll);
        page.setRecordsTotal(allProjectPayrolls.size());
        Map<String, String> columnMap = new HashMap<>();
        columnMap.put("weekEndDate", "id.weekEndDate");
        columnMap.put("weekEndDateStatus", "weekEndDate");
        Specification<ProjectPayroll> specification = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(criteriaBuilder.equal(root.get("projectId"), projectId));
            return PagingRequestUtil.getPredicate(root, criteriaQuery, criteriaBuilder, pagingRequest, columnMap,
                    predicates);
        };
        long recordsFiltered;
        List<ProjectPayroll> projectPayrolls;
        Pageable pageable = PagingRequestUtil.Paging2Pageable(pagingRequest, columnMap);
        if (pageable != null) {
            org.springframework.data.domain.Page<ProjectPayroll> rowPage = projectPayrollRepository.findAll(specification,
                    pageable);
            projectPayrolls = rowPage.getContent();
            recordsFiltered = projectPayrollRepository.count(specification);
        } else {
            projectPayrolls = projectPayrollRepository.findAll(specification);
            recordsFiltered = projectPayrolls.size();
        }
        page.setRecordsFiltered(recordsFiltered);
        List<ProjectPayrollDto> rows = projectPayrolls.stream()
                .map(ProjectPayrollDto::new).collect(Collectors.toList());
        page.setData(rows);
        return page;
    }
}
