package com.timaven.provider.service.impl;

import com.timaven.provider.dao.repository.*;
import com.timaven.provider.model.*;
import com.timaven.provider.model.dto.*;
import com.timaven.provider.model.paging.Page;
import com.timaven.provider.model.paging.PagingRequest;
import com.timaven.provider.model.utils.PagingRequestUtil;
import com.timaven.provider.service.PurchaseOrderService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.persistence.criteria.Predicate;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class PurchaseOrderServiceImpl implements PurchaseOrderService {

    private final PurchaseOrderSubmissionRepository purchaseOrderSubmissionRepository;
    private final PurchaseOrderRepository purchaseOrderRepository;
    private final PurchaseOrderSummaryRepository purchaseOrderSummaryRepository;
    private final ProjectRepository projectRepository;
    private final RequisitionRepository requisitionRepository;
    private final RequisitionGroupRepository requisitionGroupRepository;
    private final PurchaseOrderDetailsRepository purchaseOrderDetailsRepository;
    private final PurchaseOrderBillingRepository purchaseOrderBillingRepository;
    private final PurchaseOrderBillingDetailViewRepository purchaseOrderBillingDetailViewRepository;
    private final PurchaseOrderGroupRepository purchaseOrderGroupRepository;
    private final PurchaseVendorRosterRepository purchaseVendorRosterRepository;

    @Autowired
    public PurchaseOrderServiceImpl(PurchaseOrderSubmissionRepository purchaseOrderSubmissionRepository,
                                    PurchaseOrderRepository purchaseOrderRepository,
                                    PurchaseOrderSummaryRepository purchaseOrderSummaryRepository,
                                    ProjectRepository projectRepository, RequisitionRepository requisitionRepository,
                                    RequisitionGroupRepository requisitionGroupRepository,
                                    PurchaseOrderDetailsRepository purchaseOrderDetailsRepository,
                                    PurchaseOrderBillingRepository purchaseOrderBillingRepository,
                                    PurchaseOrderBillingDetailViewRepository purchaseOrderBillingDetailViewRepository,
                                    PurchaseOrderGroupRepository purchaseOrderGroupRepository,
                                    PurchaseVendorRosterRepository purchaseVendorRosterRepository) {
        this.purchaseOrderSubmissionRepository = purchaseOrderSubmissionRepository;
        this.purchaseOrderRepository = purchaseOrderRepository;
        this.purchaseOrderSummaryRepository = purchaseOrderSummaryRepository;
        this.projectRepository = projectRepository;
        this.requisitionRepository = requisitionRepository;
        this.requisitionGroupRepository = requisitionGroupRepository;
        this.purchaseOrderDetailsRepository = purchaseOrderDetailsRepository;
        this.purchaseOrderBillingRepository = purchaseOrderBillingRepository;
        this.purchaseOrderBillingDetailViewRepository = purchaseOrderBillingDetailViewRepository;
        this.purchaseOrderGroupRepository = purchaseOrderGroupRepository;
        this.purchaseVendorRosterRepository = purchaseVendorRosterRepository;
    }

    @Override
    public Page<PurchaseOrderSummaryDto> getLatestPurchaseOrderSummaries(PagingRequest pagingRequest, Long projectId) {
        Page<PurchaseOrderSummaryDto> page = new Page<>(new ArrayList<>());
        page.setDraw(pagingRequest.getDraw());
        PurchaseOrderSubmission purchaseOrderSubmission = getLatestPurchaseOrderSubmission();
        if (purchaseOrderSubmission == null) return page;

        final String jobNumber;
        final String subJob;
        if (projectId != null) {
            Project project = projectRepository.findById(projectId).orElseThrow(IllegalArgumentException::new);
            jobNumber = project.getJobNumber();
            subJob = project.getSubJob();
        } else {
            jobNumber = null;
            subJob = null;
        }


        Specification<PurchaseOrderSummary> specificationAll = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(criteriaBuilder.equal(root.get("submissionId"), purchaseOrderSubmission.getId()));
            if (projectId != null) {
                predicates.add(criteriaBuilder.equal(root.get("jobNumber"), jobNumber));
                predicates.add(criteriaBuilder.equal(root.get("subJob"), subJob));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
        long recordsTotal = purchaseOrderSummaryRepository.count(specificationAll);
        page.setRecordsTotal(recordsTotal);
        Map<String, String> columnMap = new HashMap<>();
        columnMap.put("extendedCost", "totalExtendedCost");
        Specification<PurchaseOrderSummary> specification = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(criteriaBuilder.equal(root.get("submissionId"), purchaseOrderSubmission.getId()));
            if (projectId != null) {
                predicates.add(criteriaBuilder.equal(root.get("jobNumber"), jobNumber));
                predicates.add(criteriaBuilder.equal(root.get("subJob"), subJob));
            }
            return PagingRequestUtil.getPredicate(root, criteriaQuery, criteriaBuilder, pagingRequest, columnMap,
                    predicates);
        };

        long recordsFiltered;
        List<PurchaseOrderSummary> purchaseOrders;
        Pageable pageable = PagingRequestUtil.Paging2Pageable(pagingRequest, columnMap);
        if (pageable != null) {
            org.springframework.data.domain.Page<PurchaseOrderSummary> rowPage =
                    purchaseOrderSummaryRepository.findAll(specification,
                            pageable);
            purchaseOrders = rowPage.getContent();
            recordsFiltered = purchaseOrderSummaryRepository.count(specification);
        } else {
            purchaseOrders = purchaseOrderSummaryRepository.findAll(specification);
            recordsFiltered = purchaseOrders.size();
        }
        page.setRecordsFiltered(recordsFiltered);
        page.setData(purchaseOrders.stream()
                .map(PurchaseOrderSummaryDto::new)
                .collect(Collectors.toList()));
        return page;
    }

    @Override
    public Page<PurchaseOrderDetailDto> getLatestPurchaseOrderDetails(PagingRequest pagingRequest,
                                                                      String purchaseOrderNumber) {
        Page<PurchaseOrderDetailDto> page = new Page<>(new ArrayList<>());
        page.setDraw(pagingRequest.getDraw());
        PurchaseOrderSubmission purchaseOrderSubmission = getLatestPurchaseOrderSubmission();
        if (purchaseOrderSubmission == null) return page;
        Specification<PurchaseOrderDetails> specificationAll = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(criteriaBuilder.equal(root.get("submissionId"), purchaseOrderSubmission.getId()));
            predicates.add(criteriaBuilder.equal(root.get("purchaseOrderNumber"), purchaseOrderNumber));
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
        long recordsTotal = purchaseOrderDetailsRepository.count(specificationAll);
        page.setRecordsTotal(recordsTotal);
        Map<String, String> columnMap = new HashMap<>();
        Specification<PurchaseOrderDetails> specification = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(criteriaBuilder.equal(root.get("submissionId"), purchaseOrderSubmission.getId()));
            predicates.add(criteriaBuilder.equal(root.get("purchaseOrderNumber"), purchaseOrderNumber));
            return PagingRequestUtil.getPredicate(root, criteriaQuery, criteriaBuilder, pagingRequest, columnMap,
                    predicates);
        };

        long recordsFiltered;
        List<PurchaseOrderDetails> purchaseOrderDetails;
        Pageable pageable = PagingRequestUtil.Paging2Pageable(pagingRequest, columnMap);
        if (pageable != null) {
            org.springframework.data.domain.Page<PurchaseOrderDetails> rowPage =
                    purchaseOrderDetailsRepository.findAll(specification, pageable);
            purchaseOrderDetails = rowPage.getContent();
            recordsFiltered = purchaseOrderDetailsRepository.count(specification);
        } else {
            purchaseOrderDetails = purchaseOrderDetailsRepository.findAll(specification);
            recordsFiltered = purchaseOrderDetails.size();
        }
        page.setRecordsFiltered(recordsFiltered);
        page.setData(purchaseOrderDetails.stream()
                .map(PurchaseOrderDetailDto::new)
                .collect(Collectors.toList()));
        return page;
    }

    @Override
    public RequisitionDto getRequisitionDto(String requisitionNumber) {
        List<Requisition> requisitions = requisitionRepository.findByRequisitionNumber(requisitionNumber);
        return new RequisitionDto(requisitions);
    }

    @Override
    public void deleteByRequisitionNumber(String requisitionNumber) {
        requisitionRepository.deleteByRequisitionNumber(requisitionNumber);
    }

    @Override
    public void saveRequisition(RequisitionDto requisitionDto, Long projectId) {
        Project project = projectRepository.findById(projectId).orElseThrow(IllegalArgumentException::new);
        String jobNumber = project.getJobNumber();
        String subJob = project.getSubJob();
        if (CollectionUtils.isEmpty(requisitionDto.getRequisitionLineDtos())) {
            requisitionDto.setRequisitionLineDtos(Collections.singletonList(new RequisitionLineDto()));
        }
        List<Requisition> requisitions = new ArrayList<>();
        for (RequisitionLineDto dto : requisitionDto.getRequisitionLineDtos()) {
            Requisition requisition = new Requisition();
            if (!StringUtils.isEmpty(requisitionDto.getPurchaseOrderNumber())) {
                requisition.setPurchaseOrderNumber(requisitionDto.getPurchaseOrderNumber());
            }
            requisition.setRequisitionDate(requisitionDto.getRequisitionDate());
            requisition.setVendorName(requisitionDto.getVendorName());
            requisition.setRequestor(requisitionDto.getRequestor());
            requisition.setRequisitionNumber(requisitionDto.getRequisitionNumber());
            requisition.setJobNumber(jobNumber);
            requisition.setSubJob(subJob);
            requisition.setLineNumber(dto.getLineNumber());
            requisition.setPartNumber(dto.getPartNumber());
            requisition.setPartDescription(dto.getPartDescription());
            requisition.setQuantity(dto.getQuantity());
            requisition.setUnitCost(dto.getUnitCost());
            requisition.setExtendedCost(dto.getExtendedCost());
            requisition.setFormattedCostDistribution(dto.getFormattedCostDistribution());
            requisition.setCostType(dto.getCostType());
            requisitions.add(requisition);
        }
        requisitionRepository.saveAll(requisitions);
    }

    @Override
    public Page<RequisitionGroup> getRequisitionGroups(PagingRequest pagingRequest, Long projectId, boolean purchaseNumberNull) {
        Page<RequisitionGroup> page = new Page<>();
        page.setDraw(pagingRequest.getDraw());
        final String jobNumber;
        final String subJob;
        if (projectId != null) {
            Project project = projectRepository.findById(projectId).orElseThrow(IllegalArgumentException::new);
            jobNumber = project.getJobNumber();
            subJob = project.getSubJob();
        } else {
            jobNumber = null;
            subJob = null;
        }

        Specification<RequisitionGroup> specificationAll = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (purchaseNumberNull) {
                predicates.add(criteriaBuilder.isNull(root.get("purchaseOrderNumber")));
            }
            if (projectId != null) {
                predicates.add(criteriaBuilder.equal(root.get("jobNumber"), jobNumber));
                predicates.add(criteriaBuilder.equal(root.get("subJob"), subJob));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
        long recordsTotal = requisitionGroupRepository.count(specificationAll);
        page.setRecordsTotal(recordsTotal);
        Map<String, String> columnMap = new HashMap<>();
        Specification<RequisitionGroup> specification = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (purchaseNumberNull) {
                predicates.add(criteriaBuilder.isNull(root.get("purchaseOrderNumber")));
            }
            if (projectId != null) {
                predicates.add(criteriaBuilder.equal(root.get("jobNumber"), jobNumber));
                predicates.add(criteriaBuilder.equal(root.get("subJob"), subJob));
            }
            return PagingRequestUtil.getPredicate(root, criteriaQuery, criteriaBuilder, pagingRequest, columnMap,
                    predicates);
        };

        long recordsFiltered;
        List<RequisitionGroup> requisitionGroups;
        Pageable pageable = PagingRequestUtil.Paging2Pageable(pagingRequest, columnMap);
        if (pageable != null) {
            org.springframework.data.domain.Page<RequisitionGroup> rowPage =
                    requisitionGroupRepository.findAll(specification,
                            pageable);
            requisitionGroups = rowPage.getContent();
            recordsFiltered = requisitionGroupRepository.count(specification);
        } else {
            requisitionGroups = requisitionGroupRepository.findAll(specification);
            recordsFiltered = requisitionGroups.size();
        }
        page.setRecordsFiltered(recordsFiltered);
        page.setData(requisitionGroups);
        return page;
    }

    @Override
    public Set<String> getDistinctLatestPurchaseOrderNumbers() {
        return purchaseOrderRepository.findDistinctLatestPurchaserOderNumbers();
    }

    @Override
    public Page<PurchaseOrderBillingDetailViewDto> getPurchaseOrderBillingDetailView(String purchaseOrderNumber, PagingRequest pagingRequest) {
        Page<PurchaseOrderBillingDetailViewDto> page = new Page<>(new ArrayList<>());
        page.setDraw(pagingRequest.getDraw());

        Specification<PurchaseOrderBillingDetailView> specificationAll = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(criteriaBuilder.equal(root.get("purchaseOrderNumber"), purchaseOrderNumber));
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
        long recordsTotal = purchaseOrderBillingDetailViewRepository.count(specificationAll);
        page.setRecordsTotal(recordsTotal);
        Map<String, String> columnMap = new HashMap<>();
        Specification<PurchaseOrderBillingDetailView> specification = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(criteriaBuilder.equal(root.get("purchaseOrderNumber"), purchaseOrderNumber));
            return PagingRequestUtil.getPredicate(root, criteriaQuery, criteriaBuilder, pagingRequest, columnMap,
                    predicates);
        };

        long recordsFiltered;
        List<PurchaseOrderBillingDetailView> purchaseOrders;
        Pageable pageable = PagingRequestUtil.Paging2Pageable(pagingRequest, columnMap);
        if (pageable != null) {
            org.springframework.data.domain.Page<PurchaseOrderBillingDetailView> rowPage =
                    purchaseOrderBillingDetailViewRepository.findAll(specification,
                            pageable);
            purchaseOrders = rowPage.getContent();
            recordsFiltered = purchaseOrderBillingDetailViewRepository.count(specification);
        } else {
            purchaseOrders = purchaseOrderBillingDetailViewRepository.findAll(specification);
            recordsFiltered = purchaseOrders.size();
        }
        page.setRecordsFiltered(recordsFiltered);
        page.setData(purchaseOrders.stream()
                .map(PurchaseOrderBillingDetailViewDto::new).collect(Collectors.toList()));
        return page;
    }

    @Override
    public void deletePurchaseOrderBillingById(Long id) {
        purchaseOrderBillingRepository.deleteById(id);
    }

    @Override
    public PurchaseOrderBilling savePurchaseOrderBilling(PurchaseOrderBillingDto purchaseOrderBillingDto) {
        PurchaseOrderBilling purchaseOrderBilling = new PurchaseOrderBilling(purchaseOrderBillingDto);
        return purchaseOrderBillingRepository.save(purchaseOrderBilling);
    }

    @Override
    public Requisition[] getRequisitionsByJobNumberAndSubJob(String jobNumber, String subJob) {
        Set<Requisition> requisitions = requisitionRepository.findByJobNumberAndSubJob(jobNumber, subJob);
        return requisitions.toArray(Requisition[]::new);
    }

    @Override
    public PurchaseOrderGroup savePurchaseOrderGroupComment(String purchaseOrderNumber, String comment) {
        PurchaseOrderGroup purchaseOrderGroup = purchaseOrderGroupRepository.findByPurchaseOrderNumber(purchaseOrderNumber);
        if (purchaseOrderGroup != null) {
            purchaseOrderGroup.setComment(comment);
        } else {
            purchaseOrderGroup = new PurchaseOrderGroup();
            purchaseOrderGroup.setPurchaseOrderNumber(purchaseOrderNumber);
            purchaseOrderGroup.setComment(comment);
            purchaseOrderGroup = purchaseOrderGroupRepository.save(purchaseOrderGroup);
        }
        return purchaseOrderGroup;
    }

    @Override
    public PurchaseOrderGroup savePurchaseOrderGroupFlag(String purchaseOrderNumber, boolean flag) {
        PurchaseOrderGroup purchaseOrderGroup = purchaseOrderGroupRepository.findByPurchaseOrderNumber(purchaseOrderNumber);
        if (purchaseOrderGroup != null) {
            purchaseOrderGroup.setFlag(flag);
        } else {
            purchaseOrderGroup = new PurchaseOrderGroup();
            purchaseOrderGroup.setPurchaseOrderNumber(purchaseOrderNumber);
            purchaseOrderGroup.setFlag(flag);
            purchaseOrderGroup = purchaseOrderGroupRepository.save(purchaseOrderGroup);
        }
        return purchaseOrderGroup;
    }

    @Override
    public PurchaseOrderSubmission savePurchaseOrderSubmission(PurchaseOrderSubmission purchaseOrderSubmission) {
        purchaseOrderSubmissionRepository.save(purchaseOrderSubmission);
        if (!CollectionUtils.isEmpty(purchaseOrderSubmission.getPurchaseOrders())) {
            Set<PurchaseOrder> purchaseOrders = purchaseOrderSubmission.getPurchaseOrders();
            purchaseOrders.forEach(po -> po.setSubmissionId(purchaseOrderSubmission.getId()));
            purchaseOrderRepository.saveAll(purchaseOrders);
        }
        return purchaseOrderSubmission;
    }

    @Override
    public Requisition[] getRequisitionsByPurchaseOrderNumberNull() {
        return requisitionRepository.findByPurchaseOrderNumberNull().toArray(Requisition[]::new);
    }

    @Override
    public Requisition saveRequisition(Requisition requisition) {
        return requisitionRepository.save(requisition);
    }

    @Override
    public void deletePurchaseOrderGroupByPurchaseOrderNumbers(Collection<String> purchaseOrderNumbers) {
        purchaseOrderGroupRepository.deleteByPurchaseOrderNumberNotIn(purchaseOrderNumbers);
    }

    @Override
    public PurchaseOrderGroup[] getPurchaseOrderGroups(Collection<String> purchaseOrderNumbers) {
        if (CollectionUtils.isEmpty(purchaseOrderNumbers)) return new PurchaseOrderGroup[0];
        return purchaseOrderGroupRepository.findByPurchaseOrderNumberIn(purchaseOrderNumbers).toArray(PurchaseOrderGroup[]::new);
    }

    @Override
    public PurchaseOrderGroup savePurchaseOrderGroup(PurchaseOrderGroup purchaseOrderGroup) {
        return purchaseOrderGroupRepository.save(purchaseOrderGroup);
    }

    @Override
    public PurchaseOrderSubmission getLatestPurchaseOrderSubmission() {
        List<PurchaseOrderSubmission> submissions = purchaseOrderSubmissionRepository.findPurchaseOrderSubmissionsOrderByIdDesc();
        if (CollectionUtils.isEmpty(submissions)) return null;
        return submissions.get(0);
    }

    @Override
    public List<PurchaseOrder> savePurchaseOrders(Collection<PurchaseOrder> purchaseOrders) {
        return purchaseOrderRepository.saveAll(purchaseOrders);
    }

    @Override
    public Page<PurchaseVendorRoster> getPurchaseVendorRoster(PagingRequest pagingRequest) {
        Page<PurchaseVendorRoster> page = new Page<>();
        page.setDraw(pagingRequest.getDraw());

        Specification<PurchaseVendorRoster> specification = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };

        Map<String , String> columnMap = new HashMap<>();

        Specification<PurchaseVendorRoster> pageSpecification = (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            return PagingRequestUtil.getPredicate(root, criteriaQuery, criteriaBuilder, pagingRequest, columnMap,
                    predicates);
        };

        List<PurchaseVendorRoster> purchaseVendorRosterList = purchaseVendorRosterRepository.findAll(specification);
        List<PurchaseVendorRoster> pagePurchaseVendorRosterList = purchaseVendorRosterRepository.findAll(pageSpecification);

        page.setRecordsTotal(purchaseVendorRosterList.size());
        page.setRecordsFiltered(pagePurchaseVendorRosterList.size());

        Pageable pageable = PagingRequestUtil.Paging2Pageable(pagingRequest, columnMap);
        if (pageable != null) {
            org.springframework.data.domain.Page<PurchaseVendorRoster> rowPage = purchaseVendorRosterRepository.findAll(pageSpecification,
                    pageable);
            purchaseVendorRosterList = rowPage.getContent();
        }
        page.setData(purchaseVendorRosterList);

        return page;
    }

    @Override
    public List<PurchaseVendorRoster> savePurchaseVendorRoster(Collection<PurchaseVendorRoster> purchaseVendorRosterList) {
        if(CollectionUtils.isEmpty(purchaseVendorRosterList)){return new ArrayList<>();}

        Set<Long> vendorIds = purchaseVendorRosterList.stream()
                .map(e -> e.getVendorId())
                .collect(Collectors.toSet());

        Map<Long, Long> purchaseVendorRosterMap = purchaseVendorRosterRepository.findPurchaseVendorRosterByVendorIdIn(vendorIds).stream()
                .collect(Collectors.toMap(e -> e.getVendorId(), e -> e.getId()));

        purchaseVendorRosterList.forEach( e -> {
            if(purchaseVendorRosterMap.containsKey(e.getVendorId())){
                e.setId(purchaseVendorRosterMap.get(e.getVendorId()));
            }
        });

        return purchaseVendorRosterRepository.saveAll(purchaseVendorRosterList);
    }

    @Override
    public PurchaseOrderBilling getPurchaseOrderBillingDetails(Long id) {
        return purchaseOrderBillingRepository.findByIdFetchingDetails(id);
    }
}
