package com.timaven.provider.service;


import com.timaven.provider.model.dto.ReconcileDto;

import java.time.LocalDate;

public interface ReconcileService {
    ReconcileDto getReconcileData(LocalDate weekEndDate);
}
