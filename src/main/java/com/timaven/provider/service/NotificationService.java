package com.timaven.provider.service;

import com.timaven.provider.model.Notification;

import java.util.List;

public interface NotificationService {
    Notification[] getNotificationsByUserId(Long id);

    boolean updateNotificationRead(Long id, boolean read);

    void saveNotifications(List<Notification> notifications);

    int getNotificationCountByUserIdAndRead(Long userId, boolean read);
}
