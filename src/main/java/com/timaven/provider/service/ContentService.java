package com.timaven.provider.service;

import com.timaven.provider.model.*;
import com.timaven.provider.model.dto.AllocationSubmissionDto;
import com.timaven.provider.model.dto.AllocationTimeFilter;
import com.timaven.provider.model.enums.AllocationSubmissionStatus;
import com.timaven.provider.model.enums.WeeklyProcessStatus;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface ContentService {

    AllocationSubmission saveAllocationSubmission(AllocationSubmission allocationSubmission);

    AllocationSubmissionDto getAllocationSubmissionDtoById(Long allocationSubmissionId);

    void saveReports(Long userId, String teamName, List<ReportRecord> reportRecords, Long projectId);

    TransSubmission saveTransSubmission(TransSubmission transSubmission);

    List<StageTransaction> getStageTransactions(Long projectId, String teamName, LocalDate dateOfService);

    void saveAllocationTimes(Set<AllocationTime> allocationTimes);

    Set<AllocationTime> getAllocationTimesById(Long projectId, Set<Long> allocationTimeIds, Set<String> includes);

    void fetchEmployeeAndCraft(Set<AllocationTime> allocationTimes, Long projectId);

    void fetchEmployeeAndCraft(Set<AllocationTime> allocationTimes, Long projectId, boolean excludePerDiem);

    void saveAllocationSubmissions(Set<AllocationSubmission> allocationSubmissions);

    AllocationSubmission[] getAllocationSubmissionsByProjectIdAndDateAndStatus(LocalDate startDate, LocalDate endDate, Long projectId, String teamName, AllocationSubmissionStatus status, AllocationSubmissionStatus notStatus, List<String> includes);

    StageTransaction[] getStageTransactions(Long projectId, String teamName, LocalDate startDate, LocalDate endDate, List<String> includes);

    TransSubmission[] getTransSubmissionsByProjectIdAndDates(Long projectId, LocalDate startDate, LocalDate endDate, List<String> includes);

    ReportSubmission[] getReportSubmissions(Long projectId, Long userId, String teamName, LocalDate startDate, LocalDate endDate, List<String> includes);

    void deleteAllocationSubmissionById(Long allocationSubmissionId);

    void deleteStageTransactionsByTeamAndDateOfService(String team, LocalDate dateOfService, Long projectId);

    AllocationSubmission getAllocationSubmissionById(Long id);

    AllocationTime[] getAllocationTimesByProjectIdAndDateAndStatus(AllocationTimeFilter filter);

    AllocationSubmissionDto getAllocationSubmissionDtoByDateOfService(Long userId, Long projectId, String teamName, LocalDate dateOfService);

    AllocationSubmissionDto getLastAllocationSubmissionDto(Long projectId, String teamName, LocalDate dateOfService);

    void deleteAllocationTimeById(Long id);

    void deleteStageTransactionsByProjectIdAndEmpIdAndTeamAndDateOfService(Long projectId, String empId, String team, LocalDate dateOfService);

    void updateAllocationSubmissionExport(boolean exported, Collection<Long> ids);

    void updateWeeklyProcessesStatus(Collection<Long> ids, WeeklyProcessStatus status);
}
