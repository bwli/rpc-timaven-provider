package com.timaven.provider.service;

import com.timaven.provider.model.Shift;
import org.springframework.data.repository.query.Param;

import java.util.Set;

public interface ShiftService {
    Set<Shift> getShiftsByProjectId(@Param("projectId") Long projectId);

    Shift getShiftById(Long shiftId);

    Shift saveShift(Shift shift);

    void deleteShiftById(Long id);

    Set<String> getAllShiftNamesByProjectId(Long projectId);

    Shift findDefaultShiftByProjectId(Long projectId);

    Shift getShiftByProjectIdAndName(Long projectId, String name);
}
