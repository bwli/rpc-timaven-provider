package com.timaven.provider.service;

import com.timaven.provider.model.dto.ProjectPayrollDto;
import com.timaven.provider.model.paging.Page;
import com.timaven.provider.model.paging.PagingRequest;

public interface PayrollService {
    Page<ProjectPayrollDto> getProjectPayrollPage(Long projectId, PagingRequest pagingRequest);
}
