package com.timaven.provider.service;

import com.timaven.provider.model.Craft;
import com.timaven.provider.model.dto.CraftDto;
import com.timaven.provider.model.paging.Page;
import com.timaven.provider.model.paging.PagingRequest;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

public interface CraftService {
    Page<CraftDto> getCraftPage(PagingRequest pagingRequest, Long projectId);

    Craft findCraftById(Long id);

    Craft saveCraft(Craft craft);

    Craft[] findByProjectIdAndCodes(Long projectId, Collection<String> codes);

    void saveCrafts(Collection<Craft> crafts);

    Craft[] getCraftsByProjectIdAndDateRange(Long projectId, LocalDate startDate, LocalDate endDate);

    // TM-421
    List<CraftDto> getCraftDtosByProjectIdAndDateRange(Long projectId, LocalDate start);
}
