package com.timaven.provider.service;

import com.timaven.provider.model.RainOut;

import java.util.Set;

public interface RainOutService {
    Set<RainOut> getAllRainOuts(Long projectId);

    RainOut getRainOutById(Long id);

    void saveRainOut(RainOut rainOut);
}
