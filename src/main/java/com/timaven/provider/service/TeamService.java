package com.timaven.provider.service;

import com.timaven.provider.model.UserTeam;

import java.util.Set;

public interface TeamService {
    UserTeam[] getUserTeams(Long userId, Long projectId);

    void saveUserTeams(Long userId, Long projectId, Set<String> teams);
}
