package com.timaven.provider.service;

import com.timaven.provider.model.Employee;
import com.timaven.provider.model.EmployeeFilter;
import com.timaven.provider.model.EmployeeView;
import com.timaven.provider.model.dto.EmpWorkdays;
import com.timaven.provider.model.dto.EmployeeAuditDto;
import com.timaven.provider.model.dto.EmployeeDto;
import com.timaven.provider.model.dto.EmployeeIdNameDto;
import com.timaven.provider.model.paging.Page;
import com.timaven.provider.model.paging.PagingRequest;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface EmployeeService {
    Page<EmployeeDto> getEmployees(PagingRequest pagingRequest, Long projectId);

    Employee findById(Long id);

    Employee saveEmployee(Employee employee);

    Page<EmployeeAuditDto> getEmployeeAudit(PagingRequest pagingRequest, Long projectId);

    Set<Employee> findEmployees(EmployeeFilter filter);

    void saveEmployeeTag(Long projectId, String empId, String tag);

    void deleteEmployeeTag(Long projectId, String empId, String tag);

    String[] findEmployeeTagStringsByProjectId(Long projectId);

    List<Employee> saveEmployees(Collection<Employee> employees);

    void refreshEmployees(Set<Employee> employees);

    Set<EmpWorkdays> findEmployeesDayOff(Collection<String> empIds, LocalDate dateOfService, long days);

    Set<String> getSignInSheets(EmployeeFilter filter);

    Set<String> getTeamNames(EmployeeFilter filter);

    Set<EmployeeIdNameDto> getEmployeeIdNames(EmployeeFilter filter);

    void updateAllocationTimes();

    // TM-412
    /**
     * Get the number of active employee by projectId
     * @param projectId
     * @return
     */
    int countByProjectId(Long projectId);

    EmployeeView[] getEmployeeViewByProjectIdAndEmpId(Set<Long> projectIds, Set<String> employeeIds);

    List<EmployeeView> findEmployeeByProjectIdAndShowAll(Long projectId, Boolean showAll);

    List<Employee>findEmployeeByIds(Set<Long> employeeIds);

    Set<Employee> findByTeamNameAndProjectIdAndDateOfServic(String teamName, Long projectId, LocalDate dateOfService);
}
