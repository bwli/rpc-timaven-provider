package com.timaven.provider.common;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class RuleUtils {
    public static BigDecimal roundTime(BigDecimal target, BigDecimal roundingHours, BigDecimal hoursToBePlus) {
        if (target == null || roundingHours == null || hoursToBePlus == null) return null;
        if (target.compareTo(BigDecimal.ZERO) == 0) return BigDecimal.ZERO;
        BigDecimal wholeHours = target.setScale(0, RoundingMode.DOWN);
        BigDecimal toBeRound = target.remainder(BigDecimal.ONE);
        BigDecimal roundedFractionalHour = roundingHours.multiply((toBeRound.add(hoursToBePlus)
                .divide(roundingHours, 0, RoundingMode.DOWN))).setScale(2, RoundingMode.DOWN);
        return wholeHours.add(roundedFractionalHour);
    }
}
