package com.timaven.provider.scheduling;

import com.timaven.provider.model.Rule;
import com.timaven.provider.model.enums.WeeklyProcessType;
import com.timaven.provider.service.*;
import net.javacrumbs.shedlock.core.LockAssert;
import net.javacrumbs.shedlock.core.LockProvider;
import net.javacrumbs.shedlock.provider.jdbctemplate.JdbcTemplateLockProvider;
import net.javacrumbs.shedlock.spring.annotation.EnableSchedulerLock;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.util.CollectionUtils;

import javax.sql.DataSource;
import java.time.LocalDate;
import java.util.List;

@Configuration
@EnableScheduling
@EnableSchedulerLock(defaultLockAtMostFor = "10m", defaultLockAtLeastFor = "10m")
public class ScheduledFixedRate {

    private final EquipmentService equipmentService;
    private final RuleService ruleService;
    private final WeeklyProcessService weeklyProcessService;
    private final ParserService parserService;
    private final EmployeeService employeeService;

    @Autowired
    public ScheduledFixedRate(EquipmentService equipmentService, RuleService ruleService,
                              WeeklyProcessService weeklyProcessService, ParserService parserService, EmployeeService employeeService) {
        this.equipmentService = equipmentService;
        this.ruleService = ruleService;
        this.weeklyProcessService = weeklyProcessService;
        this.parserService = parserService;
        this.employeeService = employeeService;
    }

    @Bean
    public LockProvider lockProvider(DataSource dataSource) {
        return new JdbcTemplateLockProvider(
                JdbcTemplateLockProvider.Configuration.builder()
                        .withTableName("scheduling.shedlock")
                        .withJdbcTemplate(new JdbcTemplate(dataSource))
                        .usingDbTime() // Works on Postgres, MySQL, MariaDb, MS SQL, Oracle, DB2, HSQL and H2
                        .build()
        );
    }

    @SchedulerLock(name = "checkEquipmentScheduleConflict")
    @Scheduled(cron = "0 0 0 * * *")
    public void checkEquipmentScheduleConflict() {
        // To assert that the lock is held (prevents misconfiguration errors)
        LockAssert.assertLocked();
        equipmentService.checkEquipmentScheduleConflict();
    }

    @SchedulerLock(name = "populateCostIfWeekEndDate")
    @Scheduled(cron = "0 0 0 * * *")
    public void populateCostIfWeekEndDate() {
        // To assert that the lock is held (prevents misconfiguration errors)
        LockAssert.assertLocked();
        List<Rule> rules = ruleService.getRules();
        if(!CollectionUtils.isEmpty(rules)) {
            for (Rule rule : rules) {
                LocalDate yesterday = LocalDate.now().minusDays(1);
                LocalDate weekEndDate = rule.getEndOfWeek(yesterday);
                if (weekEndDate.isEqual(yesterday)) {
                    weeklyProcessService.calculateCost(rule.getProjectId(), weekEndDate, WeeklyProcessType.NORMAL);
                    weeklyProcessService.calculateCost(rule.getProjectId(), weekEndDate, WeeklyProcessType.TLO);
                }
            }
        }
    }

    @SchedulerLock(name = "setupProject")
    @Scheduled(cron = "0 0 0 * * *")
    public void setupProject() {
        // To assert that the lock is held (prevents misconfiguration errors)
        LockAssert.assertLocked();
        parserService.updateProjectSetUp();
        employeeService.updateAllocationTimes();
    }
}
