package com.timaven.provider.controller;

import com.timaven.provider.model.User;
import com.timaven.provider.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('write')")
public class UserWriteController extends BaseController {

    private final AccountService accountService;

    @Autowired
    public UserWriteController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PutMapping(path = "/users/{id}/active")
    public String changeUserValidity(@PathVariable Long id, @RequestParam boolean active) {
        if (id == null) return RESPONSE_FAILED;
        return accountService.changeUserValidity(id, active) ? RESPONSE_SUCCESS : RESPONSE_FAILED;
    }

    @PostMapping(path = "/users", params = {"projectId"})
    public User addUserToProject(@RequestBody User user, @RequestParam Long projectId) {
        accountService.addUser(user, projectId);
        return user;
    }

    @RequestMapping(value = "/users", method = {RequestMethod.POST, RequestMethod.PUT})
    public User addOrUpdateUser(@Valid @RequestBody User user) {
        accountService.saveUser(user);
        return user;
    }
}
