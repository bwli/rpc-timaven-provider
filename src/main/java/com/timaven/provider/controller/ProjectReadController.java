package com.timaven.provider.controller;

import com.timaven.provider.model.Project;
import com.timaven.provider.model.ProjectShift;
import com.timaven.provider.model.paging.Page;
import com.timaven.provider.model.paging.PagingRequest;
import com.timaven.provider.service.ProjectService;
import com.timaven.provider.service.ProjectShiftService;
import com.timaven.provider.service.UserProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('read')")
public class ProjectReadController extends BaseController {

    private final UserProjectService userProjectService;
    private final ProjectService projectService;
    private final ProjectShiftService projectShiftService;

    @Autowired
    public ProjectReadController(UserProjectService userProjectService, ProjectService projectService, ProjectShiftService projectShiftService) {
        this.userProjectService = userProjectService;
        this.projectService = projectService;
        this.projectShiftService = projectShiftService;
    }

    @GetMapping(path = "/projects")
    public Project[] getAllProjects(@RequestParam(value = "includes", required = false) List<String> includes) {
        return userProjectService.getAllProjects(includes);
    }

    @GetMapping(path = "/projects", params = {"username"})
    public Project[] getProjectsByUsername(@RequestParam String username) {
        return userProjectService.getProjectsByUsername(username);
    }

    @PostMapping(path = "/projects-page")
    public Page<Project> getProjectsByUsername(@RequestParam String username, @RequestBody PagingRequest pagingRequest,
                                               @RequestParam boolean isAll) {
        return userProjectService.getProjectsByUsername(username, pagingRequest, isAll);
    }

    @GetMapping(path = "/projects/{id}")
    public Project getProjectsByUsername(@PathVariable Long id) {
        return projectService.getProjectById(id);
    }

    @GetMapping(path = "/projects", params = {"subJob"})
    public Project getProjectsBySubJob(@RequestParam String jobNumber,
                                       @RequestParam(required = false) String subJob) {
        return projectService.getProjectByJobNumberAndSubJob(jobNumber, subJob);
    }

    @GetMapping(path = "/project-shifts")
    public ProjectShift getProjectShiftByProjectId(@RequestParam Long projectId) {
        return projectShiftService.getProjectShiftByProjectId(projectId);
    }

    @PostMapping(path = "/project-collection")
    public Project[] getProjectByProjectIds(@RequestBody Set<Long> projectIds) {
        return projectService.getProjectByProjectIds(projectIds);
    }
}
