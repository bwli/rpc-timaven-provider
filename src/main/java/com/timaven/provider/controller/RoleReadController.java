package com.timaven.provider.controller;

import com.timaven.provider.model.Role;
import com.timaven.provider.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('read')")
public class RoleReadController extends BaseController {

    private final AccountService accountService;

    @Autowired
    public RoleReadController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping(path = "/roles", params = {"roleName"})
    public Role getRoleByName(@RequestParam String roleName) {
        if (!StringUtils.hasText(roleName)) return null;
        return accountService.getRoleByName(roleName);
    }

    @GetMapping(path = "/roles", params = {"roleNames"})
    public Role[] getRolesByNames(@RequestParam List<String> roleNames) {
        if (CollectionUtils.isEmpty(roleNames)) return null;
        return accountService.getRolesByNames(roleNames);
    }
}
