package com.timaven.provider.controller;

import com.timaven.provider.model.RPCUserPool;
import com.timaven.provider.model.RainOut;
import com.timaven.provider.service.RPCUserPoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('read')")
public class RPCUserPoolReadController {

    private final RPCUserPoolService rpcUserPoolService;

    @Autowired
    public RPCUserPoolReadController(RPCUserPoolService rpcUserPoolService) {
        this.rpcUserPoolService = rpcUserPoolService;
    }

    @GetMapping(path = "/userPools")
    public List<RPCUserPool> getUserPools() {
        return rpcUserPoolService.getUserPools();
    }
}
