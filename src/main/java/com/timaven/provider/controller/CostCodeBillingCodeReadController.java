package com.timaven.provider.controller;

import com.timaven.provider.model.CostCodeBillingCode;
import com.timaven.provider.model.CostCodeLog;
import com.timaven.provider.model.dto.CostCodeBillingCodeFilter;
import com.timaven.provider.service.CostCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('read')")
public class CostCodeBillingCodeReadController extends BaseController {

    private final CostCodeService costCodeService;

    @Autowired
    public CostCodeBillingCodeReadController(CostCodeService costCodeService) {
        this.costCodeService = costCodeService;
    }

    @PostMapping(path = "/cost-code-billing-codes-filtered")
    public CostCodeBillingCode[] getCostCodeBillingCodesByCostCodeIds(@RequestBody CostCodeBillingCodeFilter filter) {
        if (CollectionUtils.isEmpty(filter.getCostCodeIds())) return new CostCodeBillingCode[0];
        return costCodeService.getCostCodeBillingCodes(filter.getCostCodeIds(), filter.getIncludes());
    }
}
