package com.timaven.provider.controller;

import com.timaven.provider.model.Employee;
import com.timaven.provider.model.StageRoster;
import com.timaven.provider.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('write')")
public class EmployeeWriteController extends BaseController {

    private final EmployeeService employeeService;

    @Autowired
    public EmployeeWriteController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @RequestMapping(path = "/employees", method = {RequestMethod.POST, RequestMethod.PUT})
    public Employee addOrUpdateEmployee(@RequestBody Employee employee) {
        return employeeService.saveEmployee(employee);
    }

    @PostMapping(path = "/employee-tags")
    public String saveEmployeeTag(@RequestParam(required = false) Long projectId,
                                  @RequestParam String empId,
                                  @RequestParam String tag) {
        employeeService.saveEmployeeTag(projectId, empId, tag);
        return RESPONSE_SUCCESS;
    }

    @DeleteMapping(path = "/employee-tags")
    public String deleteEmployeeTag(@RequestParam(required = false) Long projectId,
                                  @RequestParam String empId,
                                  @RequestParam String tag) {
        employeeService.deleteEmployeeTag(projectId, empId, tag);
        return RESPONSE_SUCCESS;
    }

    @PostMapping(value = "/employee-collection")
    public List<Employee> saveEmployees(@RequestBody Collection<Employee> employees) {
        return employeeService.saveEmployees(employees);
    }
}
