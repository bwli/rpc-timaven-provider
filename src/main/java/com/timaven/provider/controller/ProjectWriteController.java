package com.timaven.provider.controller;

import com.timaven.provider.model.Project;
import com.timaven.provider.model.ProjectShift;
import com.timaven.provider.service.ProjectService;
import com.timaven.provider.service.ProjectShiftService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('write')")
public class ProjectWriteController extends BaseController {

    private final ProjectService projectService;
    private final ProjectShiftService projectShiftService;

    @Autowired
    public ProjectWriteController(ProjectService projectService, ProjectShiftService projectShiftService) {
        this.projectService = projectService;
        this.projectShiftService = projectShiftService;
    }

    @RequestMapping(path = "/projects", method = {RequestMethod.POST, RequestMethod.PUT})
    public Project addOrUpdateProject(@RequestBody Project project,
                                      @RequestParam(value = "userIds", required = false) Set<Long> userIds) {
        project = projectService.saveProject(project);
        projectService.addUsersToProject(project.getId(), userIds);
        return project;
    }

    @DeleteMapping(value = "/projects/{id}")
    public String deleteProjectById(@PathVariable Long id) {
        if (id == null) return RESPONSE_FAILED;
        projectService.deleteProject(id);
        return RESPONSE_SUCCESS;
    }

    @PostMapping(path = "/project-shifts")
    public ProjectShift addProjectShift(@RequestBody ProjectShift projectShift) {
        return projectShiftService.saveProjectShift(projectShift);
    }

    @PutMapping(path = "/project-shifts")
    public String updateProjectShift(@RequestBody ProjectShift projectShift) {
        projectShiftService.upsertProjectShift(projectShift);
        return RESPONSE_SUCCESS;
    }
}
