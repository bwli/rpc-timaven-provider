package com.timaven.provider.controller;

import com.timaven.provider.model.dto.ProjectPayrollDto;
import com.timaven.provider.model.paging.Page;
import com.timaven.provider.model.paging.PagingRequest;
import com.timaven.provider.service.PayrollService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('read')")
public class PayrollReadController extends BaseController {

    private final PayrollService payrollService;

    @Autowired
    public PayrollReadController(PayrollService payrollService) {
        this.payrollService = payrollService;
    }

    @PostMapping(value = "/project-payroll-dtos")
    public Page<ProjectPayrollDto> getProjectPayrollDtos(@RequestParam Long projectId,
                                                         @RequestBody PagingRequest pagingRequest) {
        return payrollService.getProjectPayrollPage(projectId, pagingRequest);
    }
}
