package com.timaven.provider.controller;

import com.timaven.provider.model.*;
import com.timaven.provider.model.dto.PurchaseOrderBillingDetailViewDto;
import com.timaven.provider.model.dto.PurchaseOrderDetailDto;
import com.timaven.provider.model.dto.PurchaseOrderSummaryDto;
import com.timaven.provider.model.dto.RequisitionDto;
import com.timaven.provider.model.paging.Page;
import com.timaven.provider.model.paging.PagingRequest;
import com.timaven.provider.service.PurchaseOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Set;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('read')")
public class PurchaseOrderReadController extends BaseController {

    private final PurchaseOrderService purchaseOrderService;

    @Autowired
    public PurchaseOrderReadController(PurchaseOrderService purchaseOrderService) {
        this.purchaseOrderService = purchaseOrderService;
    }

    @PostMapping(path = "/purchase-order-summary-dtos")
    public Page<PurchaseOrderSummaryDto> getPurchaseOrderSummaryDtoPage(@RequestParam(required = false) Long projectId,
                                                                        @RequestBody PagingRequest pagingRequest) {
        return purchaseOrderService.getLatestPurchaseOrderSummaries(pagingRequest, projectId);
    }

    @PostMapping(path = "/purchase-order-detail-dtos")
    public Page<PurchaseOrderDetailDto> getPurchaseOrderDetailDtoPage(@RequestParam String purchaseOrderNumber,
                                                                      @RequestBody PagingRequest pagingRequest) {
        return purchaseOrderService.getLatestPurchaseOrderDetails(pagingRequest, purchaseOrderNumber);
    }

    @GetMapping(path = "/requisition-dtos")
    public RequisitionDto getRequisitionDtoByRequisitionNumber(@RequestParam String requisitionNumber) {
        return purchaseOrderService.getRequisitionDto(requisitionNumber);
    }

    @PostMapping(path = "/requisition-groups")
    public Page<RequisitionGroup> getRequisitionGroup(@RequestParam(required = false) Long projectId,
                                                      @RequestParam boolean purchaseNumberNull,
                                                      @RequestBody PagingRequest pagingRequest) {
        return purchaseOrderService.getRequisitionGroups(pagingRequest, projectId, purchaseNumberNull);
    }

    @GetMapping(path = "/purchase-order-number-distinct")
    public Set<String> getDistinctLatestPurchaseOrderNumbers() {
        return purchaseOrderService.getDistinctLatestPurchaseOrderNumbers();
    }

    @PostMapping(path = "/purchase-order-billing-detail-view-dtos")
    public Page<PurchaseOrderBillingDetailViewDto> getPurchaseOrderBillingDetailView(@RequestParam String purchaseOrderNumber,
                                                                                     @RequestBody PagingRequest pagingRequest) {
        return purchaseOrderService.getPurchaseOrderBillingDetailView(purchaseOrderNumber, pagingRequest);
    }

    @GetMapping(path = "/requisitions")
    public Requisition[] getRequisitions(@RequestParam String jobNumber, @RequestParam String subJob) {
        return purchaseOrderService.getRequisitionsByJobNumberAndSubJob(jobNumber, subJob);
    }

    @GetMapping(path = "/requisitions-without-po")
    public Requisition[] getRequisitionsWithoutPo() {
        return purchaseOrderService.getRequisitionsByPurchaseOrderNumberNull();
    }

    @PostMapping(path = "/purchase-order-groups-filtered")
    public PurchaseOrderGroup[] getPurchaseOrderGroups(@RequestBody Collection<String> purchaseOrderNumbers) {
        return purchaseOrderService.getPurchaseOrderGroups(purchaseOrderNumbers);
    }

    @GetMapping(path = "/latest-purchase-order-submission")
    public PurchaseOrderSubmission getLatestPurchaseOrderSubmission() {
        return purchaseOrderService.getLatestPurchaseOrderSubmission();
    }

    @PostMapping(path = "/purchase-vendor-roster-list")
    public Page<PurchaseVendorRoster> getPurchaseVendorRoster(@RequestBody PagingRequest pagingRequest) {
        return purchaseOrderService.getPurchaseVendorRoster(pagingRequest);
    }

    @GetMapping(path = "/purchase-order-billing-details")
    public PurchaseOrderBilling getPurchaseOrderBillingDetails(@RequestParam Long id) {
        return purchaseOrderService.getPurchaseOrderBillingDetails(id);
    }
}
