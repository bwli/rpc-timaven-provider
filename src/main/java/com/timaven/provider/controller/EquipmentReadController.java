package com.timaven.provider.controller;

import com.timaven.provider.model.*;
import com.timaven.provider.model.dto.EquipmentPriceDto;
import com.timaven.provider.model.enums.EquipmentAllocationSubmissionStatus;
import com.timaven.provider.model.enums.EquipmentOwnershipType;
import com.timaven.provider.model.paging.Page;
import com.timaven.provider.model.paging.PagingRequest;
import com.timaven.provider.service.EquipmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Set;
import java.util.List;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('read')")
public class EquipmentReadController extends BaseController {

    private final EquipmentService equipmentService;

    @Autowired
    public EquipmentReadController(EquipmentService equipmentService) {
        this.equipmentService = equipmentService;
    }

    @GetMapping(path = "/equipment/{id}")
    public Equipment getEquipmentById(@PathVariable Long id) {
        return equipmentService.getEquipmentById(id);
    }

    @GetMapping(path = "/equipment", params = {"equipmentId"})
    public Equipment getEquipmentByEquipmentId(@RequestParam String equipmentId) {
        return equipmentService.getEquipmentByEquipmentId(equipmentId);
    }

    @PostMapping(path = "/equipment-filtered")
    public Equipment[] getEquipmentByEquipmentId(@RequestBody Set<String> equipmentIds) {
        return equipmentService.getEquipmentByEquipmentIds(equipmentIds);
    }

    @GetMapping(path = "/equipment", params = {"ownershipType"})
    public Equipment[] getEquipmentByType(@RequestParam EquipmentOwnershipType ownershipType) {
        return equipmentService.getAllEquipmentByOwnershipType(ownershipType);
    }

    @GetMapping(path = "/equipment", params = {"active"})
    public Equipment[] getEquipmentByActive(@RequestParam boolean active) {
        return equipmentService.getAllEquipmentByActive(active);
    }

    @GetMapping(path = "/equipment-logs")
    public Equipment[] getEquipmentLog(@RequestParam(required = false) Long projectId) {
        return equipmentService.getRentalAndBeingUsedEquipments(projectId);
    }

    @GetMapping(path = "/equipment-usages")
    public EquipmentUsage[] getEquipmentUsagesByIdsAndDates(@RequestParam(required = false) Long equipmentId,
                                                            @RequestParam(required = false) Long projectId,
                                                            @RequestParam LocalDate startDate,
                                                            @RequestParam(required = false) LocalDate endDate) {
        return equipmentService.getEquipmentUsagesByIdsAndDates(equipmentId, projectId, startDate, endDate);
    }

    @PostMapping(path = "/equipment-prices-filtered")
    public EquipmentPrice[] getEquipmentPrices(@RequestParam Long projectId,
                                               @RequestBody(required = false) Collection<String> equipmentClasses) {
        return equipmentService.getEquipmentPricesByProjectIdAndClasses(projectId, equipmentClasses);
    }

    @GetMapping(path = "/equipment-prices/{id}")
    public EquipmentPrice getEquipmentPriceById(@PathVariable Long id) {
        return equipmentService.getEquipmentPriceById(id);
    }

    @GetMapping(path = "/equipment-prices", params = "projectId")
    public EquipmentPrice[] getEquipmentPricesByProjectId(@RequestParam Long projectId) {
        return equipmentService.getEquipmentPricesByProjectId(projectId);
    }

    @PostMapping(value = "/equipment-price-dtos")
    public Page<EquipmentPriceDto> getEquipmentPriceDtos(@RequestParam Long projectId,
                                                         @RequestBody PagingRequest pagingRequest) {
        return equipmentService.getEquipmentPricesByProjectId(projectId, pagingRequest);
    }

    @GetMapping(value = "/equipment-tags")
    public EquipmentTag[] getEquipmentTags() {
        return equipmentService.getEquipmentTags();
    }

    @PostMapping(value = "/equipment-allocation-time-collection" , params = {"dateOfService"})
    public List<EquipmentAllocationTime> getEquipmentAllocationTimesByDate(@RequestParam Long projectId, @RequestParam LocalDate dateOfService,
                                                                           @RequestParam LocalDate payrollDate,
                                                                           @RequestParam EquipmentAllocationSubmissionStatus status,
                                                                           @RequestParam EquipmentOwnershipType type){
        return equipmentService.getEquipmentAllocationTimesByDate(projectId, dateOfService, payrollDate, status, type);
    }

    @PostMapping(value = "/equipment-allocation-time-collection" , params = {"startDate", "endDate"})
    public List<EquipmentAllocationTime> getEquipmentAllocationTimesByDateRange(@RequestParam Long projectId, @RequestParam LocalDate startDate,
                                                                                @RequestParam LocalDate endDate,
                                                                                @RequestBody Set<EquipmentAllocationSubmissionStatus> statuses){
        return equipmentService.getEquipmentAllocationTimesByDateRange(projectId, startDate, endDate, statuses);
    }

    @PostMapping(value = "/equipment-collection" , params = {"isAll"})
    public List<Equipment> getEquipmentAllOrByActiveTrue(@RequestParam Boolean isAll){
        return equipmentService.getEquipmentAllOrByActiveTrue(isAll);
    }
}
