package com.timaven.provider.controller;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;

import java.beans.PropertyEditorSupport;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.function.Function;

@ControllerAdvice
@Controller
public class GlobalController {

    /**
     * Init the binder to make it able to bind date type
     *
     * @param binder binder
     */
    @InitBinder
    public void initBinder(final WebDataBinder binder) {
        final SimpleDateFormat dateFormat = new SimpleDateFormat
                ("yyyy-MM-dd HH:mm:ss.SSS");
        binder.registerCustomEditor(Date.class, new CustomDateEditor
                (dateFormat, true));
        binder.registerCustomEditor(
                LocalDate.class,
                new Editor<>(
                        text -> {
                            try {
                                if (StringUtils.isEmpty(text)) return null;
                                return LocalDate.parse(text, DateTimeFormatter.ISO_LOCAL_DATE);
                            } catch (DateTimeParseException ex) {
                                return LocalDate.parse(text, DateTimeFormatter.ofPattern("MM/dd/yyyy"));
                            }
                        },
                        DateTimeFormatter.ISO_LOCAL_DATE.toFormat()));

        binder.registerCustomEditor(
                LocalDateTime.class,
                new Editor<>(
                        text -> {
                            try {
                                if (StringUtils.isEmpty(text)) return null;
                                return LocalDateTime.parse(text, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
                            } catch (DateTimeParseException ex) {
                                return LocalDateTime.parse(text, DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss.SSS"));
                            }
                        },
                        DateTimeFormatter.ISO_LOCAL_DATE_TIME.toFormat()));

        binder.registerCustomEditor(
                LocalTime.class,
                new Editor<>(
                        text -> {
                            try {
                                if (StringUtils.isEmpty(text)) return null;
                                return LocalTime.parse(text, DateTimeFormatter.ISO_LOCAL_TIME);
                            } catch (DateTimeParseException ex) {
                                return LocalTime.parse(text, DateTimeFormatter.ofPattern("HH:mm:ss.SSS"));
                            }
                        },
                        DateTimeFormatter.ISO_LOCAL_TIME.toFormat()));
    }

    private static class Editor<T> extends PropertyEditorSupport {

        private final Function<String, T> parser;
        private final Format format;

        Editor(Function<String, T> parser, Format format) {

            this.parser = parser;
            this.format = format;
        }

        public String getAsText() {
            return null == getValue() ? "" : format.format(getValue());
        }

        public void setAsText(String text) {

            setValue(this.parser.apply(text));
        }
    }
}
