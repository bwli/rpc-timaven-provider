package com.timaven.provider.controller;

import com.timaven.provider.model.RainOut;
import com.timaven.provider.service.RainOutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('write')")
public class RainOutWriteController extends BaseController {

    private final RainOutService rainOutService;

    @Autowired
    public RainOutWriteController(RainOutService rainOutService) {
        this.rainOutService = rainOutService;
    }

    @RequestMapping(path = "/rain-outs", method = {RequestMethod.POST, RequestMethod.PUT})
    public RainOut saveRainOut(@RequestBody RainOut rainOut) {
        rainOutService.saveRainOut(rainOut);
        return rainOut;
    }
}
