package com.timaven.provider.controller;

import com.timaven.provider.model.ActivityCode;
import com.timaven.provider.service.ActivityCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('read')")
public class ActivityCodeReadController extends BaseController {

    private final ActivityCodeService activityCodeService;

    @Autowired
    public ActivityCodeReadController(ActivityCodeService activityCodeService) {
        this.activityCodeService = activityCodeService;
    }

    @GetMapping(path = "/activity-codes")
    public List<ActivityCode> getActivateCodes() {
        return activityCodeService.getActivityCode();
    }


}
