package com.timaven.provider.controller;

import com.timaven.provider.model.BillingCode;
import com.timaven.provider.model.BillingCodeOverride;
import com.timaven.provider.model.BillingCodeType;
import com.timaven.provider.model.dto.SaveCostCodeAssociationDto;
import com.timaven.provider.service.BillingCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('write')")
public class BillingCodeWriteController extends BaseController {

    private final BillingCodeService billingCodeService;

    @Autowired
    public BillingCodeWriteController(BillingCodeService billingCodeService) {
        this.billingCodeService = billingCodeService;
    }

    @PostMapping(path = "/billing-code-types/level")
    public String saveBillingCodeTypes(@RequestParam List<Long> ids) {
        billingCodeService.resetCostCodeLevel();
        billingCodeService.saveCostCodeStructure(ids);
        return RESPONSE_SUCCESS;
    }

    @RequestMapping(path = "/billing-code-types", method = {RequestMethod.POST, RequestMethod.PUT})
    public BillingCodeType addOrUpdateBillingCodeType(@RequestBody BillingCodeType billingCodeType) {
        return billingCodeService.saveBillingCodeType(billingCodeType);
    }

    @PostMapping(path = "/cost-code-billing-codes")
    public String saveCostCodeBillingCodeAssociation(@RequestParam(required = false) Long projectId,
                                                     @RequestBody SaveCostCodeAssociationDto dto) {
        billingCodeService.saveCostCodeBillingCode(projectId, dto);
        return RESPONSE_SUCCESS;
    }

    @DeleteMapping(path = "/billing-code-types/{id}")
    public String deleteBillingCodeType(@PathVariable Long id) {
        billingCodeService.deleteBillingCodeTypeById(id);
        return RESPONSE_SUCCESS;
    }

    @DeleteMapping(path = "/billing-codes/{id}")
    public String deleteBillingCode(@PathVariable Long id) {
        billingCodeService.deleteBillingCodeById(id);
        return RESPONSE_SUCCESS;
    }

    @DeleteMapping(path = "/billing-code-overrides")
    public String deleteBillingCodeOverride(@RequestParam Long projectId, @RequestParam Long billingCodeId) {
        billingCodeService.deleteBillingCodeOverrideByBillingCodeIdAndProjectId(billingCodeId, projectId);
        return RESPONSE_SUCCESS;
    }

    @RequestMapping(path = "/billing-code-overrides", method = {RequestMethod.POST, RequestMethod.PUT})
    public BillingCodeOverride addOrUpdateBillingCodeOverride(@RequestBody BillingCodeOverride billingCodeOverride) {
        return billingCodeService.saveBillingCodeOverride(billingCodeOverride);
    }

    @PostMapping(path = "/billing-code-override-collection")
    public List<BillingCodeOverride> addBillingCodeOverrides(@RequestBody Collection<BillingCodeOverride> billingCodeOverrides) {
        return billingCodeService.saveBillingCodeOverrides(billingCodeOverrides);
    }

    @RequestMapping(path = "/billing-codes", method = {RequestMethod.POST, RequestMethod.PUT})
    public BillingCode addOrUpdateBillingCode(@RequestBody BillingCode billingCode) {
        return billingCodeService.saveBillingCode(billingCode);
    }

    @PostMapping(path = "/billing-code-collection")
    public List<BillingCode> addBillingCodes(@RequestBody Collection<BillingCode> billingCodes) {
        return billingCodeService.saveBillingCodes(billingCodes);
    }
}
