package com.timaven.provider.controller;

import com.timaven.provider.model.Shift;
import com.timaven.provider.model.WeeklyProcess;
import com.timaven.provider.service.WeeklyProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('write')")
public class WeeklyProcessWriteController extends BaseController {

    private final WeeklyProcessService weeklyProcessService;

    @Autowired
    public WeeklyProcessWriteController(WeeklyProcessService weeklyProcessService) {
        this.weeklyProcessService = weeklyProcessService;
    }

    @RequestMapping(path = "/weekly-processes", method = {RequestMethod.POST, RequestMethod.PUT})
    public WeeklyProcess addOrUpdateWeeklyProcess(@RequestBody WeeklyProcess weeklyProcess) {
        return weeklyProcessService.save(weeklyProcess);
    }
}
