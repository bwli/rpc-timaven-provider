package com.timaven.provider.controller;

import com.timaven.provider.model.ActivityCode;
import com.timaven.provider.service.ActivityCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('write')")
public class ActivityCodeWriteController extends BaseController {

    private final ActivityCodeService activityCodeService;

    @Autowired
    public ActivityCodeWriteController(ActivityCodeService activityCodeService) {
        this.activityCodeService = activityCodeService;
    }

    @PostMapping(value = "/activity-codes")
    public ActivityCode addActivityCode(@RequestBody ActivityCode activityCode) {
        return activityCodeService.saveActivityCode(activityCode);
    }

}
