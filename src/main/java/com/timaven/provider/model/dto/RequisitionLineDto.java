package com.timaven.provider.model.dto;

import com.timaven.provider.model.Requisition;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class RequisitionLineDto {
    private Integer lineNumber;
    private String partNumber;
    private String partDescription;
    private Integer quantity;
    private BigDecimal unitCost;
    private BigDecimal extendedCost;
    private String formattedCostDistribution;
    private String costType;

    public RequisitionLineDto() {
    }

    public RequisitionLineDto(Requisition requisition) {
        this.lineNumber = requisition.getLineNumber();
        this.partNumber = requisition.getPartNumber();
        this.partDescription = requisition.getPartDescription();
        this.quantity = requisition.getQuantity();
        this.unitCost = requisition.getUnitCost();
        this.extendedCost = requisition.getExtendedCost();
        this.formattedCostDistribution = requisition.getFormattedCostDistribution();
        this.costType = requisition.getCostType();
    }
}
