package com.timaven.provider.model.dto;

import com.timaven.provider.model.CostCodeAssociation;
import lombok.Data;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

@Data
public class CostCodeAssociationDto {
    private Long id;
    private String costCode;
    private List<String> tags = new ArrayList<>();
    private String description;
    private String jobNumber;
    private String subJob;
    private Long projectId;
    private String level1;
    private String level2;
    private String level3;
    private String level4;
    private String level5;
    private String level6;
    private String level7;
    private String level8;
    private String level9;
    private String level10;
    private String level11;
    private String level12;
    private String level13;
    private String level14;
    private String level15;
    private String level16;
    private String level17;
    private String level18;
    private String level19;
    private String level20;
    /**
     * TM 389
     */
    private String costCodeSegment;

    public CostCodeAssociationDto(CostCodeAssociation association) {
        this.id = association.getId();
        this.costCode = association.getCostCode();;
        this.description = association.getDescription();
        this.jobNumber = association.getJobNumber();
        this.subJob = association.getSubJob();
        this.projectId = association.getProjectId() == null ? 0 : association.getProjectId();
        this.level1 = association.getLevel1();
        this.level2 = association.getLevel2();
        this.level3 = association.getLevel3();
        this.level4 = association.getLevel4();
        this.level5 = association.getLevel5();
        this.level6 = association.getLevel6();
        this.level7 = association.getLevel7();
        this.level8 = association.getLevel8();
        this.level9 = association.getLevel9();
        this.level10 = association.getLevel10();
        this.level11 = association.getLevel11();
        this.level12 = association.getLevel12();
        this.level13 = association.getLevel13();
        this.level14 = association.getLevel14();
        this.level15 = association.getLevel15();
        this.level16 = association.getLevel16();
        this.level17 = association.getLevel17();
        this.level18 = association.getLevel18();
        this.level19 = association.getLevel19();
        this.level20 = association.getLevel20();
        this.costCodeSegment = association.getCostCodeSegment();
        if (StringUtils.hasText(association.getTags())) {
            this.tags = Arrays.stream(association.getTags().split(",")).sorted().collect(Collectors.toList());
        }
    }
}
