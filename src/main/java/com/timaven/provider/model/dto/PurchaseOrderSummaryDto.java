package com.timaven.provider.model.dto;

import com.timaven.provider.model.PurchaseOrderSummary;
import com.timaven.provider.util.NumberUtil;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class PurchaseOrderSummaryDto {
    private String purchaseOrderNumber;
    private LocalDate purchaseOrderDate;
    private String vendorName;
    private String requestor;
    private String extendedCost;
    private BigDecimal quantity;
    private BigDecimal billedQuantity;
    private String billedAmount;
    private LocalDate billedOn;
    private String comment;
    private boolean flag;

    public PurchaseOrderSummaryDto(PurchaseOrderSummary summary) {
        this.purchaseOrderNumber = summary.getPurchaseOrderNumber();
        this.purchaseOrderDate = summary.getPurchaseOrderDate();
        this.vendorName = summary.getVendorName();
        this.requestor = summary.getRequestor();
        this.extendedCost = NumberUtil.currencyFormat(summary.getTotalExtendedCost());
        if (null != summary.getQuantity()) {
            this.quantity = new BigDecimal(summary.getQuantity().stripTrailingZeros().toPlainString());
        }
        if (null != summary.getBilledQuantity()) {
            this.billedQuantity = new BigDecimal(summary.getBilledQuantity().stripTrailingZeros().toPlainString());
        }
        this.billedAmount = NumberUtil.currencyFormat(summary.getBilledAmount());
        this.billedOn = summary.getBilledOn();
        this.comment = summary.getComment();
        this.flag = summary.isFlag();
    }
}
