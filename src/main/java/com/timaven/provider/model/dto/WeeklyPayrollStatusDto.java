package com.timaven.provider.model.dto;

import lombok.Data;

import java.util.List;

@Data
public class WeeklyPayrollStatusDto {
    private List<Long> ids;
    private String status;
}
