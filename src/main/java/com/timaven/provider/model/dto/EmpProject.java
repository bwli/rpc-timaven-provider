package com.timaven.provider.model.dto;

import lombok.Data;
import lombok.NonNull;

@Data
public class EmpProject {
    @NonNull
    private final String empId;
    @NonNull
    private final Long projectId;
}
