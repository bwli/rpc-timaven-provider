package com.timaven.provider.model.dto;

import com.timaven.provider.model.PurchaseOrderDetails;
import com.timaven.provider.util.NumberUtil;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class PurchaseOrderDetailDto {
    private String purchaseOrderNumber;
    private LocalDate purchaseOrderDate;
    private String vendorName;
    private String requestor;
    private String requisitionNumber;
    private Integer lineNumber;
    private String partDescription;
    private BigDecimal quantity;
    private BigDecimal quantityReceived;
    private String unitCost;
    private String extendedCost;
    private String formattedCostDistribution;
    private String costType;
    private BigDecimal billedQuantity;

    public PurchaseOrderDetailDto(PurchaseOrderDetails order) {
        this.purchaseOrderNumber = order.getPurchaseOrderNumber();
        this.purchaseOrderDate = order.getPurchaseOrderDate();
        this.vendorName = order.getVendorName();
        this.requestor = order.getRequestor();
        this.requisitionNumber = order.getRequisitionNumber();
        this.lineNumber = order.getLineNumber();
        this.partDescription = order.getPartDescription();
        if (null != order.getQuantity()) {
            this.quantity = new BigDecimal(order.getQuantity().stripTrailingZeros().toPlainString());
        }
        if (null != order.getQuantityReceived()) {
            this.quantityReceived = new BigDecimal(order.getQuantityReceived().stripTrailingZeros().toPlainString());
        }
        this.unitCost = NumberUtil.currencyFormat(order.getUnitCost());
        this.extendedCost = NumberUtil.currencyFormat(order.getExtendedCost());
        this.formattedCostDistribution = order.getFormattedCostDistribution();
        this.costType = order.getCostType();
        if (null != order.getBilledQuantity()) {
            this.billedQuantity = new BigDecimal(order.getBilledQuantity().stripTrailingZeros().toPlainString());
        }
    }
}
