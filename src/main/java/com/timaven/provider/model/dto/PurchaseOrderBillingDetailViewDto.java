package com.timaven.provider.model.dto;

import com.timaven.provider.model.PurchaseOrderBillingDetailView;
import com.timaven.provider.util.NumberUtil;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class PurchaseOrderBillingDetailViewDto {
    private Long id;
    private Long billingId;
    private String billingNumber;
    private String invoiceNumber;
    private String part;
    private BigDecimal billingQuantity;
    private String amount;
    private String tax;

    public PurchaseOrderBillingDetailViewDto(PurchaseOrderBillingDetailView view) {
        this.id = view.getId();
        this.billingId = view.getBillingId();
        this.billingNumber = view.getBillingNumber();
        this.invoiceNumber = view.getInvoiceNumber();
        this.part = view.getPart();
        if (view.getBillingQuantity() != null) {
            this.billingQuantity = new BigDecimal(view.getBillingQuantity().stripTrailingZeros().toPlainString());
        }
        if (view.getAmount() != null) {
            this.amount = NumberUtil.currencyFormat(view.getAmount());
        }
        if (view.getTax() != null) {
            this.tax = NumberUtil.currencyFormat(view.getTax());
        }
    }
}
