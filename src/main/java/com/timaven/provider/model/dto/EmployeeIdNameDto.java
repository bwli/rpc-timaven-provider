package com.timaven.provider.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Comparator;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeIdNameDto {
    private Long id;
    private String fullName;
    private String craft;
    private String crew;
}
