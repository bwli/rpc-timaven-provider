package com.timaven.provider.model.dto;

import lombok.Data;

@Data
public class ProjectsDashboardDto {
    private Long projectId;
    private String jobSubJob;
    private String description;
    private Integer empCount = 0;
    private String hours;
    private String clientCost;
    private String committedCost;
    private String payrollProcessed;
}
