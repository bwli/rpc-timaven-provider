package com.timaven.provider.model.dto;

import lombok.Data;

@Data
public class TenantDto {
    private String name;
    private String host;
}
