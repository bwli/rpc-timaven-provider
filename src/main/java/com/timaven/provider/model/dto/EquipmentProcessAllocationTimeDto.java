package com.timaven.provider.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
public class EquipmentProcessAllocationTimeDto {
    private Long id;
    private String equipmentId;
    private LocalDate dateOfService;
    private LocalDate payrollDate;

    private Map<String, EquipmentProcessCostCodePercDto> equipmentCostCodePercMap;

    public Map<String, EquipmentProcessCostCodePercDto> getEquipmentCostCodePercMap() {
        if (null == equipmentCostCodePercMap) {
            equipmentCostCodePercMap = new HashMap<>();
        }
        return equipmentCostCodePercMap;
    }
}
