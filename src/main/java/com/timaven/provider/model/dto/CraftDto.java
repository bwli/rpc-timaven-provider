package com.timaven.provider.model.dto;

import com.timaven.provider.model.CraftView;
import com.timaven.provider.util.NumberUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@EqualsAndHashCode(of = {"projectId", "code", "startDate", "endDate"})
@NoArgsConstructor
public class CraftDto {
    private Long id;
    private String code;
    private Long projectId;
    private String billableST;
    private String billableOT;
    private String billableDT;
    private String description;
    private String perDiemAmt;
    private String perDiemName;
    private Long perDiemId;
    private String rigPay;
    private LocalDate startDate;
    private LocalDate endDate;
    private String company;

    public CraftDto(CraftView craftView) {
        this.id = craftView.getId();
        this.code = craftView.getCode();
        this.projectId = craftView.getProjectId();
        this.billableST = NumberUtil.currencyFormat(craftView.getBillableST());
        this.billableOT = NumberUtil.currencyFormat(craftView.getBillableOT());
        this.billableDT = NumberUtil.currencyFormat(craftView.getBillableDT());
        this.description = craftView.getDescription();
        this.perDiemId = craftView.getPerDiemId();
        this.perDiemAmt = NumberUtil.currencyFormat(craftView.getPerDiem());
        this.perDiemName = craftView.getPerDiemName();
        this.rigPay = NumberUtil.currencyFormat(craftView.getRigPay());
        this.startDate = craftView.getStartDate();
        this.endDate = craftView.getEndDate();
        this.company = craftView.getCompany();
    }
}
