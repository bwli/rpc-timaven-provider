package com.timaven.provider.model.dto;

import com.timaven.provider.model.CostCodeTagAssociation;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Comparator;

@Data
@NoArgsConstructor
public class CostCodeTagAssociationDto implements Comparable<CostCodeTagAssociationDto> {
    private Long id;
    private String tagType;
    private String typeDescription;
    private String codeName;
    private String codeDescription;

    public CostCodeTagAssociationDto(CostCodeTagAssociation costCodeTagAssociation) {
        this.id = costCodeTagAssociation.getId();
        this.tagType = costCodeTagAssociation.getTagType();
        this.typeDescription = costCodeTagAssociation.getTypeDescription();
        this.codeName = costCodeTagAssociation.getCodeName();
        this.codeDescription = costCodeTagAssociation.getCodeDescription();
    }

    @Override
    public int compareTo(CostCodeTagAssociationDto o) {
        return Comparator.comparing(CostCodeTagAssociationDto::getTagType)
                .thenComparing(CostCodeTagAssociationDto::getCodeName)
                .compare(this, o);
    }
}
