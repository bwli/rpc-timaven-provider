package com.timaven.provider.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

@Getter
@Setter
@Entity
@Table(name = "purchase_order", schema = "project")
public class PurchaseOrder {
    private Long id;
    private String purchaseOrderNumber;
    private LocalDate purchaseOrderDate;
    private Integer vendorNumber;
    private String vendorName;
    private String confirmTo;
    private String requestor;
    private String buyer;
    private String requisitionNumber;
    private String jobNumber;
    private String subJob;
    private String address1;
    private String deliveryPoint;
    private String terms;
    private String reference;
    private BigDecimal stateTax;
    private BigDecimal localTax;
    private String purchaseOrderLocation;
    private String poType;
    private Integer lineNumber;
    private String partNumber;
    private String partDescription;
    private LocalDate deliveryDate;
    private BigDecimal quantity;
    private BigDecimal adjustQuantityOrdered;
    private BigDecimal quantityReceived;
    private BigDecimal unitCost;
    private BigDecimal extendedCost;
    private String priceCode;
    private BigDecimal originalPoDollarAmount;
    private BigDecimal currentPoDollarAmount;
    private BigDecimal openAmount;
    private String formattedCostDistribution;
    private String costType;
    private String glAccountNumberFormatted;
    private LocalDateTime createdAt;
    private Long submissionId;
    private PurchaseOrderSubmission purchaseOrderSubmission;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "purchase_order_number")
    public String getPurchaseOrderNumber() {
        return purchaseOrderNumber;
    }

    public void setPurchaseOrderNumber(String purchaseOrderNumber) {
        this.purchaseOrderNumber = purchaseOrderNumber;
    }

    @Basic
    @Column(name = "purchase_order_date")
    public LocalDate getPurchaseOrderDate() {
        return purchaseOrderDate;
    }

    public void setPurchaseOrderDate(LocalDate purchaseOrderDate) {
        this.purchaseOrderDate = purchaseOrderDate;
    }

    @Basic
    @Column(name = "vendor_number")
    public Integer getVendorNumber() {
        return vendorNumber;
    }

    public void setVendorNumber(Integer vendorNumber) {
        this.vendorNumber = vendorNumber;
    }

    @Basic
    @Column(name = "vendor_name")
    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    @Basic
    @Column(name = "confirm_to")
    public String getConfirmTo() {
        return confirmTo;
    }

    public void setConfirmTo(String confirmTo) {
        this.confirmTo = confirmTo;
    }

    @Basic
    @Column(name = "requestor")
    public String getRequestor() {
        return requestor;
    }

    public void setRequestor(String requestor) {
        this.requestor = requestor;
    }

    @Basic
    @Column(name = "buyer")
    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    @Basic
    @Column(name = "requisition_number")
    public String getRequisitionNumber() {
        return requisitionNumber;
    }

    public void setRequisitionNumber(String requisitionNumber) {
        this.requisitionNumber = requisitionNumber;
    }

    @Basic
    @Column(name = "job_number")
    public String getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }

    @Basic
    @Column(name = "sub_job")
    public String getSubJob() {
        return subJob;
    }

    public void setSubJob(String subJob) {
        this.subJob = subJob;
    }

    @Basic
    @Column(name = "address_1")
    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    @Basic
    @Column(name = "delivery_point")
    public String getDeliveryPoint() {
        return deliveryPoint;
    }

    public void setDeliveryPoint(String deliveryPoint) {
        this.deliveryPoint = deliveryPoint;
    }

    @Basic
    @Column(name = "terms")
    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    @Basic
    @Column(name = "reference")
    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    @Basic
    @Column(name = "state_tax")
    public BigDecimal getStateTax() {
        return stateTax;
    }

    public void setStateTax(BigDecimal stateTax) {
        this.stateTax = stateTax;
    }

    @Basic
    @Column(name = "local_tax")
    public BigDecimal getLocalTax() {
        return localTax;
    }

    public void setLocalTax(BigDecimal localTax) {
        this.localTax = localTax;
    }

    @Basic
    @Column(name = "purchase_order_location")
    public String getPurchaseOrderLocation() {
        return purchaseOrderLocation;
    }

    public void setPurchaseOrderLocation(String purchaseOrderLocation) {
        this.purchaseOrderLocation = purchaseOrderLocation;
    }

    @Basic
    @Column(name = "po_type")
    public String getPoType() {
        return poType;
    }

    public void setPoType(String poType) {
        this.poType = poType;
    }

    @Basic
    @Column(name = "line_number")
    public Integer getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(Integer lineNumber) {
        this.lineNumber = lineNumber;
    }

    @Basic
    @Column(name = "part_number")
    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    @Basic
    @Column(name = "part_description")
    public String getPartDescription() {
        return partDescription;
    }

    public void setPartDescription(String partDescription) {
        this.partDescription = partDescription;
    }

    @Basic
    @Column(name = "delivery_date")
    public LocalDate getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(LocalDate deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    @Basic
    @Column(name = "quantity")
    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    @Basic
    @Column(name = "adjust_quantity_ordered")
    public BigDecimal getAdjustQuantityOrdered() {
        return adjustQuantityOrdered;
    }

    public void setAdjustQuantityOrdered(BigDecimal adjustQuantityOrdered) {
        this.adjustQuantityOrdered = adjustQuantityOrdered;
    }

    @Basic
    @Column(name = "quantity_received")
    public BigDecimal getQuantityReceived() {
        return quantityReceived;
    }

    public void setQuantityReceived(BigDecimal quantityReceived) {
        this.quantityReceived = quantityReceived;
    }

    @Basic
    @Column(name = "unit_cost")
    public BigDecimal getUnitCost() {
        return unitCost;
    }

    public void setUnitCost(BigDecimal unitCost) {
        this.unitCost = unitCost;
    }

    @Basic
    @Column(name = "extended_cost")
    public BigDecimal getExtendedCost() {
        return extendedCost;
    }

    public void setExtendedCost(BigDecimal extendedCost) {
        this.extendedCost = extendedCost;
    }

    @Basic
    @Column(name = "price_code")
    public String getPriceCode() {
        return priceCode;
    }

    public void setPriceCode(String priceCode) {
        this.priceCode = priceCode;
    }

    @Basic
    @Column(name = "original_po_dollar_amount")
    public BigDecimal getOriginalPoDollarAmount() {
        return originalPoDollarAmount;
    }

    public void setOriginalPoDollarAmount(BigDecimal originalPoDollarAmount) {
        this.originalPoDollarAmount = originalPoDollarAmount;
    }

    @Basic
    @Column(name = "current_po_dollar_amount")
    public BigDecimal getCurrentPoDollarAmount() {
        return currentPoDollarAmount;
    }

    public void setCurrentPoDollarAmount(BigDecimal currentPoDollarAmount) {
        this.currentPoDollarAmount = currentPoDollarAmount;
    }

    @Basic
    @Column(name = "open_amount")
    public BigDecimal getOpenAmount() {
        return openAmount;
    }

    public void setOpenAmount(BigDecimal openAmount) {
        this.openAmount = openAmount;
    }

    @Basic
    @Column(name = "formatted_cost_distribution")
    public String getFormattedCostDistribution() {
        return formattedCostDistribution;
    }

    public void setFormattedCostDistribution(String formattedCostDistribution) {
        this.formattedCostDistribution = formattedCostDistribution;
    }

    @Basic
    @Column(name = "cost_type")
    public String getCostType() {
        return costType;
    }

    public void setCostType(String costType) {
        this.costType = costType;
    }

    @Basic
    @Column(name = "gl_account_number_formatted")
    public String getGlAccountNumberFormatted() {
        return glAccountNumberFormatted;
    }

    public void setGlAccountNumberFormatted(String glAccountNumberFormatted) {
        this.glAccountNumberFormatted = glAccountNumberFormatted;
    }

    @Basic
    @Column(name = "created_at")
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "submission_id", referencedColumnName = "id", columnDefinition = "BIGINT", updatable = false, insertable = false)
    @JsonBackReference
    public PurchaseOrderSubmission getPurchaseOrderSubmission() {
        return purchaseOrderSubmission;
    }

    public void setPurchaseOrderSubmission(PurchaseOrderSubmission purchaseOrderSubmission) {
        this.purchaseOrderSubmission = purchaseOrderSubmission;
    }

    @Column(name = "submission_id")
    public Long getSubmissionId() {
        return submissionId;
    }

    public void setSubmissionId(Long submissionId) {
        this.submissionId = submissionId;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PurchaseOrder that = (PurchaseOrder) o;
        if (id != null && that.id != null) return id.equals(that.id);
        return false;
    }

    @Override
    public int hashCode() {
        if (id != null) return Objects.hash(id);
        return super.hashCode();
    }
}
