package com.timaven.provider.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@Entity
@EqualsAndHashCode(of = "id")
@Table(name = "craft_view", schema = "project")
public class CraftView {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "code")
    private String code;

    @Column(name = "project_id")
    private Long projectId;

    @Column(name = "billable_st")
    private BigDecimal billableST;

    @Column(name = "billable_ot")
    private BigDecimal billableOT;

    @Column(name = "billable_dt")
    private BigDecimal billableDT;

    @Column(name = "description")
    private String description;

    @Column(name = "per_diem")
    private BigDecimal perDiem;

    @Column(name = "per_diem_id")
    private Long perDiemId;

    @Column(name = "per_diem_name")
    private String perDiemName;

    @Column(name = "rig_pay")
    private BigDecimal rigPay;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "end_date")
    private LocalDate endDate;

    @Column(name = "company")
    private String company;
}
