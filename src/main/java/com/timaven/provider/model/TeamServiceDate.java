package com.timaven.provider.model;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class TeamServiceDate implements Comparable<TeamServiceDate> {
    @NotNull
    private final String team;
    @NotNull
    private final LocalDate dateOfService;

    @Override
    public int compareTo(TeamServiceDate teamServiceDate) {
        int result = teamServiceDate.dateOfService.compareTo(dateOfService);
        if (result == 0) {
            return team.compareTo(teamServiceDate.team);
        }
        return result;
    }
}
