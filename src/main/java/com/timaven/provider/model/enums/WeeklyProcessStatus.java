package com.timaven.provider.model.enums;

public enum WeeklyProcessStatus {
    REJECT,
    FINALIZE,
    UNFINALIZE;
}
