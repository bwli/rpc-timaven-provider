package com.timaven.provider.model.enums;

public enum CostCodeType {
    DEFAULT,
    PERDIEM,
    MOB;

    public int getValue() {
        return switch (this) {
            case DEFAULT -> 0;
            case PERDIEM -> 1;
            case MOB -> 2;
        };
    }
}
