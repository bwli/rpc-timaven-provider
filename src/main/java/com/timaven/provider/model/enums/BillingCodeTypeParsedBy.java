package com.timaven.provider.model.enums;

public enum BillingCodeTypeParsedBy {
    CODE_NAME,
    CLIENT_ALIAS;
}
