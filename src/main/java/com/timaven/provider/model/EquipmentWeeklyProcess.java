package com.timaven.provider.model;

import com.timaven.provider.model.enums.EquipmentWeeklyProcessType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Audited
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "equipment_weekly_process", schema = "time")
public class EquipmentWeeklyProcess {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "approve_user_id", columnDefinition = "BIGINT")
    private Long approveUserId;
    @Column(name = "review_user_id", columnDefinition = "BIGINT")
    private Long reviewUserId;
    @Column(name = "finalize_user_id", columnDefinition = "BIGINT")
    private Long finalizeUserId;
    @Column(name = "report_user_id", columnDefinition = "BIGINT")
    private Long reportUserId;
    @Column(name = "approved_at")
    private LocalDateTime approvedAt;
    @Column(name = "reviewed_at")
    private LocalDateTime reviewedAt;
    @Column(name = "finalized_at")
    private LocalDateTime finalizedAt;
    @Column(name = "report_generated_at")
    private LocalDateTime reportGeneratedAt;

    @Column(name = "type")
    private EquipmentWeeklyProcessType type = EquipmentWeeklyProcessType.NORMAL;

    @Column(name = "week_end_date")
    private LocalDate weekEndDate;

    @Column(name = "project_id")
    private Long projectId;

    @Column(name = "billable_amount")
    private BigDecimal billableAmount;

    @Column(name = "base_amount")
    private BigDecimal baseAmount;

    @Column(name = "total_hours")
    private BigDecimal totalHours;

    @Column(name = "created_at", insertable = false, updatable = false)
    private LocalDateTime createdAt;

    @NotAudited
    @OneToMany(mappedBy = "equipmentWeeklyProcess", fetch = FetchType.LAZY)
    private List<EquipmentAllocationTime> equipmentAllocationTimes;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = null;
        if (null != id) {
            hcb = new HashCodeBuilder();
            hcb.append(id);
        } else if (null != projectId && null != weekEndDate) {
            hcb = new HashCodeBuilder();
            hcb.append(projectId);
            hcb.append(weekEndDate);
            hcb.append(type);
        }
        if (null != hcb) {
            return hcb.toHashCode();
        }
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EquipmentWeeklyProcess)) return false;
        EquipmentWeeklyProcess that = (EquipmentWeeklyProcess) o;
        EqualsBuilder eb = null;
        if (null != id) {
            eb = new EqualsBuilder();
            eb.append(id, that.id);
        } else if (null != projectId && null != weekEndDate) {
            eb = new EqualsBuilder();
            eb.append(projectId, that.projectId);
            eb.append(weekEndDate, that.weekEndDate);
            eb.append(type, that.type);
        }
        if (null != eb) {
            return eb.isEquals();
        }
        return super.equals(o);
    }
}
