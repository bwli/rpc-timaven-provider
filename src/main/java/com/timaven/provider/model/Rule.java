package com.timaven.provider.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;


@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Rule.class)
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@Entity
@Table(name = "rule", schema = "rule")
public class Rule implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "rounding_minutes")
    private int roundingMinutes;

    @NotNull
    @Column(name = "rounding_mode")
    private String roundingMode = "ROUNDING_TO_NEAREST_NTH_MINUTE";

    @NotNull
    @Column(name = "grace_in")
    private int graceIn;

    @NotNull
    @Column(name = "grace_out")
    private int graceOut;

    @Column(name = "description")
    private String description;

    @Column(name = "project_id", columnDefinition = "BIGINT")
    private Long projectId;

    @Column(name = "rig_pay_threshold")
    private int rigPayThreshold;

    @Column(name = "week_end_day")
    private DayOfWeek weekEndDay = DayOfWeek.SUNDAY;

    @Column(name = "equipment_workday_hour")
    private BigDecimal equipmentWorkdayHour = BigDecimal.valueOf(8);

    @Column(name = "equipment_exclude_weekend")
    private boolean equipmentExcludeWeekend = true;

    @Column(name = "equipment_cost_code")
    private String equipmentCostCode;

    @Column(name = "st_cost_code")
    private String stCostCode;

    @Column(name = "ot_cost_code")
    private String otCostCode;

    @Column(name = "dt_cost_code")
    private String dtCostCode;

    @Column(name = "sick_cost_code")
    private String sickCostCode;

    @Column(name = "holiday_cost_code")
    private String holidayCostCode;

    @Column(name = "vacation_cost_code")
    private String vacationCostCode;

    @Column(name = "other_cost_code")
    private String otherCostCode;

    @Column(name = "day_off_threshold")
    private int dayOffThreshold = 13;

    @Column(name = "per_diem_id")
    private Long perDiemId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "per_diem_id", referencedColumnName = "id", insertable = false, updatable = false)
    private PerDiem perDiem;

    @Column(name = "mob_mileage_rate")
    private BigDecimal mobMileageRate = BigDecimal.ZERO;

//    @Column(name = "per_diem_by_labor_cost_code")
//    private boolean perDiemByLaborCostCode = false;

    @NotNull
    @Column(name = "taxable_per_diem")
    private boolean taxablePerDiem = false;

    @NotNull
    @Column(name = "hide_dt")
    private boolean hideDt;

    public LocalDate getStartOfWeek(LocalDate dateOfService) {
        LocalDate startOfWeek = dateOfService.with(weekEndDay.plus(1));
        if (startOfWeek.isAfter(dateOfService)) startOfWeek = startOfWeek.minusWeeks(1);
        return startOfWeek;
    }

    public LocalDate getEndOfWeek(LocalDate dateOfService) {
        LocalDate endOfWeek = dateOfService.with(weekEndDay);
        if (endOfWeek.isBefore(dateOfService)) endOfWeek = endOfWeek.plusWeeks(1);
        return endOfWeek;
    }

    public LocalDate getPreviousEndOfWeek(LocalDate dateOfService) {
        LocalDate endOfWeek = dateOfService.with(weekEndDay);
        if (endOfWeek.isAfter(dateOfService)) endOfWeek = endOfWeek.minusWeeks(1);
        return endOfWeek;
    }
}
