package com.timaven.provider.model.paging;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public class MPageRequest extends PageRequest {

    private long offset = -1;

    protected MPageRequest(int page, int size, Sort sort) {
        super(page, size, sort);
    }

    public Pageable setOffset(long offset) {
        this.offset = offset;
        return this;
    }

    @Override
    public long getOffset() {
        return offset == -1 ? super.getOffset() : offset;
    }

    public static MPageRequest of(int page, int size) {
        return of(page, size, Sort.unsorted());
    }

    public static MPageRequest of(int page, int size, Sort sort) {
        return new MPageRequest(page, size, sort);
    }

    public static PageRequest of(int page, int size, Sort.Direction direction, String... properties) {
        return of(page, size, Sort.by(direction, properties));
    }

}
