package com.timaven.provider.model.paging;

public enum Direction {
    asc,
    desc;
}
