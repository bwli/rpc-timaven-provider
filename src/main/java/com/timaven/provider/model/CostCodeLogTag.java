package com.timaven.provider.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Comparator;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = CostCodeLogTag.class)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "cclg_tag", schema = "project")
public class CostCodeLogTag implements Comparable<CostCodeLogTag> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "cost_code_full", unique = true)
    private String costCodeFull;

    @Column(name = "tag")
    private String tag;

    @Column(name = "project_id")
    private Long projectId;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(costCodeFull);
        hcb.append(projectId);
        hcb.append(tag);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CostCodeLogTag)) return false;
        CostCodeLogTag that = (CostCodeLogTag) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(costCodeFull, that.costCodeFull);
        eb.append(projectId, that.projectId);
        eb.append(tag, that.tag);
        return eb.isEquals();
    }

    @Override
    public int compareTo(CostCodeLogTag o) {
        return Comparator.comparing(CostCodeLogTag::getTag).compare(this, o);
    }
}
