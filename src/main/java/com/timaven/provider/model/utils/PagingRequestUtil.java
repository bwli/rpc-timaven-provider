package com.timaven.provider.model.utils;

import com.timaven.provider.model.paging.Order;
import com.timaven.provider.model.paging.Column;
import com.timaven.provider.model.paging.MPageRequest;
import com.timaven.provider.model.paging.PagingRequest;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.criteria.AuditCriterion;
import org.hibernate.envers.query.criteria.MatchMode;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class PagingRequestUtil {

    public static Pageable Paging2Pageable(PagingRequest page, Map<String,String> columnMap) {
       return Paging2Pageable(page,columnMap,false);
    }

    public static Pageable Paging2Pageable(PagingRequest page,Map<String,String> columnMap,boolean unsorted) {
        Pageable pageable = null;
        int pageNumber = 0;
        int pageSize = page.getLength();
        int offset = page.getStart();

        if (pageSize == -1) {
            return null;
        }
        List<Order> orders = page.getOrder();
        List<Column> columns = page.getColumns();
        if (orders != null && orders.size() > 0 && columns != null
                && columns.size() > 0 && columns.size() >= orders.size() && !unsorted) {
            List<Sort.Order> orderList = new ArrayList<>();
            for (Order value : orders) {
                int columnIndex = value.getColumn();
                Column column = columns.get(columnIndex);
                String colomnName = getColumnName(column.getData(), columnMap);
                if (!StringUtils.hasText(colomnName) || !column.getOrderable()) {
                   continue;
                }
                if (colomnName.contains(",")){
                    String[] colomnNames = colomnName.split(",");
                    for (String name:colomnNames) {
                        orderList.add(new Sort.Order(Sort.Direction.valueOf(value.getDir().name().toUpperCase()),
                                name));
                    }
                }else{
                    orderList.add(new Sort.Order(Sort.Direction.valueOf(value.getDir().name().toUpperCase()),
                            colomnName));
                }
            }
            Sort sort;
            if (orderList.size() == 0){
                sort = Sort.unsorted();
            }else{
                sort = Sort.by(orderList);
            }
            pageable = MPageRequest.of(pageNumber , pageSize, sort).setOffset(offset);
        } else {
            pageable = MPageRequest.of(pageNumber, pageSize, Sort.unsorted()).setOffset(offset);
        }
        return pageable;
    }

    public static Sort Paging2Sort(PagingRequest page, Map<String,String> columnMap) {
        Sort sort = null;
        List<Order> orders = page.getOrder();
        List<Column> columns = page.getColumns();
        if (orders != null && orders.size() > 0 && columns != null
                && columns.size() > 0 && columns.size() >= orders.size()) {
            List<Sort.Order> orderList = new ArrayList<>();
            for (Order value : orders) {
                int columnIndex = value.getColumn();
                Column column = columns.get(columnIndex);
                String colomnName = getColumnName(column.getData(), columnMap);
                if (!StringUtils.hasText(colomnName) || !column.getOrderable()) {
                    continue;
                }
                if (colomnName.contains(",")){
                    String[] colomnNames = colomnName.split(",");
                    for (String name:colomnNames) {
                        orderList.add(new Sort.Order(Sort.Direction.valueOf(value.getDir().name().toUpperCase()),
                                name).nullsLast());
                    }
                }else{
                    orderList.add(new Sort.Order(Sort.Direction.valueOf(value.getDir().name().toUpperCase()),
                            colomnName).nullsLast());
                }
            }
            if (orderList.size() == 0){
                sort = Sort.unsorted();
            }else{
                sort = Sort.by(orderList);
            }
        }else{
            sort = Sort.unsorted();
        }
        return sort;
    }

    public static Predicate getPredicate(Root<?> root, CriteriaQuery<?> criteriaQuery,
                                                        CriteriaBuilder criteriaBuilder, PagingRequest pagingRequest,
                                                        Map<String, String> columnMap, List<Predicate> andPredicates){
        List<Column> columns = pagingRequest.getColumns();
        List<Predicate> orPredicates = new ArrayList<>();

        if (columns != null) {
            for (Column column : columns) {
                if (column.getSearchable() && StringUtils.hasText(column.getSearch().getValue())) {
                    try {
                        String columnName = getColumnName(column.getData(), columnMap);
                        if (!StringUtils.hasText(columnName)) {
                            continue;
                        }
                        if (columnName.contains(",")){
                            String[] colomnNames = columnName.split(",");
                            String[] values = column.getSearch().getValue().toLowerCase().trim().split("\\s+");
                            if (values.length > 1){
                                List<Predicate> curPredicate = new ArrayList<>();
                                for (int i = 0; i < colomnNames.length && i < values.length; i++) {
                                    Path<Object> columnNamePath = root.get(colomnNames[i]);
                                    Predicate predicate = criteriaBuilder.like(criteriaBuilder.lower(columnNamePath.as(String.class)),
                                            "%" + formatIfDate(values[i]) + "%");
                                    curPredicate.add(predicate);
                                }
                                orPredicates.add(criteriaBuilder.and(curPredicate.toArray(new Predicate[0])));
                            }else{
                                for (String name:colomnNames) {
                                    Path<Object> columnNamePath = root.get(name);
                                    Predicate predicate = criteriaBuilder.like(criteriaBuilder.lower(columnNamePath.as(String.class)),
                                            "%" + formatIfDate(column.getSearch().getValue().toLowerCase()) + "%");
                                    orPredicates.add(predicate);
                                }
                            }
                        }else{
                            Path<Object> columnNamePath;
                            if (columnName.contains(".")) {
                                String[] columnNames = columnName.split("\\.");
                                columnNamePath = root.get(columnNames[0]);
                                //noinspection rawtypes
                                Join join = root.join(columnNames[0], JoinType.LEFT);
                                for (int i = 1; i < columnNames.length; i++) {
                                    if (i < columnNames.length - 1) {
                                        join = join.join(columnNames[i]);
                                    }
                                    columnNamePath = columnNamePath.get(columnNames[i]);
                                }
                            } else {
                                columnNamePath = root.get(columnName);
                            }
                            Predicate predicate = criteriaBuilder.like(criteriaBuilder.lower(columnNamePath.as(String.class)),
                                    "%" + formatIfDate(column.getSearch().getValue().toLowerCase()) + "%");
                            andPredicates.add(predicate);
                        }
                    }catch (IllegalArgumentException e){
                        e.printStackTrace();
                    }
                }
            }

            if (pagingRequest.getSearch()!= null && !StringUtils.isEmpty(pagingRequest.getSearch().getValue())){
                String searchString = pagingRequest.getSearch().getValue().trim();
                for (Column column : columns) {
                    if (column.getSearchable() && StringUtils.hasText(searchString)) {
                        try {
                            String columnName = getColumnName(column.getData(), columnMap);
                            if (!StringUtils.hasText(columnName)) {
                                continue;
                            }
                            if (columnName.contains(",")){
                                String[] columnNames = columnName.split(",");
                                String[] values = searchString.toLowerCase().trim().split("\\s+");
                                if (values.length > 1){
                                    List<Predicate> curPredicate = new ArrayList<>();
                                    for (int i = 0; i < columnNames.length && i < values.length; i++) {
                                        Path<Object> columnNamePath = root.get(columnNames[i]);
                                        Predicate predicate = criteriaBuilder.like(criteriaBuilder.lower(columnNamePath.as(String.class)),
                                                "%" + formatIfDate(values[i]) + "%");
                                        curPredicate.add(predicate);
                                    }
                                    orPredicates.add(criteriaBuilder.and(curPredicate.toArray(new Predicate[0])));
                                }else{
                                    for (String name:columnNames) {
                                        Path<Object> columnNamePath = root.get(name);
                                        Predicate predicate = criteriaBuilder.like(criteriaBuilder.lower(columnNamePath.as(String.class)),
                                                "%" + formatIfDate(searchString.toLowerCase()) + "%");
                                        orPredicates.add(predicate);
                                    }
                                }
                            }else{
                                Path<Object> columnNamePath;
                                if (columnName.contains(".")) {
                                    String[] columnNames = columnName.split("\\.");
                                    columnNamePath = root.get(columnNames[0]);
                                    //noinspection rawtypes
                                    Join join = root.join(columnNames[0], JoinType.LEFT);
                                    for (int i = 1; i < columnNames.length; i++) {
                                        if (i < columnNames.length - 1) {
                                            join = join.join(columnNames[i]);
                                        }
                                    }
                                    //noinspection unchecked
                                    columnNamePath = join.get(columnNames[columnNames.length - 1]);
                                } else {
                                    columnNamePath = root.get(columnName);
                                }
                                Predicate predicate = criteriaBuilder.like(criteriaBuilder.lower(columnNamePath.as(String.class)),
                                        "%" + formatIfDate(searchString.toLowerCase()) + "%");
                                orPredicates.add(predicate);
                            }
                        }catch (IllegalArgumentException e){
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        if (andPredicates.size() > 0 && orPredicates.size() > 0) {
            Predicate and = criteriaBuilder.and(andPredicates.toArray(new Predicate[0]));
            Predicate or = criteriaBuilder.or(orPredicates.toArray(new Predicate[0]));
            return criteriaBuilder.and(and,or);
        }
        if (andPredicates.size() > 0) {
            return criteriaBuilder.and( criteriaBuilder.and(andPredicates.toArray(new Predicate[0])));
        }
        if (orPredicates.size() > 0) {
            return criteriaBuilder.or(criteriaBuilder.or(orPredicates.toArray(new Predicate[0])));
        }
        return null;
    }

    public static AuditCriterion getAuditCriterion(PagingRequest pagingRequest, Map<String, String> columnMap,
                                                   Map<String, String> columnTypeMap) {
        List<Column> columns = pagingRequest.getColumns();

        List<AuditCriterion> andCriterions = new ArrayList<>();
        List<AuditCriterion> orCriterions = new ArrayList<>();

        if (columns != null) {
            for (Column column : columns) {
                if (column.getSearchable() && StringUtils.hasText(column.getSearch().getValue())) {
                    try {
                        String columnName = getColumnName(column.getData(), columnMap);
                        String columnType = getColumnType(column.getData(), columnTypeMap);
                        if (!StringUtils.hasText(columnName)) {
                            continue;
                        }
                        if (columnName.contains(",")) {
                            String[] columnNameArr = columnName.split(",");
                            String[] values = column.getSearch().getValue().toLowerCase().trim().split("\\s+");
                            if (values.length > 1) {
                                AuditCriterion criterion = null;
                                for (int i = 0; i < columnNameArr.length && i < values.length; i++) {
                                    AuditCriterion curCriterion = getAuditCriterion(columnNameArr[i], values[i],
                                            columnType);
                                    if (curCriterion == null) continue;
                                    if (criterion == null) {
                                        criterion = curCriterion;
                                    } else {
                                        criterion = AuditEntity.or(criterion, curCriterion);
                                    }
                                }
                                if (criterion != null) {
                                    orCriterions.add(criterion);
                                }
                            } else if (values.length == 1) {
                                for (String name : columnNameArr) {
                                    AuditCriterion curCriterion = getAuditCriterion(name, values[0], columnType);
                                    if (curCriterion == null) continue;
                                    andCriterions.add(curCriterion);
                                }
                            }
                        } else {
                            AuditCriterion curCriterion = getAuditCriterion(columnName,
                                    column.getSearch().getValue().toLowerCase(), columnType);
                            if (curCriterion != null) {
                                andCriterions.add(curCriterion);
                            }
                        }
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    }
                }
            }

            String searchValue = pagingRequest.getSearch() != null ? pagingRequest.getSearch().getValue() : null;
            if (searchValue != null) {
                for (Column column : columns) {
                    if (column.getSearchable() && StringUtils.hasText(searchValue)) {
                        try {
                            String columnName = getColumnName(column.getData(), columnMap);
                            String columnType = getColumnType(column.getData(), columnTypeMap);
                            if (!StringUtils.hasText(columnName)) {
                                continue;
                            }
                            if (columnName.contains(",")) {
                                String[] columnNameArr = columnName.split(",");
                                AuditCriterion criterion = null;
                                for (int i = 0; i < columnNameArr.length; i++) {
                                    AuditCriterion curCriterion = getAuditCriterion(columnNameArr[i], searchValue,
                                            columnType);
                                    if (curCriterion == null) continue;
                                    if (criterion == null) {
                                        criterion = curCriterion;
                                    } else {
                                        criterion = AuditEntity.or(criterion, curCriterion);
                                    }
                                }
                                if (null != criterion) {
                                    orCriterions.add(criterion);
                                }
                            } else {
                                AuditCriterion curCriterion = getAuditCriterion(columnName, searchValue, columnType);
                                if (null != curCriterion) {
                                    orCriterions.add(curCriterion);
                                }
                            }
                        } catch (IllegalArgumentException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        if (andCriterions.size() > 0 && orCriterions.size() > 0) {
            AuditCriterion auditCriterion = null;
            for (AuditCriterion criterion : andCriterions) {
                if (auditCriterion == null) {
                    auditCriterion = criterion;
                } else {
                    auditCriterion = AuditEntity.and(auditCriterion, criterion);
                }
            }
            for (AuditCriterion criterion : orCriterions) {
                auditCriterion = AuditEntity.or(auditCriterion, criterion);
            }

            return auditCriterion;
        }
        if (andCriterions.size() > 0) {
            AuditCriterion auditCriterion = null;
            for (AuditCriterion criterion : andCriterions) {
                if (auditCriterion == null) {
                    auditCriterion = criterion;
                } else {
                    auditCriterion = AuditEntity.and(auditCriterion, criterion);
                }
            }
            return auditCriterion;
        }
        if (orCriterions.size() > 0) {
            AuditCriterion auditCriterion = null;
            for (AuditCriterion criterion : orCriterions) {
                if (auditCriterion == null) {
                    auditCriterion = criterion;
                } else {
                    auditCriterion = AuditEntity.or(auditCriterion, criterion);
                }
            }
            return auditCriterion;
        }
        return null;
    }

    private static AuditCriterion getAuditCriterion(String propertyName, String value, String columnType) {
        if ("String".equalsIgnoreCase(columnType)) {
            // ilike case-insensitive search
            return AuditEntity.property(propertyName).ilike(formatIfDate(value), MatchMode.ANYWHERE);
        } else {
            switch (columnType) {
                case "Integer":
                    try {
                        return AuditEntity.property(propertyName).eq(Integer.valueOf(value));
                    } catch (NumberFormatException ignore) {
                    }
                    break;
                case "LocalDate":
                    LocalDate localDate = getLocalDate(value);
                    if (null != localDate) {
                        return AuditEntity.property(propertyName).eq(localDate);
                    }
                    break;
                case "Boolean":
                    Boolean bValue = null;
                    if ("true".equalsIgnoreCase(value)) {
                        bValue = true;
                    }
                    if ("false".equalsIgnoreCase(value)) {
                        bValue = false;
                    }
                    if (null != bValue) {
                        return AuditEntity.property(propertyName).eq(bValue);
                    }
                    break;
                default:
                    throw new RuntimeException("Not implemented yet");
            }
        }
        return null;
    }

    public static <T> Specification<T> getSpecification(PagingRequest pagingRequest, Map<String,String> columnMap){
        return (root, criteriaQuery, criteriaBuilder) -> getPredicate(root, criteriaQuery, criteriaBuilder, pagingRequest, columnMap,
                new ArrayList<>());
    }

    private static String getColumnName(String data, Map<String, String> columnMap) {
        return columnMap != null && columnMap.get(data)!= null ? columnMap.get(data) : data;
    }

    private static String getColumnType(String data, Map<String, String> columnTypeMap) {
        return columnTypeMap != null && columnTypeMap.get(data)!= null ? columnTypeMap.get(data) : "String";
    }

    private static String formatIfDate(String s){
        if (s == null || !s.contains("/")){
            return s;
        }
        try {
            SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
            SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
            Date date = format.parse(s);
            return format2.format(date);
        } catch (ParseException e) {
            try {
                SimpleDateFormat format = new SimpleDateFormat("MM/dd");
                SimpleDateFormat format2 = new SimpleDateFormat("MM-dd");
                Date date = format.parse(s);
                return format2.format(date);
            } catch (ParseException ignored) {
                return s;
            }
        }
    }

    private static LocalDate getLocalDate(String text) {
        try {
            if (StringUtils.isEmpty(text)) return null;
            return LocalDate.parse(text, DateTimeFormatter.ISO_LOCAL_DATE);
        } catch (DateTimeParseException ex) {
            try {
                return LocalDate.parse(text, DateTimeFormatter.ofPattern("MM/dd/yyyy"));
            } catch (DateTimeParseException ignore) {
            }
        }
        return null;
    }

}
