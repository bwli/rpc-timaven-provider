package com.timaven.provider.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.timaven.provider.model.enums.EquipmentHourlyType;
import com.timaven.provider.model.enums.EquipmentOwnershipType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Equipment.class)
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "equipment", schema = "project")
public class Equipment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "equipment_id")
    private String equipmentId;

    @Column(name = "description")
    private String description;

    @Column(name = "alias")
    private String alias;

    @Column(name = "serial_number")
    private String serialNumber;

    @Column(name = "department")
    private String department;

    @NotNull
    @Column(name = "class")
    private String equipmentClass;

    @Column(name = "hourly_type")
    private EquipmentHourlyType hourlyType = EquipmentHourlyType.DAILY;

    @Column(name = "ownership_type")
    private EquipmentOwnershipType ownershipType = EquipmentOwnershipType.COMPANY_OWNED;

    @Column(name = "emp_id")
    private String empId;

    @Column(name = "po")
    private String purchaseOrder;

    @NotNull
    @Column(name = "is_active")
    private Boolean active = true;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "equipment")
    Set<EquipmentUsage> equipmentUsages;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(equipmentId);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Equipment)) return false;
        Equipment that = (Equipment) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(equipmentId, that.equipmentId);
        return eb.isEquals();
    }
}
