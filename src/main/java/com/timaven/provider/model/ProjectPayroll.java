package com.timaven.provider.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "project_payroll", schema = "time")
public class ProjectPayroll {

    @EmbeddedId
    private ProjectPayrollPK id;

    @Column(name = "project_id", insertable = false, updatable = false)
    private Long projectId;

    @Column(name = "week_end_date", insertable = false, updatable = false)
    private LocalDate weekEndDate;

    @Column(name = "employee_count")
    private Long employeeCount;

    @Column(name = "total_hours")
    private BigDecimal totalHours;

    @Column(name = "timesheets")
    private Long timesheets;

    @Column(name = "approved_at")
    private LocalDate approvedAt;

    @Column(name = "finalized_at")
    private LocalDate finalizedAt;

    @Column(name = "type", insertable = false, updatable = false)
    private int type;

    @Override
    public int hashCode() {
        if (null != id) {
            HashCodeBuilder hcb = new HashCodeBuilder();
            hcb.append(id);
            return hcb.toHashCode();
        }
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProjectPayroll)) return false;
        ProjectPayroll that = (ProjectPayroll) o;
        if (null != id) {
            EqualsBuilder eb = new EqualsBuilder();
            eb.append(id, that.id);
            return eb.isEquals();
        }
        return super.equals(o);
    }
}
