package com.timaven.provider.model.converter;

import com.timaven.provider.model.StageSubmission.Type;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;


@Converter(autoApply = true)
public class StageSubmissionTypeConverter implements AttributeConverter<Type, Integer> {

    @Override
    public Integer convertToDatabaseColumn(Type status) {
        return status == null ? null : status.ordinal();
    }

    @Override
    public Type convertToEntityAttribute(Integer dbData) {
        return null != dbData ? Type.values()[dbData] : Type.Unknown;
    }
}
