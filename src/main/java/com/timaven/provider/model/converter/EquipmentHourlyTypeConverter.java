package com.timaven.provider.model.converter;


import com.timaven.provider.model.enums.EquipmentHourlyType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;


@Converter(autoApply = true)
public class EquipmentHourlyTypeConverter implements AttributeConverter<EquipmentHourlyType, Integer> {

    @Override
    public Integer convertToDatabaseColumn(EquipmentHourlyType type) {
        return type == null ? 0 : type.ordinal();
    }

    @Override
    public EquipmentHourlyType convertToEntityAttribute(Integer dbData) {
        return null != dbData ? EquipmentHourlyType.values()[dbData] : EquipmentHourlyType.DAILY;
    }
}
