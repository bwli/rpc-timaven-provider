package com.timaven.provider.model.converter;


import com.timaven.provider.model.enums.PerDiemMode;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;


@Converter(autoApply = true)
public class PerDiemModeConverter implements AttributeConverter<PerDiemMode, Integer> {

    @Override
    public Integer convertToDatabaseColumn(PerDiemMode status) {
        return status == null ? null : status.ordinal();
    }

    @Override
    public PerDiemMode convertToEntityAttribute(Integer dbData) {
        return null != dbData ? PerDiemMode.values()[dbData] : PerDiemMode.PAY_AS_YOU_GO;
    }
}
