package com.timaven.provider.model.converter;

import com.timaven.provider.model.enums.WeeklyProcessType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

// Used by AllocationSubmission Status mapping
@Converter(autoApply = true)
public class WeeklyProcessTypeConverter implements AttributeConverter<WeeklyProcessType, Integer> {

    @Override
    public Integer convertToDatabaseColumn(WeeklyProcessType type) {
        return type == null ? null : type.getValue();
    }

    @Override
    public WeeklyProcessType convertToEntityAttribute(Integer dbData) {
        return null != dbData ? WeeklyProcessType.values()[dbData] : null;
    }
}
