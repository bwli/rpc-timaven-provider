package com.timaven.provider.model.converter;

import com.timaven.provider.model.enums.CostCodeType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

// Used by AllocationSubmission Status mapping
@Converter(autoApply = true)
public class CostCodeTypeConverter implements AttributeConverter<CostCodeType, Integer> {

    @Override
    public Integer convertToDatabaseColumn(CostCodeType type) {
        return type == null ? CostCodeType.DEFAULT.getValue() : type.getValue();
    }

    @Override
    public CostCodeType convertToEntityAttribute(Integer dbData) {
        return null != dbData ? CostCodeType.values()[dbData] : CostCodeType.DEFAULT;
    }
}
