package com.timaven.provider.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "stage_record", schema = "time")
public class StageRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "stage_trans_id", referencedColumnName = "id", columnDefinition = "BIGINT")
    @JsonBackReference
    private StageTransaction stageTransaction;

    @Column(name = "door_name")
    private String doorName;

    @Column(name = "side")
    private Boolean side;

    @Column(name = "access_granted")
    private boolean accessGranted = false;

    @Column(name = "valid")
    private boolean valid = false;

    @Column(name = "adjusted_time")
    private LocalDateTime adjustedTime;

    @Column(name = "net_event_time")
    private LocalDateTime netEventTime;

    @Override
    public int hashCode() {
        if (null != id) {
            HashCodeBuilder hcb = new HashCodeBuilder();
            hcb.append(id);
            return hcb.toHashCode();
        }
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StageRecord)) return false;
        StageRecord that = (StageRecord) o;
        if (null != id) {
            EqualsBuilder eb = new EqualsBuilder();
            eb.append(id, that.id);
            return eb.isEquals();
        }
        return super.equals(o);
    }
}
