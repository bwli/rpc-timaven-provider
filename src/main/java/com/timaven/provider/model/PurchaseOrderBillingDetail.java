package com.timaven.provider.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.timaven.provider.model.dto.PurchaseOrderBillingDetailDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = PurchaseOrderBillingDetail.class)
@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "purchase_order_billing_detail", schema = "project")
public class PurchaseOrderBillingDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "purchase_order_number")
    private String purchaseOrderNumber;

    @Column(name = "part")
    private String part;

    @Column(name = "line_number")
    private Integer lineNumber;

    @Column(name = "unit_cost")
    private BigDecimal unitCost;

    @Column(name = "billing_quantity")
    private BigDecimal billingQuantity;

    @Column(name = "markup_quantity")
    private BigDecimal markupQuantity;

    @Column(name = "markup_amount")
    private BigDecimal markupAmount;

    @Column(name = "billing_id", insertable = false, updatable = false)
    private Long billingId;

    @Column(name = "tax")
    private BigDecimal tax;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "billing_id", referencedColumnName = "id", columnDefinition = "BIGINT")
    private PurchaseOrderBilling purchaseOrderBilling;

    public PurchaseOrderBillingDetail(PurchaseOrderBillingDetailDto dto) {
        this.purchaseOrderNumber = dto.getPurchaseOrderNumber();
        this.lineNumber = dto.getLineNumber();
        this.billingQuantity = dto.getBillingQuantity();
        this.markupQuantity = dto.getMarkupQuantity();
        this.markupAmount = dto.getMarkupAmount();
        this.tax = dto.getTax();
        this.unitCost = dto.getUnitCost();
        this.part = dto.getPart();
    }
}
