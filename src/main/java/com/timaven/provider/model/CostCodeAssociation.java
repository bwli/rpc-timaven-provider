package com.timaven.provider.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "cost_code_association", schema = "project")
public class CostCodeAssociation {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "start_date")
    LocalDate startDate;

    @Column(name = "end_date")
    LocalDate endDate;

    @Column(name = "cost_code")
    private String costCode;

    @Column(name = "cost_code_full")
    private String costCodeFull;

    @Column(name = "tags")
    private String tags;

    @Column(name = "description")
    private String description;

    @Column(name = "job_number")
    private String jobNumber;

    @Column(name = "sub_job")
    private String subJob;

    @Column(name = "project_id")
    private Long projectId;

    @Column(name = "level1")
    private String level1;

    @Column(name = "level2")
    private String level2;

    @Column(name = "level3")
    private String level3;

    @Column(name = "level4")
    private String level4;

    @Column(name = "level5")
    private String level5;

    @Column(name = "level6")
    private String level6;

    @Column(name = "level7")
    private String level7;

    @Column(name = "level8")
    private String level8;

    @Column(name = "level9")
    private String level9;

    @Column(name = "level10")
    private String level10;

    @Column(name = "level11")
    private String level11;

    @Column(name = "level12")
    private String level12;

    @Column(name = "level13")
    private String level13;

    @Column(name = "level14")
    private String level14;

    @Column(name = "level15")
    private String level15;

    @Column(name = "level16")
    private String level16;

    @Column(name = "level17")
    private String level17;

    @Column(name = "level18")
    private String level18;

    @Column(name = "level19")
    private String level19;

    @Column(name = "level20")
    private String level20;

    /**
     * TM 389
     * */
    @Column(name = "cost_code_segment")
    private String costCodeSegment;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(id);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CostCodeAssociation)) return false;
        CostCodeAssociation that = (CostCodeAssociation) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, that.id);
        return eb.isEquals();
    }
}
