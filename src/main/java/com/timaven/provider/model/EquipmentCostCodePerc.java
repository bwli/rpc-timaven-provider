package com.timaven.provider.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.timaven.provider.model.dto.EquipmentProcessCostCodePercDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = EquipmentCostCodePerc.class)
@Audited
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "equipment_cost_code_perc", schema = "time")
public class EquipmentCostCodePerc {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "cost_code_full")
    private String costCodeFull;

    @Column(name = "total_hour")
    private BigDecimal totalHour;

    @Column(name = "total_charge")
    private BigDecimal totalCharge;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "allocate_time_id", referencedColumnName = "id", columnDefinition = "BIGINT")
    private EquipmentAllocationTime equipmentAllocationTime;

    @Column(name = "allocate_time_id", insertable = false, updatable = false)
    private Long allocationTimeId;

    @Column(name = "created_at", insertable = false, updatable = false)
    private LocalDateTime createdAt;

    public EquipmentCostCodePerc(EquipmentProcessCostCodePercDto dto) {
        this.costCodeFull = dto.getCostCodeFull();
        this.totalHour = dto.getTotalHour();
    }

    @Override
    public int hashCode() {
        if (null != id) {
            HashCodeBuilder hcb = new HashCodeBuilder();
            hcb.append(id);
            return hcb.toHashCode();
        }
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EquipmentCostCodePerc)) return false;
        if (null != id) {
            EquipmentCostCodePerc that = (EquipmentCostCodePerc) o;
            EqualsBuilder eb = new EqualsBuilder();
            eb.append(id, that.id);
            return eb.isEquals();
        }
        return super.equals(o);
    }
}
