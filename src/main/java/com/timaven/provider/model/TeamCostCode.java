package com.timaven.provider.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Audited
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = {"projectId", "teamName", "costCode", "startDate", "endDate"})
@Entity
@Table(name = "team_cclg", schema = "project")
public class TeamCostCode implements Comparable<TeamCostCode> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "project_id")
    private Long projectId;

    @Column(name = "team_name")
    private String teamName;

    @Column(name = "cost_code")
    private String costCode;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "end_date")
    private LocalDate endDate;

    @Column(name = "created_at", insertable = false, updatable = false)
    private LocalDateTime createdAt;

    public int customHashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(projectId);
        hcb.append(teamName);
        hcb.append(costCode);
        return hcb.toHashCode();
    }

    @Override
    public int compareTo(TeamCostCode that) {
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(projectId, that.projectId);
        eb.append(teamName, that.teamName);
        eb.append(costCode, that.costCode);
        return eb.isEquals() ? 0 : -1;
    }
}
