package com.timaven.provider.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.timaven.provider.model.enums.EquipmentAllocationSubmissionStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = EquipmentAllocationSubmission.class)
@Audited
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "equipment_allocation_submission", schema = "time")
public class EquipmentAllocationSubmission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "approve_user_id", columnDefinition = "BIGINT")
    private Long approveUserId;
    @Column(name = "approved_at")
    private LocalDateTime approvedAt;

    @Column(name = "status")
    private EquipmentAllocationSubmissionStatus status;

    @Column(name = "date_of_service")
    private LocalDate dateOfService;

    @NotNull
    @Column(name = "project_id")
    private Long projectId;

    @Column(name = "created_at", insertable = false, updatable = false)
    private LocalDateTime createdAt;

    @NotAudited
    @OneToMany(mappedBy = "equipmentAllocationSubmission", fetch = FetchType.LAZY, cascade =
            {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
    @OrderBy(value = "equipmentId asc")
    private Set<EquipmentAllocationTime> equipmentAllocationTimes;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(projectId);
        hcb.append(dateOfService);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EquipmentAllocationSubmission)) return false;
        EquipmentAllocationSubmission that = (EquipmentAllocationSubmission) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(projectId, that.projectId);
        eb.append(dateOfService, that.dateOfService);
        return eb.isEquals();
    }

}
