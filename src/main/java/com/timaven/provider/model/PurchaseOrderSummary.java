package com.timaven.provider.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

@Getter
@Setter
@Entity
@Table(name = "purchase_order_summary", schema = "project")
public class PurchaseOrderSummary {
    @Id
    @Column(name = "purchase_order_number")
    private String purchaseOrderNumber;
    @Column(name = "purchase_order_date")
    private LocalDate purchaseOrderDate;
    @Column(name = "vendor_name")
    private String vendorName;
    @Column(name = "requestor")
    private String requestor;
    @Column(name = "job_number")
    private String jobNumber;
    @Column(name = "sub_job")
    private String subJob;
    @Column(name = "submission_id", updatable = false, insertable = false)
    private Long submissionId;
    @Column(name = "total_extended_cost")
    private BigDecimal totalExtendedCost;
    @Column(name = "quantity")
    private BigDecimal quantity;
    @Column(name = "quantity_billed")
    private BigDecimal billedQuantity;
    @Column(name = "amount_billed")
    private BigDecimal billedAmount;
    @Column(name = "billed_on")
    private LocalDate billedOn;
    @Column(name = "comment")
    private String comment;
    @Column(name = "flag")
    private boolean flag;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PurchaseOrderSummary that = (PurchaseOrderSummary) o;
        if (purchaseOrderNumber != null && that.purchaseOrderNumber != null)
            return purchaseOrderNumber.equals(that.getPurchaseOrderNumber());
        return false;
    }

    @Override
    public int hashCode() {
        if (purchaseOrderNumber != null) return Objects.hash(purchaseOrderNumber);
        return super.hashCode();
    }
}
