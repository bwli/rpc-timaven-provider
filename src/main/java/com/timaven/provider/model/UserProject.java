package com.timaven.provider.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;


@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "user_project", schema = "pm")
public class UserProject implements Comparable<UserProject> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "user_id", columnDefinition = "BIGINT")
    private Long userId;

    @ManyToOne
    @JoinColumn(name = "user_id", columnDefinition = "BIGINT", insertable = false, updatable = false)
    @JsonBackReference("userProjects")
    private User user;

    @Column(name = "project_id", columnDefinition = "BIGINT")
    private Long projectId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id", referencedColumnName = "id", columnDefinition = "BIGINT", insertable = false,
            updatable = false)
    @JsonBackReference("project")
    private Project project;

    public UserProject(Long userId, Long projectId) {
        this.userId = userId;
        this.projectId = projectId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserProject that = (UserProject) o;
        if (id != null && that.id != null) {
            return Objects.equals(id, that.id);
        } else if (userId != null && that.userId != null
                && projectId != null && that.projectId != null) {
            return Objects.equals(userId, that.userId) &&
                    Objects.equals(projectId, that.projectId);
        }
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        if (id != null) return Objects.hash(id);
        if (userId != null && projectId != null) return Objects.hash(id, userId, projectId);
        return super.hashCode();
    }

    @Override
    public int compareTo(UserProject userProject) {
        int result = projectId.compareTo(userProject.projectId);
        if (result == 0) {
            result = userId.compareTo(userProject.userId);
        }
        return result;
    }
}
