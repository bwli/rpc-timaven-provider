create table if not exists project.job
(
    id         bigserial primary key,
    job_id     bigint not null,
    address    varchar,
    is_ls      boolean,
    project_id bigint not null,
    created_at timestamp with time zone default now(),
    unique (job_id, project_id)
);

DO
$$
    BEGIN
        if exists(select *
                  from information_schema.columns
                  where table_name = 'job'
                    and table_schema = 'project'
                    and column_name = 'is_ls')
        then comment on column project.job.is_ls
            is 'ls stands for lump sum. tm stands for time and material';
        end if;
    END ;
$$;

