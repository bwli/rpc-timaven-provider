create table if not exists time.allocation_time
(
    id                 bigserial primary key,
    emp_id             varchar,
    client_emp_id      varchar,
    badge              varchar,
    team_name          varchar not null,
    first_name         varchar,
    last_name          varchar,
    department         varchar,
    job_number         varchar,
    st_hour            numeric,
    ot_hour            numeric,
    dt_hour            numeric,
    extra_time_type    integer                  default 0,
    total_hour         numeric,
    allocated_hour     numeric,
    net_hour           numeric,
    has_per_diem       bool    not null         default false,
    rig_pay            numeric,
    payroll_date       date,
    max_time_cost_code varchar,
    per_diem_cost_code varchar,
    mob_cost_code      varchar,
    per_diem_amount    numeric not null         default 0,
    mob_amount         numeric not null         default 0,
    submission_id      bigint  not null references time.allocation_submission (id) on update cascade on delete cascade,
    created_at         timestamp with time zone default now()
);

DO
$$
    BEGIN
        if not exists(select *
                      from information_schema.columns
                      where table_name = 'allocation_time'
                        and table_schema = 'time'
                        and column_name = 'per_diem_amount')
        then
            alter table time.allocation_time
                add column per_diem_amount numeric not null default 0;
            with t1 as (
                with t0 as (
                    select t.id, t.emp_id, t.has_per_diem, s.date_of_service, s.project_id
                    from time.allocation_time t
                             inner join time.allocation_submission s on t.submission_id = s.id
                )
                select distinct on (t0.id, t0.emp_id, t0.has_per_diem, t0.date_of_service, t0.project_id) t0.id,
                                                                                                          t0.emp_id,
                                                                                                          t0.has_per_diem,
                                                                                                          t0.date_of_service,
                                                                                                          t0.project_id,
                                                                                                          c.per_diem
                from t0
                         left join project.employee e
                                   on t0.emp_id = e.emp_id and e.project_id = t0.project_id and
                                      e.hired_at <= t0.date_of_service and
                                      (e.terminated_at is null or e.terminated_at > t0.date_of_service)
                         left join project.craft c on e.craft = c.code and c.project_id = t0.project_id and
                                                      c.start_date <= t0.date_of_service
                    and (c.end_date is null or c.end_date > t0.date_of_service)
                order by t0.id, t0.emp_id, t0.has_per_diem, t0.date_of_service, t0.project_id, e.effected_on desc,
                         e.created_at desc, c.start_date desc, c.created_at desc
            )
            update time.allocation_time at2
            set per_diem_amount = coalesce(t1.per_diem, 0)
            from t1
            where at2.id = t1.id;
        end if;
    EXCEPTION
        WHEN duplicate_column THEN RAISE NOTICE 'column already exists.';
    END ;
$$;
