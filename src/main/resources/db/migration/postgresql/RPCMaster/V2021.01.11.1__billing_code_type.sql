create table if not exists project.billing_code_type
(
    id          bigserial primary key,
    code_type   varchar unique,
    is_required boolean not null         default false,
    level       int unique,
    create_at   timestamp with time zone default now()
);

