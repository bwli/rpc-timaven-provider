create table if not exists stage.team_cost_code
(
    id            bigserial primary key,
    submission_id bigint references stage.submission (id) on delete cascade on update cascade,
    cost_code     varchar,
    team          varchar,
    created_at    timestamp with time zone default now()
);
