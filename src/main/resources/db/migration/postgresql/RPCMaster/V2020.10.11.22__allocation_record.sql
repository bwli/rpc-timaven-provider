create table if not exists time.allocation_record
(
    id                 bigserial primary key,
    allocation_time_id bigint  not null references time.allocation_time (id) on update cascade on delete cascade,
    door_name          varchar,
    side               boolean,
    valid              boolean not null         default false,
    access_granted     boolean not null         default false,
    adjusted_time      timestamp with time zone,
    net_event_time     timestamp with time zone,
    created_at         timestamp with time zone default now()
);
