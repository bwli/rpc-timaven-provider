create table if not exists project.equipment_tag
(
    id           BIGSERIAL primary key,
    equipment_id varchar not null,
    tag          varchar not null,
    created_at   timestamp with time zone default now(),
    unique (equipment_id, tag)
);