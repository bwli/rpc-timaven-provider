-- create table if not exists stage.roster
-- (
--     id             bigserial primary key,
--     submission_id  bigint references stage.submission (id) on delete cascade on update cascade,
--     badge          integer,
--     name           varchar,
--     employee_id    varchar,
--     client_emp_id  varchar,
--     company        varchar,
--     craft_code     varchar,
--     has_per_diem   boolean,
--     has_rig_pay    boolean,
--     hired_at       date,
--     terminated_at  date,
--     shift          varchar,
--     schedule_start time,
--     schedule_end   time,
--     lunch_start    time,
--     lunch_end      time,
--     created_at     timestamp with time zone default now()
-- );

DO
$$
    BEGIN
        if exists(select *
                  from information_schema.columns
                  where table_name = 'roster'
                    and table_schema = 'stage'
                    and column_name = 'employee_id')
        then alter table stage.roster
            alter column employee_id type varchar;
        end if;
        if exists(select *
                  from information_schema.columns
                  where table_name = 'roster'
                    and table_schema = 'stage'
                    and column_name = 'client_emp_id')
        then alter table stage.roster
            alter column client_emp_id type varchar;
        end if;
    END ;
$$;
