create table if not exists project.equipment
(
    id            BIGSERIAL primary key,
    equipment_id  bigint unique not null,
    description   varchar,
    alias         varchar,
    class         varchar       not null,
    serial_number varchar,
    department    varchar,
    type          integer       not null   default 0,
    emp_id        varchar,
    is_active     boolean       not null   default true,
    created_at    timestamp with time zone default now()
);

DO
$$
    BEGIN
        if not exists(select *
                      from information_schema.columns
                      where table_name = 'equipment'
                        and table_schema = 'project'
                        and column_name = 'type')
        then alter table project.equipment
            add column type integer not null default 0;
        end if;
        if exists(select *
                  from information_schema.columns
                  where table_name = 'equipment'
                    and table_schema = 'project'
                    and column_name = 'type')
        then comment on column project.equipment.type
            is '0:daily, 1:hourly';
        end if;
        if not exists(select *
                      from information_schema.columns
                      where table_name = 'equipment'
                        and table_schema = 'project'
                        and column_name = 'emp_id')
        then alter table project.equipment
            add column emp_id varchar;
        end if;
    EXCEPTION
        WHEN duplicate_column THEN RAISE NOTICE 'columns already exists.';
    END;
$$;
