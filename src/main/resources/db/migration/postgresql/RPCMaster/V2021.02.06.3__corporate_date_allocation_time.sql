create table if not exists time.corporate_date_allocation_time
(
    id               bigserial primary key,
    date_of_service  date,
    st_hour          numeric,
    ot_hour          numeric,
    allocation_time_id bigint not null references time.corporate_allocation_time (id) on delete cascade on update cascade,
    created_at       timestamp with time zone default now()
);
