create table if not exists time.allocation_submission
(
    id                   bigserial primary key,
    submit_user_id       bigint not null,
    approve_user_id      bigint,
    finalize_user_id     bigint,
    status               integer check ( status in (0, 1, 2, 3, 4, 5)) default 0,
    report_submission_id bigint references time.report_submission (id) on delete cascade on update cascade,
    team_name            varchar,
    date_of_service      date,
    approved_at          timestamp with time zone,
    finalized_at         timestamp with time zone,
    project_id           bigint,
    by_system            boolean                                       default false,
    billing_purpose_only boolean                                       default false,
    created_at           timestamp with time zone                      default now()
);

comment on column time.allocation_submission.status
    is '0:pending, 1:released, 2:approved, 3:denied, 4:approved pending';

DO
$$
    BEGIN
        delete
        from time.allocation_submission a
            using time.allocation_submission b
        where a.status <> 3
          and b.status <> 3
          and (a.project_id is null and b.project_id is null or a.project_id = b.project_id)
          and a.team_name = b.team_name
          and a.date_of_service = b.date_of_service
          and a.id < b.id;

        create unique index if not exists team_date_of_service_project_not_null_uni_idx
            on time.allocation_submission (team_name, date_of_service, project_id)
            where project_id is not null and status <> 3;

        create unique index if not exists team_date_of_service_project_null_uni_idx
            on time.allocation_submission (team_name, date_of_service)
            where project_id is null and status <> 3;
    END;
$$;
