create table if not exists pm.user_project
(
    id         bigserial primary key,
    user_id    bigint,
    project_id bigint references pm.project (id) on delete cascade on update cascade,
    created_at timestamp with time zone default now(),
    unique (user_id, project_id)
);
