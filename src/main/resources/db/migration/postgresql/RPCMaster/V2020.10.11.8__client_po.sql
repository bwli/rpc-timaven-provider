CREATE TABLE IF NOT EXISTS project.client_po
(
    id         bigserial primary key,
    order_code VARCHAR not null,
    project_id bigint  not null,
    created_at TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),
    unique (order_code, project_id)
);
