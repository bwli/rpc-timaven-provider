create table if not exists project.purchase_vendor_roster
(
    id              bigserial   primary key,
    vendor_id       bigint      not null,
    vendor_name     varchar     not null,
    created_at      timestamp with time zone default now()
);