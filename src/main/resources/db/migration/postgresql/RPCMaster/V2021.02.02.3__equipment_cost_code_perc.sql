create table if not exists time.equipment_cost_code_perc
(
    id               bigserial primary key,
    cost_code_full   varchar,
    total_hour       numeric,
    allocate_time_id bigint not null references time.equipment_allocation_time (id) on delete cascade on update cascade,
    created_at       timestamp with time zone default now()
);
