INSERT INTO project.billing_code_type (code_type, is_required)
VALUES ('Invoice', true)
ON CONFLICT DO NOTHING;

INSERT INTO project.billing_code_type (code_type, is_required)
VALUES ('Job#', true)
ON CONFLICT DO NOTHING;

INSERT INTO project.billing_code_type (code_type, is_required)
VALUES ('Sub Job', true)
ON CONFLICT DO NOTHING;
