drop view if exists project.employee_view cascade;
create or replace view project.employee_view as
select distinct on (e.emp_id, e.hired_at) e.*
from project.employee e
where e.is_active
order by e.emp_id, e.hired_at, e.created_at desc;
