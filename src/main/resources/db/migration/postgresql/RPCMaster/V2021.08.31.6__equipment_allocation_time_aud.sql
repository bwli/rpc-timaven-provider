create table if not exists time.equipment_allocation_time_aud
(
    id                bigint,
    rev               integer not null references public.custom_revision_entity (id),
    revtype           smallint,
    equipment_id      varchar,
    hourly_type       integer,
    ownership_type    integer,
    description       varchar,
    alias             varchar,
    class             varchar,
    serial_number     varchar,
    department        varchar,
    type              integer,
    emp_id            varchar,
    total_hour        numeric,
    total_charge      numeric,
    payroll_date      date,
    weekly_process_id bigint,
    submission_id     bigint,
    created_at        timestamp with time zone default now(),
    constraint equipment_allocation_time_aud_pkey
        primary key (id, rev)
);

