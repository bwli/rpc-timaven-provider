-- create table if not exists time.allocation_submission
-- (
--     id                     bigserial primary key,
--     submit_user_id         bigint not null,
--     approve_user_id        bigint,
--     weekly_approve_user_id bigint,
--     finalize_user_id       bigint,
--     status                 integer check ( status in (0, 1, 2, 3, 4, 5)) default 0,
--     report_submission_id   bigint references time.report_submission (id) on delete cascade on update cascade,
--     team_name              varchar,
--     date_of_service        date,
--     approved_at            timestamp with time zone,
--     weekly_approved_at     timestamp with time zone,
--     weekly_process_id      bigint references time.weekly_process (id),
--     finalized_at           timestamp with time zone,
--     project_id             bigint,
--     created_at             timestamp with time zone                      default now()
-- );

-- comment on column time.allocation_submission.status
--     is '0:pending, 1:released, 2:approved, 3:denied, 4:approved pending';

DO
$$
    BEGIN
        alter table time.allocation_submission
            alter column project_id drop not null;
    END;
$$;

