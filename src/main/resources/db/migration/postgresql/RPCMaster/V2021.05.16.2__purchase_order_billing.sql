DO
$$
    BEGIN
        create table if not exists project.purchase_order_billing
        (
            id                    bigserial primary key,
            purchase_order_number varchar not null,
            billing_number        varchar,
            invoice_number        varchar,
            freight_shipping      numeric,
            other_charge          varchar,
            other_charge_amount   numeric,
            created_at            timestamp with time zone default now()
        );

        begin
            alter table project.purchase_order_billing_detail
                add column billing_id bigint references project.purchase_order_billing (id) on update cascade on delete cascade;
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column already exists.';
        end;

        if exists(select *
                  from information_schema.columns
                  where table_name = 'purchase_order_billing_detail'
                    and table_schema = 'project'
                    and column_name = 'billed_on')
        then
            insert into project.purchase_order_billing (purchase_order_number, created_at)
            select purchase_order_number, max(created_at)
            from project.purchase_order_billing_detail
            group by purchase_order_number;


            update project.purchase_order_billing_detail d
            set billing_id = b.id
            from project.purchase_order_billing b
            where b.id = (select id from project.purchase_order_billing order by id desc limit 1);

            alter table project.purchase_order_billing_detail
                alter column billing_id set not null,
                alter column billing_quantity type numeric,
                alter column markup_quantity type numeric,
                drop column billed_on cascade,
                add column tax   numeric,
                add column price numeric,
                add column part  varchar;

            update project.purchase_order_billing_detail d
            set price = po.unit_cost,
                part  = po.part_description
            from project.purchase_order po
            where po.purchase_order_number = d.purchase_order_number
              and po.line_number = d.line_number;
        end if;
    END
$$;
