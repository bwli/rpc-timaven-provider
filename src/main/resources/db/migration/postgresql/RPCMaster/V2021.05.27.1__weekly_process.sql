create table if not exists time.weekly_process
(
    id                  bigserial primary key,
    approve_user_id     bigint,
    approved_at         timestamp with time zone,
    review_user_id      bigint,
    reviewed_at         timestamp with time zone,
    finalize_user_id    bigint,
    finalized_at        timestamp with time zone,
    week_end_date       date,
    report_user_id      bigint,
    report_generated_at timestamp with time zone,
    type                integer check (type in (0, 1)) default 0,
    project_id          bigint,
    billable_amount     numeric                        default 0,
    base_amount         numeric                        default 0,
    total_hours         numeric                        default 0,
    taxable_per_diem    boolean not null               default false,
    comment             varchar,
    created_at          timestamp with time zone       default now(),
    unique (week_end_date, project_id, type)
);

DO
$$
    BEGIN
        alter table time.weekly_process
            add column comment varchar;
    EXCEPTION
        WHEN duplicate_column THEN RAISE NOTICE 'column already exists in billing.invoice.';
    END;
$$;