-- create table if not exists project.employee
-- (
--     id            BIGSERIAL primary key,
--     emp_id        bigint  not null,
--     craft         varchar,
--     first_name    varchar,
--     last_name     varchar,
--     middle_name   varchar,
--     gender        varchar check (gender in ('f', 'm')),
--     company       varchar,
--     badge         integer,
--     team_name     varchar,
--     crew          varchar,
--     dob           date,
--     effected_on   date,
--     terminated_at date,
--     rehired_at    date,
--     hired_at      date,
--     has_per_diem  boolean,
--     has_rig_pay   boolean,
--     is_active     boolean not null         default true,
--     project_id    bigint  not null,
--     created_at    timestamp with time zone default now()
-- );
--
-- comment on column project.employee.is_active
--     is 'decide whether to use this record or not';

DO
$$
    BEGIN
        if exists(select *
                  from information_schema.columns
                  where table_name = 'employee'
                    and table_schema = 'project'
                    and column_name = 'client_emp_id')
        then alter table project.employee
            rename column client_emp_id to emp_id;
        end if;
    EXCEPTION
        WHEN duplicate_column THEN RAISE NOTICE 'columns already exists.';
    END ;
$$;
