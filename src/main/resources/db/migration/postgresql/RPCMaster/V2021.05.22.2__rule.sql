create table if not exists rule.rule
(
    id                        bigserial primary key,
    rounding_minutes          integer                                                not null default 0,
    rounding_mode             varchar                                                not null default 0,
    grace_in                  integer                                                not null default 0,
    grace_out                 integer                                                not null default 0,
    description               text,
    per_diem_id               bigint references rule.per_diem (id) on update cascade,
    rig_pay_threshold         integer                                                not null default 0,
    week_end_day              integer check ( week_end_day in (0, 1, 2, 3, 4, 5, 6)) not null default 6,
    equipment_workday_hour    numeric                                                not null default 8,
    equipment_exclude_weekend boolean                                                not null default true,
    equipment_cost_code       varchar,
    st_cost_code              varchar,
    ot_cost_code              varchar,
    dt_cost_code              varchar,
    sick_cost_code            varchar,
    holiday_cost_code         varchar,
    vacation_cost_code        varchar,
    other_cost_code           varchar,
    day_off_threshold         integer                                                         default 13,
    project_id                bigint unique,
    taxable_per_diem          boolean                                                not null default false,
    mob_amount                numeric                                                not null default 0,
    created_at                timestamp with time zone                                        default now()
);

comment on column rule.rule.week_end_day
    is '0: Monday. 1: Tuesday. 2: Wednesday. 3: Thursday. 4: Friday. 5: Saturday. 6: Sunday.';

DO
$$
    BEGIN
        if not exists(select *
                      from information_schema.columns
                      where table_name = 'rule'
                        and table_schema = 'rule'
                        and column_name = 'per_diem_id')
        then
            alter table rule.rule
                add column per_diem_id bigint references rule.per_diem (id) on update cascade;

            insert into rule.per_diem (name, mode, cost_code, daily_threshold, weekly_threshold, project_id)
            select 'Default',
                   r.per_diem_mode,
                   case
                       when (r.per_diem_cost_code is null or r.per_diem_cost_code = '') then null
                       else
                           (string_to_array(trim(both ' ' from regexp_replace(r.per_diem_cost_code, '\s*,\s*', ',')),
                                            ','))[1] end,
                   r.per_diem_threshold,
                   r.per_diem_weekly_threshold,
                   r.project_id
            from rule.rule r;

            update rule.rule
            set per_diem_id = p.id
            from rule.per_diem p
            where (p.project_id is null and rule.project_id is null)
               or p.project_id = rule.project_id;

            alter table rule.rule
                drop column if exists per_diem_mode,
                drop column if exists per_diem_threshold,
                drop column if exists per_diem_weekly_threshold,
                drop column if exists per_diem_cost_code;
        end if;

    EXCEPTION
        WHEN duplicate_column THEN RAISE NOTICE 'column already exists.';
    END;
$$;
