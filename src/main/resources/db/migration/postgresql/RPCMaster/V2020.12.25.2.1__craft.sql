-- create table if not exists project.craft
-- (
--     id          bigserial primary key,
--     code        varchar not null,
--     client_code varchar,
--     billable_st numeric                  default 0,
--     billable_ot numeric                  default 0,
--     billable_dt numeric                  default 0,
--     base_st     numeric                  default 0,
--     base_ot     numeric                  default 0,
--     base_dt     numeric                  default 0,
--     description varchar,
--     per_diem    numeric,
--     rig_pay     numeric,
--     project_id  bigint  not null,
--     start_date  date,
--     end_date    date,
--     created_at  timestamp with time zone default now()
-- );

DO
$$
    BEGIN
        if not exists(select *
                      from information_schema.columns
                      where table_name = 'craft'
                        and table_schema = 'project'
                        and column_name = 'client_code')
        then alter table project.craft
            add column client_code varchar;
        end if;
    END ;
$$;
