drop view if exists project.cost_code_association cascade;
drop extension if exists tablefunc cascade;
create extension if not exists tablefunc;
create or replace view project.cost_code_association as
with t0 as (
    SELECT *
    FROM crosstab(
                 $$
            SELECT sub.id, sub.cost_code_full, sub.tags, sub.description, sub.job_number, sub.sub_job, sub.project_id, sub.start_date, sub.end_date, sub.created_at, sub.level, sub.code_name
            FROM (
                    SELECT cc.id
                         , cc.cost_code_full
                         , cc.tags
                         , cc.description
                         , p.job_number
                         , p.sub_job
                         , cc.project_id
                         , bct.level
                         , bc.code_name
                         , cc.start_date
                         , cc.end_date
                         , cc.created_at
                    FROM (select cclg.id,
                                 cclg.cost_code_full,
                                 cclg.project_id,
                                 cclg.description,
                                 cclg.start_date,
                                 cclg.end_date,
                                 cclg.created_at,
                                 string_agg(ct.tag, ',') as tags
                          from project.cclg
                                   left join project.cclg_tag ct on ct.cost_code_full = cclg.cost_code_full and
                                                                    (ct.project_id is null and cclg.project_id is null or
                                                                     ct.project_id = cclg.project_id)
                          where cclg.start_date <= current_date and (cclg.end_date is null or cclg.end_date >= current_date)
                          group by cclg.id, cclg.cost_code_full, cclg.project_id, cclg.description, cclg.start_date, cclg.end_date,
                                   cclg.created_at
                          order by created_at desc) as cc
                             left join project.cost_code_billing_code ccbc
                                       on cc.id = ccbc.cost_code_id
                             left join project.billing_code bc on ccbc.billing_code_id = bc.id
                             left join project.billing_code_type bct on bc.type_id = bct.id
                             left join pm.project p on cc.project_id = p.id
                 ) sub
            ORDER BY cost_code_full, project_id nulls first
            $$::text
             , 'VALUES (1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12),(13),(14),(15),(16),(17),(18),(19),(20)'::text
             ) AS t (id bigint, cost_code varchar, tags varchar, description varchar, job_number varchar,
                     sub_job varchar, project_id bigint, start_date date, end_date date,
                     created_at timestamp with time zone, level1 varchar, level2 varchar, level3 varchar,
                     level4 varchar, level5 varchar, level6 varchar, level7 varchar, level8 varchar, level9 varchar,
                     level10 varchar, level11 varchar, level12 varchar, level13 varchar, level14 varchar,
                     level15 varchar, level16 varchar, level17 varchar, level18 varchar, level19 varchar,
                     level20 varchar)
),
     t1 as (
         select max(level) as max_level
         from project.billing_code_type
         where level is not null)
select id,
       cost_code,
       tags,
       description,
       job_number,
       sub_job,
       project_id,
       start_date,
       end_date,
       created_at,
       level1,
       level2,
       level3,
       level4,
       level5,
       level6,
       level7,
       level8,
       level9,
       level10,
       level11,
       level12,
       level13,
       level14,
       level15,
       level16,
       level17,
       level18,
       level19,
       level20,
       coalesce(level1, '') ||
       coalesce(level2, '') ||
       coalesce(level3, '') ||
       coalesce(level4, '') ||
       coalesce(level5, '') ||
       coalesce(level6, '') ||
       coalesce(level7, '') ||
       coalesce(level8, '') ||
       coalesce(level9, '') ||
       coalesce(level10, '') ||
       coalesce(level11, '') ||
       coalesce(level12, '') ||
       coalesce(level13, '') ||
       coalesce(level14, '') ||
       coalesce(level15, '') ||
       coalesce(level16, '') ||
       coalesce(level17, '') ||
       coalesce(level18, '') ||
       coalesce(level19, '') ||
       coalesce(level20, '')                                                                           as cost_code_full,
       coalesce(level1, '--') ||
       case when t1.max_level >= 2 then '.' || coalesce(level2, '--') else coalesce(level2, '') end ||
       case when t1.max_level >= 3 then '.' || coalesce(level3, '--') else coalesce(level3, '') end ||
       case when t1.max_level >= 4 then '.' || coalesce(level4, '--') else coalesce(level4, '') end ||
       case when t1.max_level >= 5 then '.' || coalesce(level5, '--') else coalesce(level5, '') end ||
       case when t1.max_level >= 6 then '.' || coalesce(level6, '--') else coalesce(level5, '') end ||
       case when t1.max_level >= 7 then '.' || coalesce(level7, '--') else coalesce(level7, '') end ||
       case when t1.max_level >= 8 then '.' || coalesce(level8, '--') else coalesce(level8, '') end ||
       case when t1.max_level >= 9 then '.' || coalesce(level9, '--') else coalesce(level9, '') end ||
       case when t1.max_level >= 10 then '.' || coalesce(level10, '--') else coalesce(level10, '') end ||
       case when t1.max_level >= 11 then '.' || coalesce(level11, '--') else coalesce(level11, '') end ||
       case when t1.max_level >= 12 then '.' || coalesce(level12, '--') else coalesce(level12, '') end ||
       case when t1.max_level >= 13 then '.' || coalesce(level13, '--') else coalesce(level13, '') end ||
       case when t1.max_level >= 14 then '.' || coalesce(level14, '--') else coalesce(level14, '') end ||
       case when t1.max_level >= 15 then '.' || coalesce(level15, '--') else coalesce(level15, '') end ||
       case when t1.max_level >= 16 then '.' || coalesce(level16, '--') else coalesce(level16, '') end ||
       case when t1.max_level >= 17 then '.' || coalesce(level17, '--') else coalesce(level17, '') end ||
       case when t1.max_level >= 18 then '.' || coalesce(level18, '--') else coalesce(level18, '') end ||
       case when t1.max_level >= 19 then '.' || coalesce(level19, '--') else coalesce(level19, '') end ||
       case
           when t1.max_level >= 20 then '.' || coalesce(level20, '--')
           else coalesce(level20, '') end                                                              as cost_code_segment
from t0,
     t1;
