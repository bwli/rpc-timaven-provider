-- create table if not exists project.equipment_usage
-- (
--     id           BIGSERIAL primary key,
--     equipment_id bigint  not null references project.equipment (id) on update cascade on delete cascade,
--     start_date   date,
--     end_date     date,
--     started_by   bigint,
--     ended_by     bigint,
--     project_id   bigint,
--     is_active    boolean not null         default true,
--     daily_rate   numeric                  default 0,
--     weekly_rate  numeric                  default 0,
--     monthly_rate numeric                  default 0,
--     created_at   timestamp with time zone default now()
-- );

DO
$$
    BEGIN
        alter table project.equipment_usage
            add column is_active boolean not null default true;
    EXCEPTION
        WHEN duplicate_column THEN RAISE NOTICE 'columns already exists.';
    END;
$$;
