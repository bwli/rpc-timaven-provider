create table if not exists time.stage_trans
(
    id              bigserial primary key,
    submission_id   bigint not null references time.trans_submission (id) on update cascade on delete cascade,
    first_name      varchar,
    last_name       varchar,
    middle_name     varchar,
    contract_id     bigint,
    date_of_service date,
    payroll_date    date,
    st_rate         numeric                  default 0,
    ot_rate         numeric                  default 0,
    dt_rate         numeric                  default 0,
    st_hour         numeric,
    ot_hour         numeric,
    dt_hour         numeric,
    net_hour        numeric,
    total_hour      numeric,
    area_id         bigint,
    work_unit_id    varchar,
    client_emp_id   bigint,
    team_name       varchar,
    pd              numeric,
    created_at      timestamp with time zone default now()
);

DO
$$
    BEGIN
        if exists(select *
                  from information_schema.columns
                  where table_name = 'stage_trans'
                    and table_schema = 'time'
                    and column_name = 'client_emp_id')
        then comment on column time.stage_trans.client_emp_id
            is 'employee or equipment_id';
        end if;
        if exists(select *
                  from information_schema.columns
                  where table_name = 'stage_trans'
                    and table_schema = 'time'
                    and column_name = 'st_rate')
        then comment on column time.stage_trans.st_rate
            is 'standard hour';
        end if;
        if exists(select *
                  from information_schema.columns
                  where table_name = 'stage_trans'
                    and table_schema = 'time'
                    and column_name = 'ot_rate')
        then comment on column time.stage_trans.ot_rate
            is 'overtime hour';
        end if;
        if exists(select *
                  from information_schema.columns
                  where table_name = 'stage_trans'
                    and table_schema = 'time'
                    and column_name = 'dt_rate')
        then comment on column time.stage_trans.dt_rate
            is 'double hour';
        end if;
        if exists(select *
                  from information_schema.columns
                  where table_name = 'stage_trans'
                    and table_schema = 'time'
                    and column_name = 'st_hour')
        then comment on column time.stage_trans.st_hour
            is 'standard rate';
        end if;
        if exists(select *
                  from information_schema.columns
                  where table_name = 'stage_trans'
                    and table_schema = 'time'
                    and column_name = 'ot_hour')
        then comment on column time.stage_trans.ot_hour
            is 'overtime rate';
        end if;
        if exists(select *
                  from information_schema.columns
                  where table_name = 'stage_trans'
                    and table_schema = 'time'
                    and column_name = 'dt_hour')
        then comment on column time.stage_trans.dt_hour
            is 'double rate';
        end if;
        if exists(select *
                  from information_schema.columns
                  where table_name = 'stage_trans'
                    and table_schema = 'time'
                    and column_name = 'team_name')
        then comment on column time.stage_trans.team_name
            is 'refer to team name';
        end if;
        if exists(select *
                  from information_schema.columns
                  where table_name = 'stage_trans'
                    and table_schema = 'time'
                    and column_name = 'pd')
        then comment on column time.stage_trans.pd
            is 'stands for per diem. refer to extra charge. often an employee, per day to cover living expenses when traveling for work';
        end if;
    END ;
$$;
