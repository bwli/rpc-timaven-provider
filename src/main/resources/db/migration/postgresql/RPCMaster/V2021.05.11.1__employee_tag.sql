create table if not exists project.employee_tag
(
    id         BIGSERIAL primary key,
    emp_id     varchar not null,
    tag        varchar not null,
    project_id bigint,
    created_at timestamp with time zone default now()
);

create unique index if not exists employee_tag_project_id_not_null_uni_idx
    on project.employee_tag (emp_id, tag, project_id)
    where project_id is not null;

create unique index if not exists employee_tag_project_id_null_uni_idx
    on project.employee_tag (emp_id, tag)
    where project_id is null;
