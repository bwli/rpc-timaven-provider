create table if not exists time.report_submission
(
    id          bigserial primary key,
    user_id     bigint  not null,
    team_name   varchar not null,
    project_id  bigint  not null,
    reported_at timestamp with time zone default now()
);
