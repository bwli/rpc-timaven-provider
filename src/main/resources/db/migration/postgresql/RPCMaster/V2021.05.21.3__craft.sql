create table if not exists project.craft
(
    id          bigserial primary key,
    code        varchar not null,
    billable_st numeric                  default 0,
    billable_ot numeric                  default 0,
    billable_dt numeric                  default 0,
    description varchar,
    per_diem    numeric,
    rig_pay     numeric,
    project_id  bigint,
    start_date  date,
    end_date    date,
    created_at  timestamp with time zone default now()
);

DO
$$
    BEGIN
        alter table project.craft
            drop column if exists base_st,
            drop column if exists base_ot,
            drop column if exists base_dt;
    END ;
$$;
