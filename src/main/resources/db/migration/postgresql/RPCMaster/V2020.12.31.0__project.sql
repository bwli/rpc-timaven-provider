-- create table if not exists pm.project
-- (
--     id          bigserial primary key,
--     name        varchar,
--     description varchar,
--     user_id     bigint,
--     is_active   boolean not null         default true,
--     create_at   timestamp with time zone default now()
-- );

DO
$$
    BEGIN
        alter table pm.project
            add column is_active boolean not null default true;
    EXCEPTION
        WHEN duplicate_column THEN RAISE NOTICE 'column already exists.';
    END;
$$;
