create table if not exists project.cost_code_tag_type
(
    id          bigserial primary key,
    tag_type    varchar,
    description varchar,
    project_id  bigint,
    create_at   timestamp with time zone default now()
);

create unique index if not exists tag_type_project_id_not_null_uni_idx
    on project.cost_code_tag_type (tag_type, project_id)
    where project_id is not null;

create unique index if not exists tag_type_project_id_null_uni_idx
    on project.cost_code_tag_type (tag_type)
    where project_id is null;
