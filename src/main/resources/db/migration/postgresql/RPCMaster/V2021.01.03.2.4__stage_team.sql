-- create table if not exists stage.team
-- (
--     id            bigserial primary key,
--     submission_id bigint references stage.submission (id) on delete cascade on update cascade,
--     employee_id   varchar,
--     employee_name varchar,
--     team          varchar,
--     crew          varchar,
--     terminated_at date,
--     created_at    timestamp with time zone default now()
-- );

DO
$$
    BEGIN
        if exists(select *
                  from information_schema.columns
                  where table_name = 'team'
                    and table_schema = 'stage'
                    and column_name = 'employee_id')
        then alter table stage.team
            alter column employee_id type varchar;
        end if;
    END ;
$$;
