create table if not exists project.purchase_order_comment
(
    id                    bigserial primary key,
    purchase_order_number varchar not null unique,
    comment               varchar,
    created_at            timestamp with time zone default now()
);