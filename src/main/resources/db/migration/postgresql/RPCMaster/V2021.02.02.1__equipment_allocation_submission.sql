create table if not exists time.equipment_allocation_submission
(
    id              bigserial primary key,
    approve_user_id bigint,
    status          integer check ( status in (0, 1)) default 0,
    date_of_service date,
    approved_at     timestamp with time zone,
    project_id      bigint not null,
    created_at      timestamp with time zone          default now()
);
--
comment on column time.equipment_allocation_submission.status
    is '0:pending, 1:approved';


