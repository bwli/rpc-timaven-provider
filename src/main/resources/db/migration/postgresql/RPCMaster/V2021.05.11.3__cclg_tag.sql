create table if not exists project.cclg_tag
(
    id             bigserial primary key,
    cost_code_full varchar not null,
    tag            varchar not null,
    project_id     bigint,
    created_at     timestamp with time zone default now()
);

create unique index if not exists cclg_tag_project_id_not_null_uni_idx
    on project.cclg_tag (cost_code_full, tag, project_id)
    where project_id is not null;

create unique index if not exists cclg_tag_project_id_null_uni_idx
    on project.cclg_tag (cost_code_full, tag)
    where project_id is null;
