create table if not exists time.allocation_time
(
    id                 bigserial primary key,
    emp_id             varchar,
    client_emp_id      varchar,
    badge              varchar,
    team_name          varchar not null,
    first_name         varchar,
    last_name          varchar,
    department         varchar,
    job_number         varchar,
    st_hour            numeric,
    ot_hour            numeric,
    dt_hour            numeric,
    extra_time_type    integer                  default 0,
    total_hour         numeric,
    allocated_hour     numeric,
    net_hour           numeric,
    has_per_diem       bool    not null         default false,
    rig_pay            numeric,
    payroll_date       date,
    max_time_cost_code varchar,
    per_diem_cost_code varchar,
    mob_cost_code      varchar,
    per_diem_amount    numeric not null         default 0,
    mob_amount         numeric not null         default 0,
    mob_mileage_rate   numeric not null         default 0,
    mob_mileage        numeric not null         default 0,
    submission_id      bigint  not null references time.allocation_submission (id) on update cascade on delete cascade,
    created_at         timestamp with time zone default now()
);

DO
$$
    BEGIN
        if not exists(select *
                      from information_schema.columns
                      where table_name = 'allocation_time'
                        and table_schema = 'time'
                        and column_name = 'mob_mileage_rate')
        then
            alter table time.allocation_time
                add column mob_mileage_rate numeric not null default 0,
                add column mob_mileage      numeric not null default 0;
            with t0 as (
                select r.mob_mileage_rate as mob_mileage_rate, r.project_id
                from rule.rule r
            )
            update time.allocation_time t
            set mob_mileage_rate = t0.mob_mileage_rate,
                mob_mileage      = case when t0.mob_mileage_rate = 0 then 0 else mob_amount / t0.mob_mileage_rate end
            from t0,
                 time.allocation_submission s
            where t0.project_id = s.project_id
              and s.id = t.submission_id;
        end if;
    EXCEPTION
        WHEN duplicate_column THEN RAISE NOTICE 'column already exists.';
    END ;
$$;
