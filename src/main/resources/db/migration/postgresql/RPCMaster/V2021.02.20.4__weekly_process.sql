create table if not exists time.weekly_process
(
    id                  bigserial primary key,
    approve_user_id     bigint,
    approved_at         timestamp with time zone,
    review_user_id      bigint,
    reviewed_at         timestamp with time zone,
    finalize_user_id    bigint,
    finalized_at        timestamp with time zone,
    week_end_date       date,
    report_user_id      bigint,
    report_generated_at timestamp with time zone,
    project_id          bigint,
    billable_amount     numeric                  default 0,
    base_amount         numeric                  default 0,
    total_hours         numeric                  default 0,
    created_at          timestamp with time zone default now(),
    unique (week_end_date, project_id)
);

DO
$$
    BEGIN
        alter table time.weekly_process
            add column review_user_id bigint,
            add column reviewed_at    timestamp with time zone,
            alter column project_id drop not null;
    EXCEPTION
        WHEN duplicate_column THEN RAISE NOTICE 'column claim_number already exists in billing.invoice.';
    END;
$$;
