create table if not exists time.equipment_allocation_time
(
    id                bigserial primary key,
    equipment_id      bigint  not null,
    description       varchar,
    alias             varchar,
    class             varchar not null,
    serial_number     varchar,
    department        varchar,
    type              integer not null         default 0,
    emp_id            varchar,
    total_hour        numeric,
    payroll_date      date,
    weekly_process_id bigint references time.weekly_process (id),
    submission_id     bigint  not null references time.equipment_allocation_submission (id) on update cascade on delete cascade,
    created_at        timestamp with time zone default now()
);

DO
$$
    BEGIN
        alter table time.equipment_allocation_time
            add column weekly_process_id bigint references time.equipment_weekly_process (id);
    EXCEPTION
        WHEN duplicate_column THEN RAISE NOTICE 'column claim_number already exists in billing.invoice.';
    END;
$$;
