create table if not exists time.activity_code_aud
(
    id            bigint,
    rev           integer not null references public.custom_revision_entity (id),
    revtype       smallint,
    activity_code varchar,
    constraint activity_code_aud_pkey
        primary key (id, rev)
);

