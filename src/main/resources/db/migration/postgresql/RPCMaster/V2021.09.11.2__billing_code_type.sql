create table if not exists project.billing_code_type
(
    id                    bigserial primary key,
    code_type             varchar unique,
    is_required           boolean not null         default false,
    manageable_by_project boolean not null         default false,
    parsed_by             integer not null         default 0,
    level                 int unique,
    create_at             timestamp with time zone default now()
);

DO
$$
    BEGIN
        alter table project.billing_code_type
            add column parsed_by integer not null default 0;
        update project.billing_code_type set parsed_by = 1 where code_type in ('PO', 'WO');
        comment on column project.billing_code_type.parsed_by
            is '0: code_name. 1: client_alias.';
    EXCEPTION
        WHEN duplicate_column THEN RAISE NOTICE 'column already exists.';
    END;
$$;
