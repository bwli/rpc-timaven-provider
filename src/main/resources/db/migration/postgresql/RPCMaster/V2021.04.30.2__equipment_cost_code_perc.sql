create table if not exists time.equipment_cost_code_perc
(
    id               bigserial primary key,
    cost_code_full   varchar,
    total_hour       numeric,
    total_charge     numeric,
    allocate_time_id bigint not null references time.equipment_allocation_time (id) on delete cascade on update cascade,
    created_at       timestamp with time zone default now()
);

DO
$$
    BEGIN
        alter table time.equipment_cost_code_perc
            add column total_charge numeric;
    EXCEPTION
        WHEN duplicate_column THEN RAISE NOTICE 'column already exists.';
    END;
$$;
