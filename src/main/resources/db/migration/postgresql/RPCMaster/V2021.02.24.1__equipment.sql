create table if not exists project.equipment
(
    id             BIGSERIAL primary key,
    equipment_id   varchar unique not null,
    description    varchar,
    alias          varchar,
    class          varchar        not null,
    serial_number  varchar,
    department     varchar,
    hourly_type    integer        not null  default 0,
    ownership_type integer        not null  default 0,
    po             varchar,
    emp_id         varchar,
    is_active      boolean        not null  default true,
    created_at     timestamp with time zone default now()
);

DO
$$
    BEGIN
        if not exists(select *
                      from information_schema.columns
                      where table_name = 'equipment'
                        and table_schema = 'project'
                        and column_name = 'hourly_type')
        then alter table project.equipment
            rename type to hourly_type;
            comment on column project.equipment.hourly_type is '0: Non-hourly, 1: Hourly';
        end if;
        if not exists(select *
                      from information_schema.columns
                      where table_name = 'equipment'
                        and table_schema = 'project'
                        and column_name = 'ownership_type')
        then alter table project.equipment
            add ownership_type integer not null default 0;
            comment on column project.equipment.ownership_type is '0: Company Owned, 1: Rental';
        end if;
        if not exists(select *
                      from information_schema.columns
                      where table_name = 'equipment'
                        and table_schema = 'project'
                        and column_name = 'po')
        then alter table project.equipment
            add column po varchar;
        end if;
    END ;
$$;
