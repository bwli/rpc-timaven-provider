create table if not exists time.report_perc
(
    id             bigserial primary key,
    cost_code_full varchar null,
    order_code     varchar null,
    percentage     integer not null,
    submission_id  bigint  not null references time.report_submission (id) on update cascade on delete cascade,
    created_at     timestamp with time zone default now(),
    unique (cost_code_full, submission_id)
);
