create table if not exists project.billing_code_type
(
    id                    bigserial primary key,
    code_type             varchar unique,
    is_required           boolean not null         default false,
    manageable_by_project boolean not null         default false,
    level                 int unique,
    create_at             timestamp with time zone default now()
);

DO
$$
    BEGIN
        alter table project.billing_code_type
            add column manageable_by_project boolean not null default false;
    EXCEPTION
        WHEN duplicate_column THEN RAISE NOTICE 'column already exists.';
    END;
$$;
