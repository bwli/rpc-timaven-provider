-- create table if not exists project.employee_aud
-- (
--     id            bigint  not null,
--     rev           integer not null references public.custom_revision_entity (id),
--     revtype       smallint,
--     is_active     boolean,
--     badge         integer,
--     team_name     varchar,
--     crew          varchar,
--     emp_id        varchar,
--     client_emp_id varchar,
--     company       varchar(255),
--     created_at    timestamp,
--     dob           date,
--     first_name    varchar(255),
--     gender        varchar(255),
--     has_per_diem  boolean,
--     has_rig_pay   boolean,
--     hired_at      date,
--     last_name     varchar(255),
--     middle_name   varchar(255),
--     project_id    bigint,
--     rehired_at    date,
--     effected_on   date,
--     terminated_at date,
--     craft         varchar,
--     constraint employee_aud_pkey
--         primary key (id, rev)
-- );

DO
$$
    BEGIN
        if exists(select *
                  from information_schema.columns
                  where table_name = 'employee_aud'
                    and table_schema = 'project'
                    and column_name = 'emp_id')
        then alter table project.employee_aud
            alter column emp_id type varchar;
        end if;
        if exists(select *
                  from information_schema.columns
                  where table_name = 'employee_aud'
                    and table_schema = 'project'
                    and column_name = 'client_emp_id')
        then alter table project.employee_aud
            alter column client_emp_id type varchar;
        end if;
    END ;
$$;
