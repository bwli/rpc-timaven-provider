create table if not exists time.corporate_allocation_time
(
    id                bigserial primary key,
    emp_id            varchar not null,
    first_name        varchar,
    last_name         varchar,
    department        varchar,
    job_number        varchar,
    holiday_hour      numeric,
    vacation_hour     numeric,
    sick_hour         numeric,
    other_hour        numeric,
    weekly_process_id bigint  not null references time.corporate_weekly_process (id) on delete cascade on update cascade,
    created_at        timestamp with time zone default now()
);
