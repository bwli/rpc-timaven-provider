-- create table if not exists time.allocation_time
-- (
--     id                 bigserial primary key,
--     emp_id             bigint  not null,
--     team_name          varchar not null,
--     first_name         varchar,
--     last_name          varchar,
--     st_hour            numeric,
--     ot_hour            numeric,
--     dt_hour            numeric,
--     total_hour         numeric,
--     allocated_hour     numeric,
--     net_hour           numeric,
--     has_per_diem       bool    not null         default false,
--     rig_pay            numeric,
--     payroll_date       date,
--     max_time_cost_code varchar,
--     submission_id      bigint  not null references time.allocation_submission (id) on update cascade on delete cascade,
--     created_at         timestamp with time zone default now()
-- );

DO
$$
    BEGIN
        if exists(select *
                  from information_schema.columns
                  where table_name = 'allocation_time'
                    and table_schema = 'time'
                    and column_name = 'client_emp_id')
        then alter table time.allocation_time
            rename column client_emp_id to emp_id;
        end if;
    EXCEPTION
        WHEN duplicate_column THEN RAISE NOTICE 'columns already exists.';
    END ;
$$;
