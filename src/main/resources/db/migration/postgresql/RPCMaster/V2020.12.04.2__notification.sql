create table if not exists project.notification
(
    id           bigserial primary key,
    user_id      bigint  not null,
    message_from varchar,
    summary      varchar,
    message      varchar,
    is_read      boolean not null         default false,
    created_at   timestamp with time zone default now()
);
