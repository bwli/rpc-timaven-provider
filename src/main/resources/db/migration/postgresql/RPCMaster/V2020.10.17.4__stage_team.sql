create table if not exists stage.team
(
    id            bigserial primary key,
    submission_id bigint references stage.submission (id) on delete cascade on update cascade,
    employee_id   bigint,
    employee_name varchar,
    team          varchar,
    crew          varchar,
    terminated_at date,
    created_at    timestamp with time zone default now()
);
